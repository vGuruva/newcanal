(function( $ ){
	        var mapplic = $("#mapplic").mapplic({
	            source : {
	                "mapwidth":"1200",
	                "mapheight":"746",
	                "minimap": false,
	                "clearbutton": false,
	                "zoombuttons": true,
	                "sidebar": true,
	                "search": true,
	                "hovertip": false,
	                "mousewheel": false,
	                "fullscreen": false,
	                "deeplinking": true,
	                "mapfill": true,
	                "zoom": false,
	                "alphabetic": true,
	                "zoomlimit": "2",
	                "categories": [{
				"id": "Banks & Forex",
				"title": "Banks & Forex",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Books, Cards & Gifts",
				"title": "Books, Cards & Gifts",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Coffee Shops",
				"title": "Coffee Shops",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Department Stores",
				"title": "Department Stores",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Eyewear & Optometrists",
				"title": "Eyewear & Optometrists",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Fashion & Footwear",
				"title": "Fashion & Footwear",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Fast Food & Confectionary",
				"title": "Fast Food & Confectionary",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Food & Beverage",
				"title": "Food & Beverage",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Footwear",
				"title": "Footwear",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Groceries & Supermarkets",
				"title": "Groceries & Supermarkets",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Halaal",
				"title": "Halaal",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Health Food",
				"title": "Health Food",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Health, Hair & Beauty",
				"title": "Health, Hair & Beauty",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Home & Décor",
				"title": "Home & Décor",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Jewellery & Accessories",
				"title": "Jewellery & Accessories",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Kids Fashion",
				"title": "Kids Fashion",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Leather & Luggage",
				"title": "Leather & Luggage",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Liquor",
				"title": "Liquor",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Men's Fashion",
				"title": "Men's Fashion",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Movies & Entertainment",
				"title": "Movies & Entertainment",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Pet Shops",
				"title": "Pet Shops",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Restaurants",
				"title": "Restaurants",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Speciality",
				"title": "Speciality",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Speciality Shops & Services",
				"title": "Speciality Shops & Services",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Sport, Fitness & Lifestyle",
				"title": "Sport, Fitness & Lifestyle",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Technology",
				"title": "Technology",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Tobacconists & E-Cigarettes",
				"title": "Tobacconists & E-Cigarettes",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Toys & Hobbies",
				"title": "Toys & Hobbies",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Travel",
				"title": "Travel",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Unisex Fashion",
				"title": "Unisex Fashion",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Women's Fashion",
				"title": "Women's Fashion",
				"color": "#666666",
				"show": "false"
			},],
	                "levels": [{
				"id": "LL",
				"title": "Lower Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5a8/d7c/86e/5a8d7c86e0567658639788.svg","show": true,"locations": [
					{
						"id": "ll122","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/684/6bc/thumb_1071_268_231_0_0_auto.png","title": "@home",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">@home</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/home?location=ll122\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '2'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6921",
						"y": "0.4447",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll5","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/688/588/thumb_1074_268_231_0_0_auto.png","title": "Absolute Pets 1",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Absolute Pets 1</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/absolute-pets-1?location=ll5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '5'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2884",
						"y": "0.4163",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll121","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/688/793/thumb_1075_268_231_0_0_auto.png","title": "Absolute Pets 2",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Absolute Pets 2</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/absolute-pets-2?location=ll121\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '6'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.802",
						"y": "0.3575",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll195","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/688/9c3/thumb_1076_268_231_0_0_auto.png","title": "Accessorize",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Accessorize</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/accessorize?location=ll195\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '7'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6371",
						"y": "0.4754",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll56","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/689/90c/thumb_1079_268_231_0_0_auto.png","title": "Adidas Kids",
						"pin": "blue","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion</h4><h3 class=\"mapplic-map-store-name\">Adidas Kids</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/adidas-kids?location=ll56\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '10'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.589",
						"y": "0.3631",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll170","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/689/cc2/thumb_1081_268_231_0_0_auto.png","title": "ALDO",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">ALDO</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/aldo?location=ll170\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '12'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3737",
						"y": "0.4155",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB10","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/689/e80/thumb_1082_268_231_0_0_auto.png","title": "All Leather",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">All Leather</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/all-leather?location=llB10\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '13'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7162",
						"y": "0.4349",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll241A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/042/thumb_1083_268_231_0_0_auto.png","title": "Alysian Hair Design",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Alysian Hair Design</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/alysian-hair?location=ll241A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '140'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.449",
						"y": "0.5274",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll229","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/25f/thumb_1084_268_231_0_0_auto.png","title": "American Express Forex",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">American Express Forex</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/american-express-forex?location=ll229\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '14'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4712",
						"y": "0.5528",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC9","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/79b/thumb_1086_268_231_0_0_auto.png","title": "Anat Falafel & Shwarma",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Anat Falafel & Shwarma</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/anat-falafel-shwarma?location=llFC9\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '17'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5826",
						"y": "0.2885",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll74","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/efa/thumb_1089_268_231_0_0_auto.png","title": "Audiolens",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Audiolens</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/audiolens?location=ll74\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '20'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6795",
						"y": "0.3706",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll13","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68c/385/thumb_1091_268_231_0_0_auto.png","title": "Barksole",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Barksole</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/barksole?location=ll13\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '22'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2973",
						"y": "0.3726",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll17","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68c/630/thumb_1092_268_231_0_0_auto.png","title": "Baskin Robbins",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Baskin Robbins</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/baskin-robbins?location=ll17\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '23'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3129",
						"y": "0.3552",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "lla8","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68c/8e3/thumb_1093_268_231_0_0_auto.png","title": "Bayete Basics",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Bayete Basics</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bayete-basics?location=lla8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '25'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3924",
						"y": "0.4131",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll215","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68d/cde/thumb_1095_268_231_0_0_auto.png","title": "Bella & Me Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Bella & Me Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bella-me-jewellers?location=ll215\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '27'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.503",
						"y": "0.4725",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA15","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/699/ada/thumb_1096_268_231_0_0_auto.png","title": "Bella Roza Hair",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Bella Roza Hair</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bella-roza-hair?location=llA15\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '379'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4009",
						"y": "0.4032",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll28","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7a3/15d/thumb_1099_268_231_0_0_auto.png","title": "Big Blue",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Big Blue</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/big-blue?location=ll28\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '30'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4343",
						"y": "0.3698",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll57","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7a3/426/thumb_1100_268_231_0_0_auto.png","title": "Blades & Triggers",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Blades & Triggers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/blades-triggers?location=ll57\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '32'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5082",
						"y": "0.3345",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll99","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ac/487/thumb_1102_268_231_0_0_auto.png","title": "Boardmans",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Boardmans</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/boardmans?location=ll99\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '35'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.659",
						"y": "0.2877",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll127","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ac/fe8/thumb_1105_268_231_0_0_auto.png","title": "Boesmanland Biltong",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Boesmanland Biltong</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/boesmanland-biltong?location=ll127\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '38'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8248",
						"y": "0.3825",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll100","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ae/66c/thumb_1108_268_231_0_0_auto.png","title": "Brocka",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Brocka</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/brocka?location=ll100\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '383'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8079",
						"y": "0.4004",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll47","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ae/cc8/thumb_1110_268_231_0_0_auto.png","title": "BT Games",
						"pin": "blue","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment</h4><h3 class=\"mapplic-map-store-name\">BT Games</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bt-games?location=ll47\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '42'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.469",
						"y": "0.3258",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll233","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7af/8fa/thumb_1111_268_231_0_0_auto.png","title": "Budget Keys",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Budget Keys</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>0</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/budget-keys?location=ll233\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '43'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4815",
						"y": "0.5861",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll152","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bbf/798/thumb_1122_268_231_0_0_auto.png","title": "Cape Union Mart",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Cape Union Mart</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cape-union-mart?location=ll152\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '50'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5218",
						"y": "0.4151",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll171","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bbf/f02/thumb_1124_268_231_0_0_auto.png","title": "Capitec Bank",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">Capitec Bank</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/capitec-bank?location=ll171\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '52'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6732",
						"y": "0.5302",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll269","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc0/24a/thumb_1125_268_231_0_0_auto.png","title": "Cardies",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">Cardies</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cardies?location=ll269\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '53'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3308",
						"y": "0.4662",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll255A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc1/1d5/thumb_1128_268_231_0_0_auto.png","title": "Carrol Boyes",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Carrol Boyes</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/carrol-boyes?location=ll255A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '56'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4003",
						"y": "0.4725",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA6","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/68e/29e/thumb_1454_268_231_0_0_auto.png","title": "Casari",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">Casari</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/casari?location=llA6\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '57'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3923",
						"y": "0.4099",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll15","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc2/4fb/thumb_1131_268_231_0_0_auto.png","title": "Cell C",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cell C</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cell-c-2?location=ll15\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '59'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.307",
						"y": "0.3655",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll180","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc2/7c5/thumb_1132_268_231_0_0_auto.png","title": "Cellini",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">Cellini</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cellini?location=ll180\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '60'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3409",
						"y": "0.4111",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll273A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc2/ab8/thumb_1133_268_231_0_0_auto.png","title": "Cellucity",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cellucity</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cellucity?location=ll273A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '61'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3072",
						"y": "0.4595",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd0/5d5/thumb_1157_268_231_0_0_auto.png","title": "Cellular Supreme",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cellular Supreme</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cellular-supreme?location=llB4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '62'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7168",
						"y": "0.3885",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll181","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc3/5f9/thumb_1134_268_231_0_0_auto.png","title": "Cellular Village",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cellular Village</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cellular-village-1?location=ll181\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '63'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6368",
						"y": "0.5782",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll140","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8a/8e3/thumb_1469_268_231_0_0_auto.png","title": "Charles & Keith",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Charles & Keith</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/charles-keith?location=ll140\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '67'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5904",
						"y": "0.4007",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll11","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc5/6e6/thumb_1136_268_231_0_0_auto.png","title": "Checkers",
						"pin": "blue","category": "Groceries & Supermarkets","description": "<h4 class=\"mapplic-map-store-category\">Groceries & Supermarkets</h4><h3 class=\"mapplic-map-store-name\">Checkers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/checkers?location=ll11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '68'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.239",
						"y": "0.405",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll141","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc5/f0f/thumb_1138_268_231_0_0_auto.png","title": "Cinnabon",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Cinnabon</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cinnabon?location=ll141\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '71'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8204",
						"y": "0.4385",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFCK3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc6/1b1/thumb_1139_268_231_0_0_auto.png","title": "Cinnabon Kiosk",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Cinnabon Kiosk</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cinnabon-kiosk?location=llFCK3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '72'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4845",
						"y": "0.3923",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll22","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc6/448/thumb_1140_268_231_0_0_auto.png","title": "Claire's",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Claire's</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/claires?location=ll22\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '73'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4095",
						"y": "0.3742",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll103","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc6/761/thumb_1141_268_231_0_0_auto.png","title": "Clicks",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Clicks</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/clicks?location=ll103\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '74'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6945",
						"y": "0.2799",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll51","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc6/9c9/thumb_1142_268_231_0_0_auto.png","title": "CNA",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">CNA</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cna?location=ll51\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '75'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4865",
						"y": "0.3056",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll167","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc6/d7a/thumb_1143_268_231_0_0_auto.png","title": "Col'Cacchio",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Col'Cacchio</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/colcacchio?location=ll167\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '77'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6701",
						"y": "0.5058",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll166","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc9/b32/thumb_1144_268_231_0_0_auto.png","title": "Colette",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Colette</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/colette?location=ll166\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '78'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4095",
						"y": "0.4286",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll11","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bca/507/thumb_1146_268_231_0_0_auto.png","title": "Computicket",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Computicket</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/computicket?location=ll11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '80'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2808",
						"y": "0.3568",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll241","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bca/b49/thumb_1147_268_231_0_0_auto.png","title": "Coricraft",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Coricraft</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/coricraft?location=ll241\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '81'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4229",
						"y": "0.5058",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll85","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bca/ef6/thumb_1148_268_231_0_0_auto.png","title": "Cotton On",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Cotton On</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cotton-on?location=ll85\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '82'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6109",
						"y": "0.2897",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll33","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bce/c41/thumb_1153_268_231_0_0_auto.png","title": "CUM Books",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">CUM Books</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cum-books?location=ll33\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '87'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3926",
						"y": "0.3425",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bcf/5e3/thumb_1155_268_231_0_0_auto.png","title": "Debonairs Pizza",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Debonairs Pizza</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/debonairs-pizza?location=llFC3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '88'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5298",
						"y": "0.2698",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll20","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bcf/c7f/thumb_1156_268_231_0_0_auto.png","title": "Decor East Persian Carpets",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Decor East Persian Carpets</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/decor-east-persian-carpets?location=ll20\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '89'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3862",
						"y": "0.381",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll109","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd4/b5e/thumb_1163_268_231_0_0_auto.png","title": "Dion Wired",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Dion Wired</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/dion-wired?location=ll109\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '92'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7432",
						"y": "0.306",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll37","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd7/f8b/thumb_1164_268_231_0_0_auto.png","title": "Dis-Chem",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Dis-Chem</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/dis-chem?location=ll37\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '93'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4309",
						"y": "0.3423",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll107","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd8/896/thumb_1165_268_231_0_0_auto.png","title": "Divas Nails",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Divas Nails</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/divas-nails?location=ll107\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '94'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7257",
						"y": "0.3456",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll48A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bdd/7db/thumb_1167_268_231_0_0_auto.png","title": "Donut Centre",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Donut Centre</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/donut-centre?location=ll48A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '96'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5295",
						"y": "0.3607",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bdd/e30/thumb_1168_268_231_0_0_auto.png","title": "Dress Up",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Dress Up</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/dress?location=llB7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '97'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7154",
						"y": "0.4198",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll161","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bde/c31/thumb_1170_268_231_0_0_auto.png","title": "Due South",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Due South</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/due-south?location=ll161\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '99'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6813",
						"y": "0.4929",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll201","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bdf/10b/thumb_1171_268_231_0_0_auto.png","title": "Dune",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Dune</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/dune?location=ll201\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '100'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6163",
						"y": "0.4793",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll89","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bdf/952/thumb_1172_268_231_0_0_auto.png","title": "Dunkin' Donuts",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Dunkin' Donuts</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/dunkin-donuts?location=ll89\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '101'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6123",
						"y": "0.3333",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll261","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/be0/063/thumb_1173_268_231_0_0_auto.png","title": "Earthchild",
						"pin": "blue","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion</h4><h3 class=\"mapplic-map-store-name\">Earthchild</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/earthchild?location=ll261\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '102'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2828",
						"y": "0.5867",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llM2","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/be1/ee5/thumb_1174_268_231_0_0_auto.png","title": "EB Cafe",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">EB Cafe</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/eb-cafe?location=llM2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '103'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6642",
						"y": "0.3997",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll151","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/be2/4e9/thumb_1175_268_231_0_0_auto.png","title": "Edgars",
						"pin": "blue","category": "Department Stores","description": "<h4 class=\"mapplic-map-store-category\">Department Stores</h4><h3 class=\"mapplic-map-store-name\">Edgars</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/edgars?location=ll151\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '104'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7617",
						"y": "0.5252",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll117","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d24/166/thumb_1177_268_231_0_0_auto.png","title": "Elna",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Elna</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/elna?location=ll117\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '107'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7684",
						"y": "0.3544",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB18","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d25/947/thumb_1178_268_231_0_0_auto.png","title": "Eufrasia",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Eufrasia</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/eufrasia?location=llB18\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '108'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7254",
						"y": "0.3849",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll70","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2d/aa9/thumb_1182_268_231_0_0_auto.png","title": "Exclusive Books",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">Exclusive Books</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/exclusive-books?location=ll70\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '112'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6615",
						"y": "0.3813",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB16","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2e/1c7/thumb_1184_268_231_0_0_auto.png","title": "Exotic Fragrances",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Exotic Fragrances</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/exotic-fragrances?location=llB16\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '114'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7252",
						"y": "0.3893",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "lla13","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2e/456/thumb_1185_268_231_0_0_auto.png","title": "Exotic Perfumes",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Exotic Perfumes</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/exotic-perfumes?location=lla13\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '115'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4017",
						"y": "0.4162",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll64","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d31/604/thumb_1190_268_231_0_0_auto.png","title": "Factorie",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Factorie</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/factorie?location=ll64\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '120'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6354",
						"y": "0.3714",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llM1A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d31/ae2/thumb_1191_268_231_0_0_auto.png","title": "Falco Milano",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Falco Milano</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/falco-milano?location=llM1A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '121'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4537",
						"y": "0.3524",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llfc2","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/e5d/ee7/thumb_1480_268_231_0_0_auto.png","title": "Fishaways",
						"pin": "blue","category": "Halaal","description": "<h4 class=\"mapplic-map-store-category\">Halaal</h4><h3 class=\"mapplic-map-store-name\">Fishaways</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fishaways?location=llfc2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '395'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5341",
						"y": "0.2518",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll139","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d32/57a/thumb_1193_268_231_0_0_auto.png","title": "FNB",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">FNB</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fnb-branch?location=ll139\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '124'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8057",
						"y": "0.4691",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll144","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/4f1/2f1/thumb_1442_268_231_0_0_auto.png","title": "Folli Follie",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Folli Follie</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/folli-follie?location=ll144\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '125'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5757",
						"y": "0.4155",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll30","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d33/89e/thumb_1195_268_231_0_0_auto.png","title": "Forever 21",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Forever 21</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/forever-21?location=ll30\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '127'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4471",
						"y": "0.4311",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll136","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4a/06b/thumb_1196_268_231_0_0_auto.png","title": "Forever New",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Forever New</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/forever-new?location=ll136\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '128'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6043",
						"y": "0.4159",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll213","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4a/d36/thumb_1197_268_231_0_0_auto.png","title": "Foschini",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Foschini</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/foschini?location=ll213\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '129'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5132",
						"y": "0.5004",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll207B","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4b/205/thumb_1198_268_231_0_0_auto.png","title": "Foschini Kids",
						"pin": "blue","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion</h4><h3 class=\"mapplic-map-store-name\">Foschini Kids</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/foschini-kids?location=ll207B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '130'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5295",
						"y": "0.4725",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll143","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4b/db5/thumb_1201_268_231_0_0_auto.png","title": "Foxglove Studio",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Foxglove Studio</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/foxglove-studio?location=ll143\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '389'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8112",
						"y": "0.4377",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll130","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4c/36b/thumb_1203_268_231_0_0_auto.png","title": "Frasers",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">Frasers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/frasers?location=ll130\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '134'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6482",
						"y": "0.431",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB19","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4c/5f9/thumb_1204_268_231_0_0_auto.png","title": "Frozen Lemons",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Frozen Lemons</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/frozen-lemons?location=llB19\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '135'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7257",
						"y": "0.3782",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll164","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4c/d6a/thumb_1206_268_231_0_0_auto.png","title": "G-Star Raw",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">G-Star Raw</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/g-star-raw?location=ll164\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '136'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4209",
						"y": "0.4242",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll90","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4c/a3c/thumb_1205_268_231_0_0_auto.png","title": "Gadget Time",
						"pin": "blue","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies</h4><h3 class=\"mapplic-map-store-name\">Gadget Time</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/gadget-time?location=ll90\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '137'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7559",
						"y": "0.3825",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll273","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4d/026/thumb_1207_268_231_0_0_auto.png","title": "Galaxy & Co.",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Galaxy & Co.</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/galaxy-co?location=ll273\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '138'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3154",
						"y": "0.4605",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll126","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4d/e31/thumb_1209_268_231_0_0_auto.png","title": "Green Cross",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Green Cross</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/green-cross?location=ll126\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '142'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6778",
						"y": "0.4494",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llfc11","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8c/a50/thumb_1476_268_231_0_0_auto.png","title": "Guacamole",
						"pin": "blue","category": "Halaal","description": "<h4 class=\"mapplic-map-store-category\">Halaal</h4><h3 class=\"mapplic-map-store-name\">Guacamole</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/guacamole?location=llfc11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '391'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5349",
						"y": "0.2622",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll255","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4e/37b/thumb_1210_268_231_0_0_auto.png","title": "Guess",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Guess</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/guess?location=ll255\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '144'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.408",
						"y": "0.4822",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll205","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4e/7a5/thumb_1211_268_231_0_0_auto.png","title": "H&M",
						"pin": "blue","category": "Department Stores","description": "<h4 class=\"mapplic-map-store-category\">Department Stores</h4><h3 class=\"mapplic-map-store-name\">H&M</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/hm?location=ll205\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '145'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.559",
						"y": "0.5077",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll147","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d54/7a8/thumb_1212_268_231_0_0_auto.png","title": "H20 Water Bar",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">H20 Water Bar</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/h20-water-bar?location=ll147\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '146'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7982",
						"y": "0.4377",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll87","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d54/c69/thumb_1213_268_231_0_0_auto.png","title": "Haagen-Dazs",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Haagen-Dazs</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/haagen-dazs?location=ll87\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '147'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6004",
						"y": "0.3349",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll18","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d54/fcd/thumb_1214_268_231_0_0_auto.png","title": "Hair Freedom",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Hair Freedom</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/hair-freedom?location=ll18\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '148'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3782",
						"y": "0.3829",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll219","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d55/419/thumb_1215_268_231_0_0_auto.png","title": "Harris Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Harris Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/harris-jewellers?location=ll219\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '149'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.475",
						"y": "0.4769",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll144A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d55/783/thumb_1216_268_231_0_0_auto.png","title": "Havaianas",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Havaianas</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/havaianas?location=ll144A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '150'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5829",
						"y": "0.4214",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll142","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d56/40b/thumb_1217_268_231_0_0_auto.png","title": "Hepkers",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">Hepkers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/hepkers?location=ll142\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '151'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5748",
						"y": "0.3972",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll120","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d57/983/thumb_1218_268_231_0_0_auto.png","title": "Hi",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Hi</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/hi?location=ll120\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '152'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.709",
						"y": "0.4266",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll88","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5a/01c/thumb_1220_268_231_0_0_auto.png","title": "Homemark",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Homemark</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/homemark?location=ll88\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '154'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7462",
						"y": "0.3813",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll96","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5a/d1a/thumb_1222_268_231_0_0_auto.png","title": "Huawei",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Huawei</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/huawei?location=ll96\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '156'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7868",
						"y": "0.3873",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll134","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5c/ede/thumb_1225_268_231_0_0_auto.png","title": "Identity",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Identity</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/identity?location=ll134\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '159'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6347",
						"y": "0.4424",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA15","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5f/0f7/thumb_1226_268_231_0_0_auto.png","title": "Ilhaam's Flowers",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Ilhaam's Flowers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/ilhaams-flowers?location=llA15\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '160'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4008",
						"y": "0.3914",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll193","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5f/9b2/thumb_1227_268_231_0_0_auto.png","title": "Incredible Connection",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Incredible Connection</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/incredible-connection?location=ll193\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '161'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.632",
						"y": "0.5554",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll154","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5f/e78/thumb_1228_268_231_0_0_auto.png","title": "Inglot",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Inglot</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/inglot?location=ll154\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '162'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5101",
						"y": "0.4274",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFCk5","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d61/643/thumb_1231_268_231_0_0_auto.png","title": "Jakura Sushi Express",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Jakura Sushi Express</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jakura-sushi-express?location=llFCk5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '165'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.48",
						"y": "0.35",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll149","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8b/942/thumb_1473_268_231_0_0_auto.png","title": "JDC Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">JDC Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jdc-jewellers?location=ll149\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '166'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7898",
						"y": "0.4381",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB12","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d64/815/thumb_1232_268_231_0_0_auto.png","title": "Jessie's Chinese Massage",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Jessie's Chinese Massage</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/chinese-finger-press-massage?location=llB12\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '70'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.724",
						"y": "0.4222",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll159","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d64/ca5/thumb_1233_268_231_0_0_auto.png","title": "Jet",
						"pin": "blue","category": "Department Stores","description": "<h4 class=\"mapplic-map-store-category\">Department Stores</h4><h3 class=\"mapplic-map-store-name\">Jet</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jet?location=ll159\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '167'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7017",
						"y": "0.4858",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll71A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d65/5e0/thumb_1234_268_231_0_0_auto.png","title": "Jimmy's Killer Prawns",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Jimmy's Killer Prawns</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jimmys-killer-prawns?location=ll71A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '168'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5976",
						"y": "0.1829",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll189","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d68/ee3/thumb_1238_268_231_0_0_auto.png","title": "Joubert & Monty",
						"pin": "blue","category": "Speciality","description": "<h4 class=\"mapplic-map-store-category\">Speciality</h4><h3 class=\"mapplic-map-store-name\">Joubert & Monty</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/joubert-monty?location=ll189\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '172'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6523",
						"y": "0.5058",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llK37","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d69/c95/thumb_1239_268_231_0_0_auto.png","title": "Joubert & Monty Kiosk",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Joubert & Monty Kiosk</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/joubert-monty-kiosk?location=llK37\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '173'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5187",
						"y": "0.3484",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll95","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5a5/f3c/244/thumb_821_268_231_0_0_auto.png","title": "JT One",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">JT One</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jt-one?location=ll95\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '174'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6279",
						"y": "0.3266",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB20","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e2d/297/thumb_1240_268_231_0_0_auto.png","title": "K's Silver",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">K's Silver</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/ks-silver?location=llB20\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '175'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7248",
						"y": "0.3718",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll62","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e2d/9ea/thumb_1241_268_231_0_0_auto.png","title": "Kauai",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Kauai</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/kauai?location=ll62\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '176'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6159",
						"y": "0.369",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC5","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e2e/0b7/thumb_1243_268_231_0_0_auto.png","title": "KFC",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">KFC</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/kfc?location=llFC5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '178'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5326",
						"y": "0.221",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll9","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e2e/5c4/thumb_1244_268_231_0_0_auto.png","title": "King Pie",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">King Pie</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/king-pie?location=ll9\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '179'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2776",
						"y": "0.4004",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll137","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e2f/adb/thumb_1245_268_231_0_0_auto.png","title": "King Pie",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">King Pie</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/king-pie-2?location=ll137\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '180'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8429",
						"y": "0.4381",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll233","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e30/8a6/thumb_1246_268_231_0_0_auto.png","title": "Klinomat Dry Cleaners & Laundry",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Klinomat Dry Cleaners & Laundry</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/klinomat-dry-cleaners-laundry?location=ll233\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '182'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.482",
						"y": "0.579",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA14","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e31/097/thumb_1248_268_231_0_0_auto.png","title": "La Belle Jewellery",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">La Belle Jewellery</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/la-belle-jewellery?location=llA14\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '185'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.402",
						"y": "0.4167",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll10","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e32/955/thumb_1252_268_231_0_0_auto.png","title": "La Rocca",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">La Rocca</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/la-rocca?location=ll10\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '186'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3579",
						"y": "0.3828",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll176","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e34/3e0/thumb_1258_268_231_0_0_auto.png","title": "Le Creuset",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Le Creuset</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/le-creuset?location=ll176\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '191'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3587",
						"y": "0.4163",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA11","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e33/be9/thumb_1256_268_231_0_0_auto.png","title": "Leather Affair",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">Leather Affair</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/leather-affair?location=llA11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '192'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4023",
						"y": "0.4353",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll128","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e34/bf1/thumb_1259_268_231_0_0_auto.png","title": "Legit",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Legit</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/legit?location=ll128\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '193'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6629",
						"y": "0.4412",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll156","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e43/39b/thumb_1262_268_231_0_0_auto.png","title": "Limnos Bakery",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Limnos Bakery</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/limnos-bakery?location=ll156\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '196'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.502",
						"y": "0.4262",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll182","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e3e/9a0/thumb_1261_268_231_0_0_auto.png","title": "Lindt",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Lindt</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/lindt?location=ll182\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '197'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3315",
						"y": "0.4163",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll69","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e44/072/thumb_1265_268_231_0_0_auto.png","title": "Lounge Relax Play",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Lounge Relax Play</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/lounge-relax-play?location=ll69\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '199'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5234",
						"y": "0.179",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll265","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e44/526/thumb_1266_268_231_0_0_auto.png","title": "Lovisa",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Lovisa</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/lovisa?location=ll265\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '200'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3654",
						"y": "0.4701",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll263","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e44/9d4/thumb_1267_268_231_0_0_auto.png","title": "Lush Cosmetics",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Lush Cosmetics</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/lush-cosmetics?location=ll263\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '201'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3721",
						"y": "0.4701",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll8","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e6c/e8e/thumb_1268_268_231_0_0_auto.png","title": "Magents",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Magents</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/magents?location=ll8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '202'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3401",
						"y": "0.3877",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll50","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/775/4f9/thumb_1459_268_231_0_0_auto.png","title": "Magnifico Bistro",
						"pin": "blue","category": "Coffee Shops","description": "<h4 class=\"mapplic-map-store-category\">Coffee Shops</h4><h3 class=\"mapplic-map-store-name\">Magnifico Bistro</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/magnifico-bistro?location=ll50\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '203'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5357",
						"y": "0.3861",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA18","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e6d/543/thumb_1269_268_231_0_0_auto.png","title": "Malaikah Curios",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">Malaikah Curios</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/malaikah-curios?location=llA18\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '204'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.402",
						"y": "0.3821",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll48","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e6e/157/thumb_1271_268_231_0_0_auto.png","title": "Marcel's Frozen Yoghurt",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Marcel's Frozen Yoghurt</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/marcels-frozen-yoghurt?location=ll48\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '205'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5332",
						"y": "0.373",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll158","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e6e/56e/thumb_1272_268_231_0_0_auto.png","title": "Marios Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Marios Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/marios-jewellers?location=ll158\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '206'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4884",
						"y": "0.4302",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA20","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e6e/c7d/thumb_1273_268_231_0_0_auto.png","title": "Market Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Market Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/market-jewellers?location=llA20\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '207'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4032",
						"y": "0.369",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llA4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e72/2e4/thumb_1275_268_231_0_0_auto.png","title": "Massai Mara Curios",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">Massai Mara Curios</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/massai-mara-curios?location=llA4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '209'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.392",
						"y": "0.3909",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC6","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e72/e89/thumb_1277_268_231_0_0_auto.png","title": "McCafe",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">McCafe</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mccafe?location=llFC6\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '211'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5858",
						"y": "0.185",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC6","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e73/2e8/thumb_1278_268_231_0_0_auto.png","title": "McDonalds",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">McDonalds</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mcdonalds?location=llFC6\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '212'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5865",
						"y": "0.219",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll155","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e74/406/thumb_1281_268_231_0_0_auto.png","title": "Miladys",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Miladys</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/miladys?location=ll155\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '215'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7288",
						"y": "0.4822",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llfc1","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/e56/720/thumb_1479_268_231_0_0_auto.png","title": "Milky Lane",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Milky Lane</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/milky-lane?location=llfc1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '394'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.535",
						"y": "0.2617",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "lla3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e75/3d4/thumb_1283_268_231_0_0_auto.png","title": "Mombasa",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Mombasa</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mombasa?location=lla3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '216'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3",
						"y": "0.4",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll191","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e75/c5e/thumb_1284_268_231_0_0_auto.png","title": "Montagu",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Montagu</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/montagu?location=ll191\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '217'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6533",
						"y": "0.4952",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll14A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/540/c40/thumb_1443_268_231_0_0_auto.png","title": "Move With Time",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Move With Time</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>0</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/move-time?location=ll14A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '218'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2865",
						"y": "0.4667",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll101","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e78/549/thumb_1289_268_231_0_0_auto.png","title": "MTN",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">MTN</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mtn?location=ll101\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '223'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6818",
						"y": "0.3294",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll221","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e79/0ad/thumb_1290_268_231_0_0_auto.png","title": "Mugg & Bean",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Mugg & Bean</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mugg-bean?location=ll221\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '224'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4663",
						"y": "0.4928",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll115","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e79/c7c/thumb_1292_268_231_0_0_auto.png","title": "Mullers Optometrists",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Mullers Optometrists</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mullers-optometrists?location=ll115\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '226'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7587",
						"y": "0.3484",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll261","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e7b/8d4/thumb_1295_268_231_0_0_auto.png","title": "Naartjie",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Naartjie</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>0</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/naartjie?location=ll261\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '229'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3811",
						"y": "0.4735",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll45","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e7e/060/thumb_1296_268_231_0_0_auto.png","title": "Nail Fantasy",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Nail Fantasy</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nail-fantasy?location=ll45\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '230'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4577",
						"y": "0.3199",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC10","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e7e/676/thumb_1297_268_231_0_0_auto.png","title": "Nando's",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Nando's</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nandos?location=llFC10\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '231'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.582",
						"y": "0.3028",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll227","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e7e/a75/thumb_1298_268_231_0_0_auto.png","title": "Nedbank",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">Nedbank</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nedbank-branch?location=ll227\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '232'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4794",
						"y": "0.5342",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll271","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/541/3f5/thumb_1444_268_231_0_0_auto.png","title": "NWJ Jewellery",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">NWJ Jewellery</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>0</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nwj-jewellery?location=ll271\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '241'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3231",
						"y": "0.4638",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll92","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eab/ce3/thumb_1307_268_231_0_0_auto.png","title": "Ocean Basket",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Ocean Basket</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/ocean-basket?location=ll92\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '244'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.768",
						"y": "0.4272",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll203","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eac/5ae/thumb_1308_268_231_0_0_auto.png","title": "Office London",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Office London</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/office-london?location=ll203\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '245'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6001",
						"y": "0.5029",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll60","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eac/aa8/thumb_1309_268_231_0_0_auto.png","title": "Old Khaki",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Old Khaki</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/old-khaki?location=ll60\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '246'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6023",
						"y": "0.3726",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll123","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f94/8ed/thumb_1318_268_231_0_0_auto.png","title": "Peacock Tea & Coffee",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Peacock Tea & Coffee</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/peacock-tea-coffee?location=ll123\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '255'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8098",
						"y": "0.3627",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll177A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f95/42b/thumb_1319_268_231_0_0_auto.png","title": "Pentravel",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Pentravel</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pentravel?location=ll177A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '257'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6645",
						"y": "0.5667",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll129","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f96/f09/thumb_1322_268_231_0_0_auto.png","title": "Pick n Pay",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Pick n Pay</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pick-n-pay?location=ll129\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '258'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8717",
						"y": "0.4103",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll1","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f98/e14/thumb_1323_268_231_0_0_auto.png","title": "Pick n Pay Clothing",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Pick n Pay Clothing</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pick-n-pay-clothing?location=ll1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '259'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2683",
						"y": "0.4494",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll131","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f99/94e/thumb_1324_268_231_0_0_auto.png","title": "Pick n Pay Liquor",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Pick n Pay Liquor</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pick-n-pay-liquor?location=ll131\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '260'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8459",
						"y": "0.4163",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll275","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9a/1de/thumb_1325_268_231_0_0_auto.png","title": "Pick n Pay Liquor",
						"pin": "blue","category": "Liquor","description": "<h4 class=\"mapplic-map-store-category\">Liquor</h4><h3 class=\"mapplic-map-store-name\">Pick n Pay Liquor</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pick-n-pay-liquor-1?location=ll275\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '261'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3019",
						"y": "0.4735",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll251","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9a/6a3/thumb_1326_268_231_0_0_auto.png","title": "Poetry",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Poetry</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/poetry?location=ll251\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '263'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4297",
						"y": "0.4826",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll140","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9b/40a/thumb_1329_268_231_0_0_auto.png","title": "Prime Time",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Prime Time</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/prime-time?location=ll140\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '266'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5918",
						"y": "0.4119",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll257","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b2/372/04d/thumb_1488_268_231_0_0_auto.png","title": "Ray-Ban",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Ray-Ban</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/ray-ban?location=ll257\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '396'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3944",
						"y": "0.4537",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll19","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc3/4b6/thumb_1336_268_231_0_0_auto.png","title": "Refinery",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Refinery</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/refinery?location=ll19\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '273'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3259",
						"y": "0.352",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll38","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fec/e10/thumb_1347_268_231_0_0_auto.png","title": "Samsung",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Samsung</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/samsung?location=ll38\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '283'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5045",
						"y": "0.375",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC2","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/4d9/a4d/thumb_1440_268_231_0_0_auto.png","title": "Sausage Saloon",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Sausage Saloon</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sausage-saloon?location=llFC2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '284'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5343",
						"y": "0.2806",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll97","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/4d9/702/thumb_1439_268_231_0_0_auto.png","title": "Sheet Street",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Sheet Street</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sheet-street?location=ll97\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '285'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6437",
						"y": "0.3194",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll119","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/4da/0f9/thumb_1441_268_231_0_0_auto.png","title": "Shoe City",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Shoe City</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/shoe-city?location=ll119\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '288'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7868",
						"y": "0.3448",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll199","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15f/27a/thumb_1435_268_231_0_0_auto.png","title": "Shoe Connection",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Shoe Connection</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/shoe-connection?location=ll199\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '289'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6274",
						"y": "0.4759",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll157","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15e/fea/thumb_1434_268_231_0_0_auto.png","title": "Shoerama",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Shoerama</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/shoerama?location=ll157\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '290'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7129",
						"y": "0.4687",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llfck7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15e/be1/thumb_1433_268_231_0_0_auto.png","title": "Simply Asia",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Simply Asia</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/simply-asia?location=llfck7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '388'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.55",
						"y": "0.17",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll37B","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8c/195/thumb_1475_268_231_0_0_auto.png","title": "Simply Natural",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Simply Natural</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/simply-natural?location=ll37B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '292'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4212",
						"y": "0.3353",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll253","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15d/c16/thumb_1432_268_231_0_0_auto.png","title": "Sissy Boy",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Sissy Boy</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sissy-boy?location=ll253\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '293'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4229",
						"y": "0.4759",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll84","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/158/54f/thumb_1430_268_231_0_0_auto.png","title": "Smart Spaces",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Smart Spaces</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/smart-spaces?location=ll84\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '295'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7323",
						"y": "0.3786",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll239","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/157/a4c/thumb_1428_268_231_0_0_auto.png","title": "Sorbet Face & Body Professionals",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Sorbet Face & Body Professionals</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sorbet-face-body-professionals-1?location=ll239\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '297'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4507",
						"y": "0.5429",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/156/c20/thumb_1426_268_231_0_0_auto.png","title": "Soviet",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Soviet</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/soviet?location=ll4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '299'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3329",
						"y": "0.3869",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll114","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/156/5d9/thumb_1425_268_231_0_0_auto.png","title": "Spec-Savers",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Spec-Savers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/spec-savers?location=ll114\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '300'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7494",
						"y": "0.4129",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll112","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/155/925/thumb_1422_268_231_0_0_auto.png","title": "Splush",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Splush</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/splush?location=ll112\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '303'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7595",
						"y": "0.4167",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC8","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/14f/f81/thumb_1418_268_231_0_0_auto.png","title": "Steers",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Steers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/steers?location=llFC8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '307'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5829",
						"y": "0.2798",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll21","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/4d9/2e7/thumb_1438_268_231_0_0_auto.png","title": "Street Fever",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Street Fever</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/street-fever?location=ll21\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '310'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3443",
						"y": "0.3476",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll24","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/14e/e66/thumb_1415_268_231_0_0_auto.png","title": "Style Studio",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Style Studio</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/style-studio?location=ll24\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '311'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4218",
						"y": "0.375",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/11e/57a/thumb_1413_268_231_0_0_auto.png","title": "Subway",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Subway</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/subway?location=llFC7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '312'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5865",
						"y": "0.2683",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll93","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/11d/cf3/thumb_1412_268_231_0_0_auto.png","title": "Sunglass Hut",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Sunglass Hut</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sunglass-hut-2?location=ll93\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '314'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6218",
						"y": "0.329",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll168","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/115/707/thumb_1410_268_231_0_0_auto.png","title": "Superdry",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Superdry</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/superdry?location=ll168\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '316'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3854",
						"y": "0.4151",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll14A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/114/637/thumb_1409_268_231_0_0_auto.png","title": "Superfoto",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Superfoto</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/superfoto?location=ll14A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '317'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3659",
						"y": "0.3901",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll148","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/0ee/d60/thumb_1407_268_231_0_0_auto.png","title": "Swarovski",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Swarovski</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/swarovski?location=ll148\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '319'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5437",
						"y": "0.4171",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB5","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/0ec/8e1/thumb_1405_268_231_0_0_auto.png","title": "Sweet Centre",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Sweet Centre</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sweet-centre?location=llB5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '321'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.717",
						"y": "0.4044",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll61","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/0ec/59e/thumb_1404_268_231_0_0_auto.png","title": "Sweets From Heaven",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Sweets From Heaven</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sweets-heaven?location=ll61\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '322'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5173",
						"y": "0.3341",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llB16","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffe/cdf/thumb_1400_268_231_0_0_auto.png","title": "TeAwesome Tea & Smoothie Bar",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">TeAwesome Tea & Smoothie Bar</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/teawesome-tea-smoothie-bar?location=llB16\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '325'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7268",
						"y": "0.4036",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll193","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffe/977/thumb_1399_268_231_0_0_auto.png","title": "Tekkie Town",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Tekkie Town</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tekkie-town?location=ll193\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '326'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6345",
						"y": "0.5318",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll133","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffd/412/thumb_1397_268_231_0_0_auto.png","title": "Telkom",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Telkom</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/telkom?location=ll133\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '327'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.837",
						"y": "0.423",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll76","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffe/539/thumb_1398_268_231_0_0_auto.png","title": "Telkom Direct",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Telkom Direct</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/telkom-direct?location=ll76\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '328'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6884",
						"y": "0.3742",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll78","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffc/e46/thumb_1396_268_231_0_0_auto.png","title": "Tempur",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Tempur</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tempur?location=ll78\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '329'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6984",
						"y": "0.375",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll174","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/775/792/thumb_1460_268_231_0_0_auto.png","title": "The Body Shop",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">The Body Shop</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/body-shop?location=ll174\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '330'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3662",
						"y": "0.4155",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll125","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffc/55e/thumb_1395_268_231_0_0_auto.png","title": "The Cock 'n Bull",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">The Cock 'n Bull</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cock-n-bull?location=ll125\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '331'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8157",
						"y": "0.371",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll27","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffb/fce/thumb_1394_268_231_0_0_auto.png","title": "The Cross Trainer",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">The Cross Trainer</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cross-trainer?location=ll27\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '332'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3682",
						"y": "0.3444",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffa/aa4/thumb_1389_268_231_0_0_auto.png","title": "The Tobacconist",
						"pin": "blue","category": "Tobacconists & E-Cigarettes","description": "<h4 class=\"mapplic-map-store-category\">Tobacconists & E-Cigarettes</h4><h3 class=\"mapplic-map-store-name\">The Tobacconist</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tobacconist?location=ll7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '337'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2812",
						"y": "0.4091",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll179","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffa/679/thumb_1388_268_231_0_0_auto.png","title": "Tidy Tuc's Tailors",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Tidy Tuc's Tailors</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tidy-tucs-tailors?location=ll179\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '338'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6659",
						"y": "0.5798",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff9/297/thumb_1384_268_231_0_0_auto.png","title": "Tong Lok",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Tong Lok</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tong-lok?location=llFC4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '342'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5354",
						"y": "0.2583",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll151","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff8/fdc/thumb_1383_268_231_0_0_auto.png","title": "Topman",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Topman</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/topman?location=ll151\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '343'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7636",
						"y": "0.5227",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll217","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff4/f60/thumb_1381_268_231_0_0_auto.png","title": "Totalsports",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Totalsports</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/totalsports?location=ll217\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '345'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4837",
						"y": "0.4905",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll160","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff4/827/thumb_1380_268_231_0_0_auto.png","title": "Totalsports Women",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Totalsports Women</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/totalsports-women?location=ll160\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '382'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4781",
						"y": "0.4426",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll65","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff4/24e/thumb_1378_268_231_0_0_auto.png","title": "Toys R Us & Babies R Us",
						"pin": "blue","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies</h4><h3 class=\"mapplic-map-store-name\">Toys R Us & Babies R Us</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/toys-r-us-babies-r-us?location=ll65\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '347'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4965",
						"y": "0.2651",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll237","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff3/efa/thumb_1377_268_231_0_0_auto.png","title": "Travelex",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">Travelex</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/travelex?location=ll237\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '348'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4532",
						"y": "0.5651",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll207A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff3/bb3/thumb_1376_268_231_0_0_auto.png","title": "Tread+Miller",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Tread+Miller</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>2</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/treadmiller?location=ll207A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '349'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5392",
						"y": "0.4701",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llK3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff3/86d/thumb_1375_268_231_0_0_auto.png","title": "Trollbeads",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Trollbeads</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/trollbeads?location=llK3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '350'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5598",
						"y": "0.419",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll37A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff2/d35/thumb_1371_268_231_0_0_auto.png","title": "Twisp",
						"pin": "blue","category": "Tobacconists & E-Cigarettes","description": "<h4 class=\"mapplic-map-store-category\">Tobacconists & E-Cigarettes</h4><h3 class=\"mapplic-map-store-name\">Twisp</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/twisp?location=ll37A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '353'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4112",
						"y": "0.3381",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll104","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff2/acd/thumb_1370_268_231_0_0_auto.png","title": "Typo",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">Typo</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/typo?location=ll104\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '354'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.782",
						"y": "0.4143",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll25","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff1/b72/thumb_1367_268_231_0_0_auto.png","title": "VapeShop",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">VapeShop</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/vapeshop?location=ll25\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '357'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3579",
						"y": "0.3548",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll86","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff1/923/thumb_1366_268_231_0_0_auto.png","title": "Verimark",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Verimark</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/verimark?location=ll86\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '358'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7395",
						"y": "0.3806",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll118","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff1/3f1/thumb_1364_268_231_0_0_auto.png","title": "Vodacom Shop Repairs",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Vodacom Shop Repairs</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/vodacom-shop-repairs?location=ll118\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '360'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7343",
						"y": "0.4139",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llFC1-7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff0/bc0/thumb_1361_268_231_0_0_auto.png","title": "Waffle Mania",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Waffle Mania</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/waffle-mania?location=llFC1-7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '363'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.48",
						"y": "0.39",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll82","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff0/7fe/thumb_1360_268_231_0_0_auto.png","title": "weFix",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">weFix</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/wefix?location=ll82\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '364'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7087",
						"y": "0.3742",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll2","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fef/eca/thumb_1357_268_231_0_0_auto.png","title": "Wimpy",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Wimpy</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/wimpy?location=ll2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '367'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3116",
						"y": "0.4014",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll73","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fee/227/thumb_1354_268_231_0_0_auto.png","title": "Wonderland",
						"pin": "blue","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment</h4><h3 class=\"mapplic-map-store-name\">Wonderland</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/wonderland?location=ll73\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '370'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6295",
						"y": "0.2448",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll267","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fee/002/thumb_1353_268_231_0_0_auto.png","title": "Woolworths",
						"pin": "blue","category": "Department Stores","description": "<h4 class=\"mapplic-map-store-category\">Department Stores</h4><h3 class=\"mapplic-map-store-name\">Woolworths</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/woolworths?location=ll267\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '371'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3496",
						"y": "0.5431",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "llk01","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fed/2c0/thumb_1348_268_231_0_0_auto.png","title": "Yummi",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Yummi</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/yummi?location=llk01\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '376'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "-33.8926",
						"y": "18.5086",
						"fill": "#ffffff",
						"zoom": "2"
					},
				]
			},{
				"id": "ML",
				"title": "Mezzanine Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5a8/d89/b0c/5a8d89b0c85d1773211086.svg","locations": [
					{
						"id": "m-centurycarting","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd0/c67/thumb_1158_268_231_0_0_auto.png","title": "Century Karting",
						"pin": "blue","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment</h4><h3 class=\"mapplic-map-store-name\">Century Karting</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/century-karting?location=m-centurycarting\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '65'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2871",
						"y": "0.7119",
						"fill": "#ffffff",
						"zoom": "2"
					},
				]
			},{
				"id": "UL",
				"title": "Upper Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5ab/cae/e46/5abcaee46c131285264564.svg","locations": [
					{
						"id": "ul3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/687/e20/thumb_1072_268_231_0_0_auto.png","title": "@homelivingspace",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">@homelivingspace</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/homelivingspace?location=ul3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '3'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5079",
						"y": "0.8214",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul627","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/688/2d8/thumb_1073_268_231_0_0_auto.png","title": "ABSA Bank",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">ABSA Bank</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/absa-bank?location=ul627\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '4'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4802",
						"y": "0.5088",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul407a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/688/c81/thumb_1077_268_231_0_0_auto.png","title": "Ackermans",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Ackermans</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/ackermans?location=ul407a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '8'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2764",
						"y": "0.325",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul452","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/689/584/thumb_1078_268_231_0_0_auto.png","title": "Adidas",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Adidas</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/adidas?location=ul452\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '9'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5188",
						"y": "0.4277",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul669","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/689/af2/thumb_1080_268_231_0_0_auto.png","title": "Adidas Originals",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Adidas Originals</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/adidas-originals?location=ul669\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '11'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5958",
						"y": "0.2966",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul609","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/531/thumb_1085_268_231_0_0_auto.png","title": "American Swiss",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">American Swiss</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/american-swiss?location=ul609\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '15'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5345",
						"y": "0.474",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulK1","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/773/835/thumb_1458_268_231_0_0_auto.png","title": "American Swiss Kiosk",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">American Swiss Kiosk</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/american-swiss-kiosk?location=ulK1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '16'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4657",
						"y": "0.4921",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul472","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/a0b/thumb_1087_268_231_0_0_auto.png","title": "Archive",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Archive</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/archive?location=ul472\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '18'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6918",
						"y": "0.4315",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul497","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68a/cc7/thumb_1088_268_231_0_0_auto.png","title": "Asics",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Asics</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/asics?location=ul497\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '19'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6179",
						"y": "0.3274",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul629","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68b/27a/thumb_1090_268_231_0_0_auto.png","title": "Bargain Books",
						"pin": "blue","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts</h4><h3 class=\"mapplic-map-store-name\">Bargain Books</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bargain-books?location=ul629\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '21'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4796",
						"y": "0.5534",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul553","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/68d/9bb/thumb_1094_268_231_0_0_auto.png","title": "Beds From Home",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Beds From Home</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/beds-home?location=ul553\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '26'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8383",
						"y": "0.4123",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul637","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7a2/eb1/thumb_1098_268_231_0_0_auto.png","title": "Bidvest Bank",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">Bidvest Bank</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bidvest-bank?location=ul637\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '29'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4499",
						"y": "0.5518",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul545","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd2/a11/thumb_1160_268_231_0_0_auto.png","title": "Blockhouse",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Blockhouse</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/blockhouse?location=ul545\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '33'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.816",
						"y": "0.3599",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul619","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7a4/56e/thumb_1101_268_231_0_0_auto.png","title": "Blue Collar White Collar",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Blue Collar White Collar</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/blue-collar-white-collar?location=ul619\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '34'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.467",
						"y": "0.4855",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul436","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ac/809/thumb_1103_268_231_0_0_auto.png","title": "Boardriders",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Boardriders</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/boardriders?location=ul436\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '36'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5112",
						"y": "0.3633",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul603","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ac/afe/thumb_1104_268_231_0_0_auto.png","title": "Bobbi Brown",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Bobbi Brown</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bobbi-brown?location=ul603\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '37'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6531",
						"y": "0.4904",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul615","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ad/6de/thumb_1106_268_231_0_0_auto.png","title": "Bogart Man",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Bogart Man</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bogart-man?location=ul615\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '39'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4872",
						"y": "0.4797",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul6a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ad/9da/thumb_1107_268_231_0_0_auto.png","title": "Bride & Co.",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Bride & Co.</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/bride-co?location=ul6a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '40'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.791",
						"y": "0.8473",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul544","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7ae/a34/thumb_1109_268_231_0_0_auto.png","title": "Browns The Diamond Store",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Browns The Diamond Store</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/browns-diamond-store?location=ul544\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '41'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5158",
						"y": "0.4143",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul575","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5a5/f24/b54/thumb_789_268_231_0_0_auto.png","title": "Build A Bear",
						"pin": "blue","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies</h4><h3 class=\"mapplic-map-store-name\">Build A Bear</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/build-bear?location=ul575\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '44'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7113",
						"y": "0.4657",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul481","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/7b0/180/thumb_1112_268_231_0_0_auto.png","title": "Burger King",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Burger King</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/burger-king?location=ul481\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '45'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5248",
						"y": "0.2458",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul657","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bbd/e6d/thumb_1120_268_231_0_0_auto.png","title": "Cafe Coton",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Cafe Coton</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cafe-coton?location=ul657\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '46'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3657",
						"y": "0.4711",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul485","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bbe/4c4/thumb_1121_268_231_0_0_auto.png","title": "Cape Town Fish Market",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Cape Town Fish Market</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cape-town-fish-market?location=ul485\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '48'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5853",
						"y": "0.2157",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bbf/b8e/thumb_1123_268_231_0_0_auto.png","title": "Cape Union Mart Adventure Centre",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Cape Union Mart Adventure Centre</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cape-union-mart-adventure-centre?location=ul4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '51'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6053",
						"y": "0.8421",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul655","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc0/486/thumb_1126_268_231_0_0_auto.png","title": "Carducci",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Carducci</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/carducci?location=ul655\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '54'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.384",
						"y": "0.4769",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul416A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc0/71b/thumb_1127_268_231_0_0_auto.png","title": "Carlton Hair",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Carlton Hair</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/carlton-hair?location=ul416A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '55'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.392",
						"y": "0.3713",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulC7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc2/1de/thumb_1130_268_231_0_0_auto.png","title": "Cell C",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cell C</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cell-c-1?location=ulC7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '58'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3059",
						"y": "0.4055",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul633","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc3/966/thumb_1135_268_231_0_0_auto.png","title": "Cellular Village",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cellular Village</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cellular-village-2?location=ul633\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '64'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4524",
						"y": "0.578",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul575","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bc5/b07/thumb_1137_268_231_0_0_auto.png","title": "Cherry Melon",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Cherry Melon</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cherry-melon?location=ul575\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '69'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7106",
						"y": "0.4783",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul641","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bca/038/thumb_1145_268_231_0_0_auto.png","title": "Computer Mania",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Computer Mania</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/computer-mania?location=ul641\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '79'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4421",
						"y": "0.524",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul462a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bcb/260/thumb_1149_268_231_0_0_auto.png","title": "Crocs",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Crocs</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/crocs?location=ul462a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '83'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5651",
						"y": "0.4246",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul519","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bcb/5b1/thumb_1150_268_231_0_0_auto.png","title": "Crossover",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Crossover</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/crossover?location=ul519\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '84'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7033",
						"y": "0.3351",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul635","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bcb/8d0/thumb_1151_268_231_0_0_auto.png","title": "Cruise About",
						"pin": "blue","category": "Travel","description": "<h4 class=\"mapplic-map-store-category\">Travel</h4><h3 class=\"mapplic-map-store-name\">Cruise About</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cruise-about?location=ul635\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '85'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4496",
						"y": "0.5685",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul554","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bce/5e4/thumb_1152_268_231_0_0_auto.png","title": "CSquared",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">CSquared</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/csquared?location=ul554\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '86'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.44",
						"y": "0.43",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulL4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bcf/098/thumb_1154_268_231_0_0_auto.png","title": "Cybertronics Cell",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Cybertronics Cell</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>12</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/cybertronics-cell?location=ulL4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '380'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2981",
						"y": "0.2946",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul478","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd4/110/thumb_1161_268_231_0_0_auto.png","title": "Deluxe Laser & Spa",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Deluxe Laser & Spa</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/deluxe-laser-spa?location=ul478\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '90'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7098",
						"y": "0.38",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul462","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd4/756/thumb_1162_268_231_0_0_auto.png","title": "Diesel",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Diesel</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/diesel?location=ul462\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '91'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.627",
						"y": "0.3595",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul213","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bdc/51d/thumb_1166_268_231_0_0_auto.png","title": "Donna",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Donna</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/donna?location=ul213\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '95'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5105",
						"y": "0.499",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul412","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8a/c1e/thumb_1470_268_231_0_0_auto.png","title": "Edge for Men",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Edge for Men</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/edge-men?location=ul412\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '105'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3717",
						"y": "0.3919",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul597","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d23/4db/thumb_1176_268_231_0_0_auto.png","title": "Elegance Fabrics",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Elegance Fabrics</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/elegance-fabrics?location=ul597\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '106'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6335",
						"y": "0.559",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul649","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d27/423/thumb_1179_268_231_0_0_auto.png","title": "Europa Art",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Europa Art</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/europa-art?location=ul649\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '109'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4322",
						"y": "0.4851",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul6a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2d/3cc/thumb_1180_268_231_0_0_auto.png","title": "Eurosuit",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Eurosuit</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/eurosuit?location=ul6a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '110'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7887",
						"y": "0.8634",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul425","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2d/717/thumb_1181_268_231_0_0_auto.png","title": "Exact",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Exact</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/exact?location=ul425\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '111'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3746",
						"y": "0.3113",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul510a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2d/e6b/thumb_1183_268_231_0_0_auto.png","title": "Execuspecs",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Execuspecs</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/execuspecs?location=ul510a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '113'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7209",
						"y": "0.4171",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul625","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2e/851/thumb_1186_268_231_0_0_auto.png","title": "Experimax",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Experimax</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/experimax?location=ul625\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '116'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4657",
						"y": "0.5036",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul607","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d2e/ac9/thumb_1187_268_231_0_0_auto.png","title": "Eye Q",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Eye Q</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/eye-q?location=ul607\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '117'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6329",
						"y": "0.487",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul651A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d30/d2c/thumb_1188_268_231_0_0_auto.png","title": "Fabiani",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Fabiani</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fabiani?location=ul651A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '118'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4205",
						"y": "0.475",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul550A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d31/262/thumb_1189_268_231_0_0_auto.png","title": "Fabiani Women",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">Fabiani Women</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fabiani-women?location=ul550A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '119'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4679",
						"y": "0.4083",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul5b","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5a5/f2b/0d0/thumb_807_268_231_0_0_auto.png","title": "Fielli",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Fielli</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fielli?location=ul5b\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '122'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7129",
						"y": "0.8369",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul591","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d32/11a/thumb_1192_268_231_0_0_auto.png","title": "Flight Centre",
						"pin": "blue","category": "Travel","description": "<h4 class=\"mapplic-map-store-category\">Travel</h4><h3 class=\"mapplic-map-store-name\">Flight Centre</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/flight-centre?location=ul591\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '123'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6675",
						"y": "0.5623",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul6b","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d33/5d3/thumb_1194_268_231_0_0_auto.png","title": "Footgear",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Footgear</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/footgear?location=ul6b\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '126'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8268",
						"y": "0.8573",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul532","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4b/6d5/thumb_1199_268_231_0_0_auto.png","title": "Fossil",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Fossil</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fossil?location=ul532\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '131'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5733",
						"y": "0.4099",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul424","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4b/afa/thumb_1200_268_231_0_0_auto.png","title": "Fox",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Fox</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fox?location=ul424\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '132'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4319",
						"y": "0.3796",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulK38","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4c/009/thumb_1202_268_231_0_0_auto.png","title": "Fragrance Boutique",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Fragrance Boutique</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fragrance-boutique?location=ulK38\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '133'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7583",
						"y": "0.4244",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul549","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8a/f75/thumb_1471_268_231_0_0_auto.png","title": "Game",
						"pin": "blue","category": "Department Stores","description": "<h4 class=\"mapplic-map-store-category\">Department Stores</h4><h3 class=\"mapplic-map-store-name\">Game</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/game?location=ul549\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '139'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.9257",
						"y": "0.4441",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul1","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d4d/91b/thumb_1208_268_231_0_0_auto.png","title": "Golfers Club",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Golfers Club</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/golfers-club?location=ul1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '141'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.308",
						"y": "0.8616",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul442","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8b/2c0/thumb_1472_268_231_0_0_auto.png","title": "Herschel Supply Co.",
						"pin": "blue","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage</h4><h3 class=\"mapplic-map-store-name\">Herschel Supply Co.</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/herschel-supply-co?location=ul442\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '392'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5286",
						"y": "0.3706",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul541","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d57/ff2/thumb_1219_268_231_0_0_auto.png","title": "Home etc.",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Home etc.</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/home-etc?location=ul541\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '153'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7901",
						"y": "0.3452",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul2","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5a/565/thumb_1221_268_231_0_0_auto.png","title": "Honda",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Honda</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/honda?location=ul2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '155'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4248",
						"y": "0.833",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul562","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5b/bfd/thumb_1223_268_231_0_0_auto.png","title": "Hugo Boss Menswear",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Hugo Boss Menswear</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/hugo-boss-menswear?location=ul562\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '157'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3839",
						"y": "0.4117",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul469A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d5c/49c/thumb_1224_268_231_0_0_auto.png","title": "Hurley",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Hurley</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/hurley?location=ul469A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '158'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.511",
						"y": "0.3029",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul526","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d60/4e7/thumb_1229_268_231_0_0_auto.png","title": "iStore",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">iStore</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/istore?location=ul526\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '163'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6008",
						"y": "0.4143",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul647","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d60/dc9/thumb_1230_268_231_0_0_auto.png","title": "Jack Friedman Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Jack Friedman Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jack-friedman-jewellers?location=ul647\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '164'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4419",
						"y": "0.4879",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul653","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d67/0ba/thumb_1236_268_231_0_0_auto.png","title": "Jo Malone",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Jo Malone</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/jo-malone?location=ul653\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '169'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.397",
						"y": "0.4759",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul483","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/d68/463/thumb_1237_268_231_0_0_auto.png","title": "John Dory's Fish & Grill Restaurant",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">John Dory's Fish & Grill Restaurant</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/john-dorys-fish-grill-restaurant?location=ul483\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '171'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5293",
						"y": "0.2192",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul560","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e2d/d46/thumb_1242_268_231_0_0_auto.png","title": "Keedo",
						"pin": "blue","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion</h4><h3 class=\"mapplic-map-store-name\">Keedo</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/keedo?location=ul560\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '177'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4092",
						"y": "0.4252",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ull7","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/7e8/843/thumb_1464_268_231_0_0_auto.png","title": "King Pie",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">King Pie</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>12</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/king-pie-3?location=ull7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '393'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3129",
						"y": "0.2622",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul548","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/a8b/b4d/thumb_1474_268_231_0_0_auto.png","title": "Kingsley Heath",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Kingsley Heath</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/kingsley-heath?location=ul548\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '181'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4874",
						"y": "0.4407",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul550B","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e30/cc6/thumb_1247_268_231_0_0_auto.png","title": "Kurt Geiger",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Kurt Geiger</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/kurt-geiger?location=ul550B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '183'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4782",
						"y": "0.4143",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul655A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e31/397/thumb_1249_268_231_0_0_auto.png","title": "L'Occitane en Provence",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">L'Occitane en Provence</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/loccitane-en-provence?location=ul655A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '184'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3751",
						"y": "0.455",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul605","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e32/be9/thumb_1253_268_231_0_0_auto.png","title": "La Senza",
						"pin": "blue","category": "Women's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Women's Fashion</h4><h3 class=\"mapplic-map-store-name\">La Senza</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/la-senza?location=ul605\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '187'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6455",
						"y": "0.4762",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul564","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e32/f50/thumb_1254_268_231_0_0_auto.png","title": "Lacoste",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Lacoste</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/lacoste?location=ul564\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '188'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3742",
						"y": "0.4125",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulL3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e32/0ff/thumb_1251_268_231_0_0_auto.png","title": "Lagoon Hair Salon",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Lagoon Hair Salon</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>12</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/lagoon-hair-salon?location=ulL3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '381'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2973",
						"y": "0.2893",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul495","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e34/07b/thumb_1257_268_231_0_0_auto.png","title": "Le Coq Sportif",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Le Coq Sportif</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/le-coq-sportif?location=ul495\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '190'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6075",
						"y": "0.328",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul501","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e34/fc6/thumb_1260_268_231_0_0_auto.png","title": "Levi's Store",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Levi's Store</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/levis-store?location=ul501\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '194'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6304",
						"y": "0.3244",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulk13","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e43/922/thumb_1263_268_231_0_0_auto.png","title": "Limnos Bakers Patisserie Boutique",
						"pin": "blue","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary</h4><h3 class=\"mapplic-map-store-name\">Limnos Bakers Patisserie Boutique</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/limnos-kiosk?location=ulk13\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '195'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.56",
						"y": "0.39",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul482","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e43/d43/thumb_1264_268_231_0_0_auto.png","title": "Loads of Living",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Loads of Living</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/loads-living?location=ul482\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '198'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.739",
						"y": "0.4036",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul468","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e71/968/thumb_1274_268_231_0_0_auto.png","title": "Markham",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Markham</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/markham?location=ul468\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '208'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6663",
						"y": "0.3879",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul518","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e72/7c7/thumb_1276_268_231_0_0_auto.png","title": "Mat & May",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Mat & May</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mat-may?location=ul518\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '210'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6773",
						"y": "0.4317",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul467","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e73/5d9/thumb_1279_268_231_0_0_auto.png","title": "Melissa",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Melissa</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/melissa?location=ul467\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '213'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4901",
						"y": "0.3288",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulR1","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e73/c5b/thumb_1280_268_231_0_0_auto.png","title": "Mexico Spur",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Mexico Spur</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mexico-spur?location=ulR1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '214'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5302",
						"y": "0.1924",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulL1","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e74/e36/thumb_1282_268_231_0_0_auto.png","title": "Mi Tobacco",
						"pin": "blue","category": "Tobacconists & E-Cigarettes","description": "<h4 class=\"mapplic-map-store-category\">Tobacconists & E-Cigarettes</h4><h3 class=\"mapplic-map-store-name\">Mi Tobacco</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>12</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mi-tobacco?location=ulL1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '384'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2979",
						"y": "0.2681",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul401","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e77/d8e/thumb_1287_268_231_0_0_auto.png","title": "Mr Price",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Mr Price</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mr-price?location=ul401\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '219'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2554",
						"y": "0.4097",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul535","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e77/602/thumb_1285_268_231_0_0_auto.png","title": "Mr Price Home",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Mr Price Home</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mr-price-home?location=ul535\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '220'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7746",
						"y": "0.3059",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul529","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e77/adb/thumb_1286_268_231_0_0_auto.png","title": "Mr Price Sport",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Mr Price Sport</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mr-price-sport?location=ul529\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '221'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7449",
						"y": "0.3246",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul663","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e78/143/thumb_1288_268_231_0_0_auto.png","title": "MTN",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">MTN</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>4</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mtn?location=ul663\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '222'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2982",
						"y": "0.4691",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulK5","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e79/75e/thumb_1291_268_231_0_0_auto.png","title": "Mugg & Bean On The Move",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Mugg & Bean On The Move</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/mugg-bean-move?location=ulK5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '225'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.75",
						"y": "0.46",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul651","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e7a/0f3/thumb_1293_268_231_0_0_auto.png","title": "Musica Mega Store",
						"pin": "blue","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment</h4><h3 class=\"mapplic-map-store-name\">Musica Mega Store</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/musica-mega-store?location=ul651\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '227'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4063",
						"y": "0.515",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul558","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e99/794/thumb_1299_268_231_0_0_auto.png","title": "Nespresso",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Nespresso</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nespresso?location=ul558\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '233'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4189",
						"y": "0.4407",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul668","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e9a/f23/thumb_1300_268_231_0_0_auto.png","title": "New Balance",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">New Balance</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/new-balance?location=ul668\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '234'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5223",
						"y": "0.2994",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul512","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e9b/c5c/thumb_1301_268_231_0_0_auto.png","title": "Next Step",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Next Step</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/next-step?location=ul512\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '236'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7084",
						"y": "0.426",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul422","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e9c/98a/thumb_1302_268_231_0_0_auto.png","title": "Nike",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Nike</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nike?location=ul422\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '237'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4192",
						"y": "0.3792",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul420","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e9d/1ea/thumb_1303_268_231_0_0_auto.png","title": "Nixon",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Nixon</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nixon?location=ul420\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '238'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6365",
						"y": "0.3804",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul404a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e9d/6cd/thumb_1304_268_231_0_0_auto.png","title": "Nu Health Food Cafe",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Nu Health Food Cafe</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nu-health-food-cafe?location=ul404a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '239'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3273",
						"y": "0.4028",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul479","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e9d/a02/thumb_1305_268_231_0_0_auto.png","title": "Nu Metro",
						"pin": "blue","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment</h4><h3 class=\"mapplic-map-store-name\">Nu Metro</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nu-metro?location=ul479\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '240'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5975",
						"y": "0.2692",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul579","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ea3/e09/thumb_1306_268_231_0_0_auto.png","title": "NYX Professional Makeup",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">NYX Professional Makeup</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/nyx-professional-makeup?location=ul579\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '242'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6779",
						"y": "0.4807",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul643","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eae/2a5/thumb_1310_268_231_0_0_auto.png","title": "Pak Persian Carpets",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Pak Persian Carpets</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pak-persian-carpets?location=ul643\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '247'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4371",
						"y": "0.5024",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul513","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eae/c5d/thumb_1311_268_231_0_0_auto.png","title": "Palladium Boots",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Palladium Boots</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/palladium-boots?location=ul513\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '248'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6733",
						"y": "0.325",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul487","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eaf/42d/thumb_1312_268_231_0_0_auto.png","title": "Panarottis",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Panarottis</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/panarottis?location=ul487\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '249'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5933",
						"y": "0.2407",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul572","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eaf/b09/thumb_1313_268_231_0_0_auto.png","title": "Pandora",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Pandora</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pandora?location=ul572\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '250'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3345",
						"y": "0.4214",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul449","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eb3/29a/thumb_1314_268_231_0_0_auto.png","title": "Partners Hair Design",
						"pin": "blue","description": "<h3 class=\"mapplic-map-store-name\">Partners Hair Design</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/partners-hair-design?location=ul449\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '251'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.425",
						"y": "0.3274",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul527","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eb5/cee/thumb_1315_268_231_0_0_auto.png","title": "Partners Hair Design for Men",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Partners Hair Design for Men</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/partners-hair-design-men?location=ul527\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '252'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7307",
						"y": "0.3397",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul4","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eb9/319/thumb_1316_268_231_0_0_auto.png","title": "Patio Warehouse",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Patio Warehouse</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/patio-warehouse?location=ul4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '253'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.336",
						"y": "0.599",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul597","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/eba/1e6/thumb_1317_268_231_0_0_auto.png","title": "Paul van Zyl Couture & Designer Fabric Collection",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Paul van Zyl Couture & Designer Fabric Collection</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/paul-van-zyl-couture-designer-fabric-collection?location=ul597\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '254'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.634",
						"y": "0.5563",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul659a","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f96/839/thumb_1321_268_231_0_0_auto.png","title": "Petit Love",
						"pin": "blue","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion</h4><h3 class=\"mapplic-map-store-name\">Petit Love</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/petit-love?location=ul659a\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '385'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3233",
						"y": "0.472",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul520","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9a/ae1/thumb_1327_268_231_0_0_auto.png","title": "Polo",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Polo</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/polo?location=ul520\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '264'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.632",
						"y": "0.4397",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul589","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9a/f1a/thumb_1328_268_231_0_0_auto.png","title": "Postnet",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Postnet</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/postnet?location=ul589\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '265'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6697",
						"y": "0.5488",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul490","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9b/864/thumb_1330_268_231_0_0_auto.png","title": "Primi",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Primi</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/primi?location=ul490\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '267'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7815",
						"y": "0.4301",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul556","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9c/1d8/thumb_1331_268_231_0_0_auto.png","title": "Pringle of Scotland",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Pringle of Scotland</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/pringle-scotland?location=ul556\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '268'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4328",
						"y": "0.403",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul505","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9c/819/thumb_1332_268_231_0_0_auto.png","title": "Puma",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Puma</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/puma?location=ul505\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '269'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6483",
						"y": "0.3214",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul466","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9c/bbe/thumb_1333_268_231_0_0_auto.png","title": "Queenspark",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Queenspark</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/queenspark?location=ul466\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '270'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6465",
						"y": "0.4281",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul560A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/f9e/aa5/thumb_1334_268_231_0_0_auto.png","title": "Red Square",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Red Square</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/red-square?location=ul560A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '271'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3937",
						"y": "0.4042",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul491","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc2/fd8/thumb_1335_268_231_0_0_auto.png","title": "Reebok",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Reebok</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/reebok?location=ul491\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '272'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5988",
						"y": "0.322",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul468","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc4/20c/thumb_1337_268_231_0_0_auto.png","title": "Relay Jeans",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Relay Jeans</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/relay-jeans?location=ul468\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '386'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6672",
						"y": "0.4296",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul426","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc4/837/thumb_1338_268_231_0_0_auto.png","title": "Rip Curl",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Rip Curl</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/rip-curl?location=ul426\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '274'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4408",
						"y": "0.378",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul470","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc4/d05/thumb_1339_268_231_0_0_auto.png","title": "Rockport",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Rockport</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/rockport?location=ul470\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '275'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6773",
						"y": "0.372",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul540","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc6/066/thumb_1340_268_231_0_0_auto.png","title": "Romens",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Romens</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/romens?location=ul540\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '276'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5272",
						"y": "0.4135",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul555","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc6/4a3/thumb_1341_268_231_0_0_auto.png","title": "Rugs Original",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Rugs Original</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/rugs-original?location=ul555\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '277'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8425",
						"y": "0.4309",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul447","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fc6/9c5/thumb_1342_268_231_0_0_auto.png","title": "RVCA The Store",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">RVCA The Store</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/rvca-store?location=ul447\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '278'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4138",
						"y": "0.3339",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul581","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fcb/446/thumb_1343_268_231_0_0_auto.png","title": "SA Art Gallery & Framing",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">SA Art Gallery & Framing</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sa-art-gallery-framing?location=ul581\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '279'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6769",
						"y": "0.4894",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul561","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fcb/ac9/thumb_1344_268_231_0_0_auto.png","title": "SA Framing",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">SA Framing</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sa-framing?location=ul561\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '280'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8222",
						"y": "0.4401",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul436","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fcc/f72/thumb_1345_268_231_0_0_auto.png","title": "Salomon",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Salomon</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/salomon?location=ul436\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '281'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5114",
						"y": "0.375",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul623","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fcd/714/thumb_1346_268_231_0_0_auto.png","title": "Salon Annique",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Salon Annique</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/salon-annique?location=ul623\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '282'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4665",
						"y": "0.4966",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul459","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15f/cbc/thumb_1437_268_231_0_0_auto.png","title": "Shesha",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">Shesha</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/shesha?location=ul459\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '286'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4587",
						"y": "0.3214",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul538","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15f/a4b/thumb_1436_268_231_0_0_auto.png","title": "Shimansky",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Shimansky</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/shimansky?location=ul538\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '287'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5372",
						"y": "0.4167",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul517","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/15d/70b/thumb_1431_268_231_0_0_auto.png","title": "Skechers",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Skechers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/skechers?location=ul517\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '294'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6921",
						"y": "0.3292",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul482A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/157/e63/thumb_1429_268_231_0_0_auto.png","title": "SODA Bloc",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">SODA Bloc</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/soda-bloc?location=ul482A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '296'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7558",
						"y": "0.3914",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul583","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/157/133/thumb_1427_268_231_0_0_auto.png","title": "Sorbet Face & Body Professionals",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Sorbet Face & Body Professionals</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sorbet-face-body-professionals-2?location=ul583\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '298'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6774",
						"y": "0.4961",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul456A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/156/0ac/thumb_1424_268_231_0_0_auto.png","title": "Speedo",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Speedo</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/speedo?location=ul456A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '301'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6065",
						"y": "0.3621",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul659A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/155/d3e/thumb_1423_268_231_0_0_auto.png","title": "Spitz",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Spitz</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/spitz?location=ul659A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '302'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3225",
						"y": "0.4557",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul423","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/152/d30/thumb_1421_268_231_0_0_auto.png","title": "Sportscene",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Sportscene</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sportscene?location=ul423\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '304'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3369",
						"y": "0.3",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul451","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/151/0f6/thumb_1420_268_231_0_0_auto.png","title": "Sportsmans Warehouse",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Sportsmans Warehouse</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sportsmans-warehouse?location=ul451\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '305'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4308",
						"y": "0.2857",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul599","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/150/2c0/thumb_1419_268_231_0_0_auto.png","title": "Standard Bank - Branch",
						"pin": "blue","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex</h4><h3 class=\"mapplic-map-store-name\">Standard Bank - Branch</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/standard-bank-branch?location=ul599\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '306'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6352",
						"y": "0.5198",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul528","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/14f/c36/thumb_1417_268_231_0_0_auto.png","title": "Sterns",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Sterns</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sterns?location=ul528\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '308'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5897",
						"y": "0.4143",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul444","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/14f/8ff/thumb_1416_268_231_0_0_auto.png","title": "Steve Madden",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Steve Madden</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/steve-madden?location=ul444\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '309'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5392",
						"y": "0.3841",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ll257","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/11d/37a/thumb_1411_268_231_0_0_auto.png","title": "Sunglass Hut",
						"pin": "blue","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists</h4><h3 class=\"mapplic-map-store-name\">Sunglass Hut</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/sunglass-hut-3?location=ll257\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '315'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3945",
						"y": "0.4578",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul477","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/0ef/3e3/thumb_1408_268_231_0_0_auto.png","title": "Superga",
						"pin": "blue","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear</h4><h3 class=\"mapplic-map-store-name\">Superga</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/superga?location=ul477\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '318'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5198",
						"y": "0.3264",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul522","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/0ec/c61/thumb_1406_268_231_0_0_auto.png","title": "Swatch",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Swatch</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/swatch?location=ul522\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '320'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6158",
						"y": "0.4246",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul593","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fff/0e3/thumb_1401_268_231_0_0_auto.png","title": "Tailors Shop",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Tailors Shop</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tailors-shop?location=ul593\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '323'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6643",
						"y": "0.5774",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul408","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fff/562/thumb_1402_268_231_0_0_auto.png","title": "Tashas",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Tashas</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tashas?location=ul408\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '324'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.25",
						"y": "0.52",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul577","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffb/7ce/thumb_1392_268_231_0_0_auto.png","title": "The Fix",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">The Fix</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/fix?location=ul577\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '333'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6947",
						"y": "0.4851",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul439","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffb/433/thumb_1391_268_231_0_0_auto.png","title": "The North Face",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">The North Face</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/north-face?location=ul439\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '334'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3896",
						"y": "0.3188",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul617","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b1/775/a38/thumb_1461_268_231_0_0_auto.png","title": "The Scoin Shop - closed & relocating to Shop 150",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">The Scoin Shop - closed & relocating to Shop 150</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/scoin-shop?location=ul617\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '335'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4766",
						"y": "0.4807",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul482A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffa/e39/thumb_1390_268_231_0_0_auto.png","title": "The Space",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">The Space</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/space?location=ul482A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '336'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.69",
						"y": "0.5",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul631","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ffa/2f8/thumb_1387_268_231_0_0_auto.png","title": "Tidy Tuc's Tailors",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Tidy Tuc's Tailors</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>3</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tidy-tucs-2?location=ul631\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '339'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4904",
						"y": "0.5849",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul515","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff9/fab/thumb_1386_268_231_0_0_auto.png","title": "Timberland",
						"pin": "blue","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion</h4><h3 class=\"mapplic-map-store-name\">Timberland</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/timberland?location=ul515\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '340'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6821",
						"y": "0.325",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul546","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff9/c71/thumb_1385_268_231_0_0_auto.png","title": "Tissot Boutique",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Tissot Boutique</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tissot-boutique?location=ul546\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '341'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5011",
						"y": "0.4258",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulku01","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fff/8ef/thumb_1403_268_231_0_0_auto.png","title": "ToePorn",
						"pin": "blue","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion</h4><h3 class=\"mapplic-map-store-name\">ToePorn</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/toeporn?location=ulku01\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '390'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6201",
						"y": "0.3306",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul151","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff8/cc3/thumb_1382_268_231_0_0_auto.png","title": "Topshop",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Topshop</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/topshop?location=ul151\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '344'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7569",
						"y": "0.5198",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul407","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff4/546/thumb_1379_268_231_0_0_auto.png","title": "Toy Kingdom",
						"pin": "blue","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies</h4><h3 class=\"mapplic-map-store-name\">Toy Kingdom</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/toy-kingdom?location=ul407\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '346'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.2563",
						"y": "0.3619",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul203","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff3/560/thumb_1374_268_231_0_0_auto.png","title": "Truworths",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Truworths</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/truworths?location=ul203\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '351'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5986",
						"y": "0.4971",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul530","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff3/2dc/thumb_1373_268_231_0_0_auto.png","title": "Tumi",
						"pin": "blue","category": "Travel","description": "<h4 class=\"mapplic-map-store-category\">Travel</h4><h3 class=\"mapplic-map-store-name\">Tumi</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/tumi?location=ul530\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '387'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5822",
						"y": "0.4123",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul402","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff3/012/thumb_1372_268_231_0_0_auto.png","title": "Twill",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Twill</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/twill?location=ul402\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '352'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3348",
						"y": "0.3821",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul465","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff2/0b3/thumb_1369_268_231_0_0_auto.png","title": "Uzzi",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">Uzzi</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/uzzi?location=ul465\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '355'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.484",
						"y": "0.3236",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul456B","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff1/eab/thumb_1368_268_231_0_0_auto.png","title": "Vans",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Vans</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/vans?location=ul456B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '356'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6152",
						"y": "0.3581",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul404","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff1/6ed/thumb_1365_268_231_0_0_auto.png","title": "Vida e Caffe",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Vida e Caffe</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/vida-e-caffe?location=ul404\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '359'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.334",
						"y": "0.3968",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul416","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff1/172/thumb_1363_268_231_0_0_auto.png","title": "Vodashop",
						"pin": "blue","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology</h4><h3 class=\"mapplic-map-store-name\">Vodashop</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>5</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/vodashop?location=ul416\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '361'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3814",
						"y": "0.3859",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul563","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff0/f04/thumb_1362_268_231_0_0_auto.png","title": "Volpes",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Volpes</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/volpes?location=ul563\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '362'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.8096",
						"y": "0.4534",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul478A","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff0/4e6/thumb_1359_268_231_0_0_auto.png","title": "Wildfire Bodypiercing",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Wildfire Bodypiercing</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/wildfire-bodypiercing?location=ul478A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '365'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7183",
						"y": "0.3686",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul480","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/ff0/232/thumb_1358_268_231_0_0_auto.png","title": "Wildfire Tattoos",
						"pin": "blue","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services</h4><h3 class=\"mapplic-map-store-name\">Wildfire Tattoos</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/wildfire-tattoos?location=ul480\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '366'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.7251",
						"y": "0.3857",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul585","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fef/bc9/thumb_1356_268_231_0_0_auto.png","title": "Winston Sahd",
						"pin": "blue","category": "Home & Décor","description": "<h4 class=\"mapplic-map-store-category\">Home & Décor</h4><h3 class=\"mapplic-map-store-name\">Winston Sahd</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>7</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/winston-sahd?location=ul585\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '368'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6745",
						"y": "0.5159",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul536","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fef/6c7/thumb_1355_268_231_0_0_auto.png","title": "Wolf Bros. Jewellers",
						"pin": "blue","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories</h4><h3 class=\"mapplic-map-store-name\">Wolf Bros. Jewellers</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/wolf-bros-jewellers?location=ul536\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '369'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.5428",
						"y": "0.4099",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulL8","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fed/ce6/thumb_1352_268_231_0_0_auto.png","title": "Xpresso Cafe",
						"pin": "blue","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage</h4><h3 class=\"mapplic-map-store-name\">Xpresso Cafe</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>1</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/xpresso-cafe?location=ulL8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '372'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.3144",
						"y": "0.2988",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul457","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fed/a71/thumb_1351_268_231_0_0_auto.png","title": "Xtreme Nutrition",
						"pin": "blue","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle</h4><h3 class=\"mapplic-map-store-name\">Xtreme Nutrition</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>6</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/xtreme-nutrition?location=ul457\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '373'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4504",
						"y": "0.3262",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ul432","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fed/554/thumb_1349_268_231_0_0_auto.png","title": "YDE",
						"pin": "blue","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear</h4><h3 class=\"mapplic-map-store-name\">YDE</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/young-designers-emporium-yde?location=ul432\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '375'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.4772",
						"y": "0.3796",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "ulK19","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/fed/7a5/thumb_1350_268_231_0_0_auto.png","title": "Yemaya Express Nail Bar",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Yemaya Express Nail Bar</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>8</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/yemaya-express-nail-bar?location=ulK19\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '374'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.66",
						"y": "0.51",
						"fill": "#ffffff",
						"zoom": "2"
					},
				]
			},{
				"id": "RL",
				"title": "Roof Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5a8/d89/d0a/5a8d89d0a2561764930317.svg","locations": [
					{
						"id": "rlewg","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bd1/177/thumb_1159_268_231_0_0_auto.png","title": "Century Spa",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Century Spa</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/century-spa?location=rlewg\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '66'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.63",
						"y": "0.55",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "rlr3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/bde/223/thumb_1169_268_231_0_0_auto.png","title": "Dros",
						"pin": "blue","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants</h4><h3 class=\"mapplic-map-store-name\">Dros</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>10</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/dros?location=rlr3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '98'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.63",
						"y": "0.27",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "rwg","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e33/279/thumb_1255_268_231_0_0_auto.png","title": "Laser Clinic",
						"pin": "blue","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty</h4><h3 class=\"mapplic-map-store-name\">Laser Clinic</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>0</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/laser-clinic?location=rwg\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '189'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.6488",
						"y": "0.5536",
						"fill": "#ffffff",
						"zoom": "2"
					},{
						"id": "reg3","thumbnail": "https://www.canalwalk.co.za/storage/app/uploads/public/5b0/e7a/927/thumb_1294_268_231_0_0_auto.png","title": "Musicians Gear Zone",
						"pin": "blue","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment</h4><h3 class=\"mapplic-map-store-name\">Musicians Gear Zone</h3><div class=\"row mapplic-map-store-level-entrance\">    <div class=\"col-6\">Level <strong>Lower Level</strong></div>    <div class=\"col-6\">Closest Entrance <strong>11</strong></div></div><div class=\"row mapplic-map-store-buttons\">    <div class=\"col-6\"><a href=\"https://www.canalwalk.co.za/shops/shop/musicians-gear-zone?location=reg3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '228'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div></div>",
						"x": "0.836",
						"y": "0.4699",
						"fill": "#ffffff",
						"zoom": "2"
					},
				]
			},],
	                "maxscale": 2
	            },
	            sidebar : true,
	            minimap : false,
	            markers : false,
	            fullscreen : true,
	            maxscale : 3
	        });
	    })( jQuery );