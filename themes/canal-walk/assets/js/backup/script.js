jQuery(document).ready(function () {

    jQuery(function () {

        var maxLength = 150;
        var content = jQuery('.nu-metro-intro .content p').html();
        var sectionContent = jQuery('section .intro p').html();

        if(sectionContent) {

            jQuery('.nu-metro-intro .content p').text('<p>' + content.substr(0, maxLength) + '</p>');
            jQuery('section .intro p').text(sectionContent.substr(0, maxLength));

            if (content.length > 150 || sectionContent.length > 150) {

                var c = content.substr(0, maxLength);
                var h = content.substr(maxLength, content.length - maxLength);

                var sc = sectionContent.substr(0, maxLength);
                var sh = sectionContent.substr(maxLength, sectionContent.length - maxLength);

                if (jQuery(window).width() < 481) {

                    var html = c + '<span class="morecontent" style="display: none;">' + h + '</span>&nbsp;<span><a href="" class="morelink">Read More <i class="fa fa-angle-down" aria-hidden="true"></i></a></span>';

                    var shtml = sc + '<span class="morecontent" style="display: none;">' + sh + '</span>&nbsp;<a href="" class="morelink">Read More <i class="fa fa-angle-down" aria-hidden="true"></i></a>';

                    jQuery('.nu-metro-intro .content p').html(html);
                    jQuery('section .intro p').html(shtml);
                }
            }
        }


        jQuery(".morelink").click(function() {
            if (jQuery(this).hasClass("less")) {
                jQuery(this).removeClass("less");
                jQuery(this).html('Read More <i class="fa fa-angle-down" aria-hidden="true"></i>');
            } else {
                jQuery(this).addClass("less");
                jQuery(this).html('Read Less <i class="fa fa-angle-up" aria-hidden="true"></i>');
            }
            // jQuery(this).parent().prev().toggle();
            jQuery(this).prev().toggle();

            return false;
        });

    });

    /* movie trailer */
    jQuery('button.close').on('click', function () {
        jQuery('.trailerPlayer').get(0).pause();
    });

    jQuery('.page-content .page-image img').height(function(index, height) {
        return window.innerHeight - jQuery(this).offset().top;
    });

    // toggle menu
    jQuery('.nav-toggler-icon .fa-bars').click(function() {
        jQuery(this).toggleClass('fa-times');
        jQuery('.main-navigation').toggle();
    });

    // drop down menu toggle
    //console.log(jQuery(jQuery('.nav-link').data('toggle')));

    // toggle search form
    jQuery(document).on('click', '.site-header .search-button .fa-search', function() {
        jQuery(this).toggleClass('fa-times');
        jQuery('.main-search-form').toggle();

    });

    // toggle footer nav
    if( jQuery(window).width() < 768 ) {
        // footer toggle
        jQuery(document).on('click', '.footer-nav h5', function () {
            jQuery(this).toggleClass('open');
            jQuery(this).next().toggle();
        });

        //section header container
        jQuery('section header').parent().parent().parent().css('padding','0');
    }

    // ===== Scroll to Top ====
    jQuery(function () {
        jQuery(window).scroll(function() {
            var scrollHeight = jQuery(document).height();
            var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();

            if( jQuery(window).width() > 768 ) {
                if ((scrollHeight - scrollPosition) / scrollHeight === 0) {        // If page is scrolled more than 50px
                    jQuery('.back-to-top .fa-angle-up').fadeIn(200);    // Fade in the arrow
                } else {
                    jQuery('.back-to-top .fa-angle-up').fadeOut(200);   // Else fade out the arrow
                }
            }
        });
        jQuery('.back-to-top .fa-angle-up').click(function() {      // When arrow is clicked
            jQuery('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
    });

    // tooltip
    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip();
    });

    // Slick configurations
    jQuery('.banner-slick').slick({
        arrows: false,
		autoplay: true,
  		autoplaySpeed: 5000,
    });
    jQuery('.banner-slick .banner-item').height(function() {
        return window.innerHeight - jQuery(this).offset().top;
    });


    // Slick configurations
    jQuery('.tag-filter .tags').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 5,
        variableWidth: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });

    // Gallery Slick configurations
    jQuery('.main-image-slick').slick({
        arrows: true,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    variableWidth: true,
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    jQuery('.store-information .store-logo').slick({
        infinite: true,
        speed: 300,
        arrows: true,
        dots: true,

        responsive: [
            {
                breakpoint: 640,
                settings: {
                    arrows: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                }
            }
        ]
    });
    jQuery('.eight-column-slick').slick({
        infinite: true,
        speed: 300,
        variableWidth: true,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: false,
                    variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    jQuery('.articles-slick').slick({
        infinite: false,
        arrows: false,
        speed: 300,
        variableWidth: true,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    // slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    // slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    // slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: false,
                    variableWidth: true,
                    // slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    jQuery('.three-column-slick').slick({
        infinite: false,
        speed: 300,
        variableWidth: true,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: false,
                    variableWidth: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    jQuery('.mobile-slick-3').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    // variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    jQuery('.mobile-slick-3-square').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: false,
                    variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    jQuery('.mobile-slick-4').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        variableWidth: false,
        arrows: false,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: false,
                    variableWidth: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});