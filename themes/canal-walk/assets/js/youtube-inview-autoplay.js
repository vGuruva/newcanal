//play when video is visible
var videos = document.getElementsByTagName("iframe"), fraction = 0.8;

function checkScroll() {

  for(var i = 0; i < videos.length; i++) {
    var video = videos[i];

    var x = 0,
        y = 0,
        w = video.width,
        h = video.height,
        r, //right
        b, //bottom 
        visibleX, visibleY, visible,
        parent;

    
    parent = video;
    while (parent && parent !== document.body) {
      x += parent.offsetLeft;
      y += parent.offsetTop;
      parent = parent.offsetParent;
    }

    r = x + parseInt(w);
    b = y + parseInt(h);
   

    visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
    visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));
    

    visible = visibleX * visibleY / (w * h);


    if (visible > fraction) {
      playVideo();
    } else {
      pauseVideo();

    }
  }

};


var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;

function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
};

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    window.addEventListener('scroll', checkScroll, false);
    window.addEventListener('resize', checkScroll, false);

    //check at least once so you don't have to wait for scrolling for the    video to start
    window.addEventListener('load', checkScroll, false);
};


function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
      //console.log("event played");
    } else {
      //console.log("event paused");
    }
};

function stopVideo() {
    player.stopVideo();
};

function playVideo() {
  player.playVideo();
};

function pauseVideo() {
  player.pauseVideo();
};


(function($) {
    jQuery(document).ready(function($) {

      var slideWrapper = $(".full-width-slider-wrapper");
      
      /*
       * The below example uses Slick Carousel, however this can be
       * extended into any type of carousel, provided it lets you
       * bind events when the slide changes. This will only work
       * if all framed videos have the JS API parameters enabled.
       */
      
      //bind our event here, it gets the current slide and pauses the video before each slide changes.
      $(".slick").on("beforeChange", function(event, slick) {
        var currentSlide, slideType, player, command;
        
        //find the current slide element and decide which player API we need to use.
        currentSlide = $(slick.$slider).find(".slick-current");
        
        //determine which type of slide this, via a class on the slide container. This reads the second class, you could change this to get a data attribute or something similar if you don't want to use classes.
        slideType = currentSlide.find('.nowview').attr("class").split(" ")[1];
        
        //get the iframe inside this slide.
        player = currentSlide.find("iframe").get(0);
        
        if (slideType == "vimeo") {
          command = {
            "method": "pause",
            "value": "true"
          };
        } else {
          command = {
            "event": "command",
            "func": "pauseVideo"
          };
        }
        
        //check if the player exists.
        if (player != undefined) {
          //post our command to the iframe.
          player.contentWindow.postMessage(JSON.stringify(command), "*");
        }
      });
      
    });
  })(jQuery);