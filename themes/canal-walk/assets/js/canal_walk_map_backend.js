(function( $ ){
				var mapplic = $('#mapplic').mapplic({
					source : {
						"mapwidth":"1200",
						"mapheight":"746",
						"minimap": false,
						"clearbutton": false,
						"zoombuttons": true,
						"sidebar": false,
						"search": false,
						"hovertip": false,
						"mousewheel": false,
						"fullscreen": false,
						"deeplinking": true,
						"mapfill": true,
						"zoom": false,
						"alphabetic": true,
						"zoomlimit": "2",
						"action": "tooltip",
						"categories": [
						],
						"levels": [
							{
				"id": "LL",
				"title": "Lower Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5a8/d7c/86e/5a8d7c86e0567658639788.svg",
				"show": "true",
				"locations": [ ]
			},{
				"id": "ML",
				"title": "Mezzanine Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5a8/d89/b0c/5a8d89b0c85d1773211086.svg",
				"show": "true",
				"locations": [ ]
			},{
				"id": "UL",
				"title": "Upper Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5ab/cae/e46/5abcaee46c131285264564.svg",
				"show": "true",
				"locations": [ ]
			},{
				"id": "RL",
				"title": "Roof Level",
				"map": "https://www.canalwalk.co.za/storage/app/uploads/public/5a8/d89/d0a/5a8d89d0a2561764930317.svg",
				"show": "true",
				"locations": [ ]
			},
						],
						"maxscale": 2
					},
					sidebar : false, // Enable sidebar
					minimap : false, // Enable minimap
					markers : false, // Disable markers
					fullscreen : true, // Enable fullscreen
					developer: true,
					maxscale : 3
					// Setting maxscale to 3 times bigger than the original file
				});
			})( jQuery );