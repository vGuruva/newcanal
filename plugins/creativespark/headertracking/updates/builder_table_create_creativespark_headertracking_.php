<?php namespace Creativespark\Headertracking\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkHeadertracking extends Migration
{
    public function up()
    {
        Schema::create('creativespark_headertracking_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('code')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('purpose')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_headertracking_');
    }
}
