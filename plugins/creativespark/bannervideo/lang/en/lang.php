<?php return [
    'plugin' => [
        'name' => 'BannerVideo',
        'description' => 'Video should appear full width and resize according to device
Banner should be able to accept any online video from Youtube, Vimeo, etc.
Video should autoplay
Video should loop
Video sound muted by default'
    ]
];