<?php namespace Creativespark\Bannervideo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkBannervideo2 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_bannervideo_', function($table)
        {
            $table->string('video_provider')->nullable();
            $table->string('url')->change();
            $table->string('visible')->change();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_bannervideo_', function($table)
        {
            $table->dropColumn('video_provider');
            $table->string('url', 191)->change();
            $table->string('visible', 191)->change();
        });
    }
}
