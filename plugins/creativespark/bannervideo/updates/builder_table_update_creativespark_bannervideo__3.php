<?php namespace Creativespark\Bannervideo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkBannervideo3 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_bannervideo_', function($table)
        {
            $table->string('url')->change();
            $table->boolean('visible')->nullable()->unsigned(false)->default(null)->change();
            $table->string('video_provider')->change();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_bannervideo_', function($table)
        {
            $table->string('url', 191)->change();
            $table->string('visible', 191)->nullable()->unsigned(false)->default(null)->change();
            $table->string('video_provider', 191)->change();
        });
    }
}
