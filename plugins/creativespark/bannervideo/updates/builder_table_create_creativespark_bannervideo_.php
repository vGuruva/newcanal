<?php namespace Creativespark\Bannervideo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkBannervideo extends Migration
{
    public function up()
    {
        Schema::create('creativespark_bannervideo_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->string('url')->nullable();
            $table->string('visible')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_bannervideo_');
    }
}
