<?php namespace Creativespark\Bannervideo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkBannervideo extends Migration
{
    public function up()
    {
        Schema::table('creativespark_bannervideo_', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('url')->change();
            $table->string('visible')->change();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_bannervideo_', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->increments('id')->unsigned()->change();
            $table->string('url', 191)->change();
            $table->string('visible', 191)->change();
        });
    }
}
