<?php namespace Creativespark\AppInterests\Models;

use Model;

/**
 * Model
 */
class AppInterest extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'creativespark_appinterests_';
}
