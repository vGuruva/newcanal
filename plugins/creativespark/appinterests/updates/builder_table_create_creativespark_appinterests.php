<?php namespace Creativespark\AppInterests\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkAppinterests extends Migration
{
    public function up()
    {
        Schema::create('creativespark_appinterests_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('content_type');
            $table->string('icon');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_appinterests_');
    }
}