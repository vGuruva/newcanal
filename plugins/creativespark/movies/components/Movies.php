<?php 

namespace CreativeSpark\Movies\Components;

use ApplicationException;
use Cms\Classes\ComponentBase;
use Cache;
use Request;


class Movies extends ComponentBase
{


    public $detail;

    public function componentDetails()
    {
        return [
            'name'        => 'Movie List',
            'description' => 'Movie Listing'
        ];
    }

    public function defineProperties()
    {
        return [
            'movies' => [
                'title'             => 'Movies',
                'type'              => 'dropdown',
                'default'           => 'now_showing',
			    'placeholder'       => 'Select',
                'options'           => ['now_showing'=>'now_showing', 'forthcoming'=>'Forth Coming']
            ]
        ];
    }



    public function onRun()
    {
        $this->addCss('/plugins/CreativeSpark/movies/assets/css/movies.css');
        $this->page['movies'] = $this->info();

    }

    public function info()
         {

$json = file_get_contents('storage/app/uploads/cineplex.json');

$items = json_decode($json);

$movieitems = array();	

$i=0;

	
foreach ($items as $item) {


if (!in_array($item->movie_id,$movieitems))
	
{

array_push($movieitems,$item->movie_id);
	
	
		//echo '<h1>'.$movies.'</h1>';
		
		$details = json_decode(file_get_contents('storage/app/uploads/'.$item->movie_id.'.json'));


		/*
        $movie = '';
		
		$file=$movie.'.jpg';
		$imageString = file_get_contents($details->poster_file);

		$filename = 'storage/app/uploads/'.$file;

		if (!file_exists($filename)) {
		$save = file_put_contents('storage/app/uploads/'.$file,$imageString);
		}

*/

		//if ($i++ === 3) break;

		

$movies[] = $details;


            }
	



}

return  $movies;


        }

}

