<?php 

namespace CreativeSpark\Movies\Components;

use ApplicationException;
use Cms\Classes\ComponentBase;
use Cache;
use Request;


class MoviesNew extends ComponentBase
{


    public $detail;

    public function componentDetails()
    {
        return [
            'name'        => 'New Movie List',
            'description' => 'New Movie Listing'
        ];
    }

    public function defineProperties()
    {
        return [
            'movies' => [
                'title'             => 'New Movies',
                'type'              => 'dropdown',
                'default'           => 'Forth Coming',

            ]
        ];
    }



    public function onRun()
    {

        $this->page['movies'] = $this->info();

    }

    public function info()
         {

            $json = file_get_contents('storage/app/uploads/new.json');
            
            $items = json_decode($json);
            
            foreach ($items as $item) {
             
            $details = json_decode(file_get_contents('storage/app/uploads/'.$item->id.'.json'));
            
            $movies[] = $details;
            
            }
            
            return  $movies;


        }

}

