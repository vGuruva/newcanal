<?php namespace CreativeSpark\Movies;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

/*
https://octobercms.com/docs/plugin/scheduling
*/

use System\Classes\PluginBase;

class Plugin extends PluginBase
{


    public function registerSchedule($schedule)
    {

        $schedule->call(function () {





        })->everyMinute();
    }



    public function pluginDetails()
    {
        return [
            'name'        => 'Movies',
            'description' => 'Provides the NuMetro movies information.',
            'author'      => 'Creative Spark',
            'icon'        => 'icon-video-camera'
        ];
    }

    public function registerComponents()
    {
        return [

                //'\CreativeSpark\Movies\Components\Movieslider' => 'movieslider',
                //'\CreativeSpark\Movies\Components\Movies' => 'movies',
                //'\CreativeSpark\Movies\Components\Movie' => 'movie',
                '\CreativeSpark\Movies\Components\MoviesNew' => 'new'

        ];
    }






}
