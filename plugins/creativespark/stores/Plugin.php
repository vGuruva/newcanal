<?php namespace CreativeSpark\Stores;

use System\Classes\PluginBase;
use CreativeSpark\Stores\Models\Store;
use Hyprop\Malls\Models\Floor;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Event;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
			'creativespark\stores\components\Stores' => 'stores',
			'creativespark\stores\components\Storedetails' => 'storedetails',
			'creativespark\stores\components\MallMap' => 'mallmap'
    	];
    }

    public function registerSettings()
    {
    }

	public function registerSchedule($schedule)
    {
		$schedule->call(function () {
			Store::saveMapFile(); // central function to save the mapplic files for backend and frontend
        })->everyMinute();
    }

	// not sure what this function is for as it is the same as what was in registerSchedule
	public function runscripts() {
		$mapplic_js = Store::buildMapplicJS();

		$fileName = themes_path().'/canal-walk/assets/js/canal_walk_map.js';
		file_put_contents($fileName, $mapplic_js);

		$floor_obj_arr = Floor::where('mall_id', 1)->orderBy('sort_order')->get();

		// set the floor list
		$mapplic_js_floor_list = '';
		foreach ($floor_obj_arr as $floor_obj) {
			$mapplic_js_floor_list .= '{
				"id": "'.$floor_obj->floor_code.'",
				"title": "'.$floor_obj->name.'",
				"map": "'.$floor_obj->mall_map->path.'",
				"show": "true",
				"locations": [ ]
			},';
		}

		$mapplic_js_backend = '(function( $ ){
				var mapplic = $(\'#mapplic\').mapplic({
					source : {
						"mapwidth":"1200",
						"mapheight":"746",
						"minimap": false,
						"clearbutton": false,
						"zoombuttons": true,
						"sidebar": false,
						"search": false,
						"hovertip": false,
						"mousewheel": false,
						"fullscreen": false,
						"deeplinking": true,
						"mapfill": true,
						"zoom": false,
						"alphabetic": true,
						"zoomlimit": "2",
						"action": "tooltip",
						"categories": [
						],
						"levels": [
							'.$mapplic_js_floor_list.'
						],
						"maxscale": 2
					},
					sidebar : false, // Enable sidebar
					minimap : false, // Enable minimap
					markers : false, // Disable markers
					fullscreen : true, // Enable fullscreen
					developer: true,
					maxscale : 3
					// Setting maxscale to 3 times bigger than the original file
				});
			})( jQuery );';

		$file_name_backend = themes_path().'/canal-walk/assets/js/canal_walk_map_backend.js';
		file_put_contents($file_name_backend, $mapplic_js_backend);
	}

    public function boot()
    {
		\Event::listen('formBuilder.beforeSendMessage', function ($form, $data, $files) {

			if(isset($data['storeslug'])){
				$store = Store::where('slug', '=', $data['storeslug'])->first();

				if(env('APP_DEBUG') === TRUE) {
					$form['to_email'] = 'bryce@virtualdesigns.co.za';
					$form['to_name'] = 'Bryce';
				} else {
					$form['to_email'] = $store->email_address;
					$form['to_name'] = $store->name;
				}
			}

		});

    	\Event::listen('offline.sitesearch.query', function ($query) {

            
        $themeName = Theme::getActiveTheme()->getDirName();
        $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first();             

    		// Search your plugin's contents
    		$items = Store::where('name', 'like', "%${query}%")
                ->where('hyprop_malls_id', $mall->id )
    		->orWhere('description', 'like', "%${query}%")
                ->where('hyprop_malls_id', $mall->id )
    		->get();

    		// Now build a results array
    		$results = $items->map(function ($item) use ($query) {

    			// If the query is found in the title, set a relevance of 2
    			$relevance = mb_stripos($item->title, $query) !== false ? 2 : 1;

				$category = 'No Category';
				if($item->categories->first()){
					$category = $item->categories->first()->name;
				}

				$fetchedUrl = url(str_replace(':slug', $item->slug, Page::load(Theme::getActiveTheme(), 'store')->url));

    			return [
    					'title'     => $item->name,
    					'text'      => $item->description,
    					'url'       => $fetchedUrl,
    					'thumb'     => $item->logo, // Instance of System\Models\File
    					'relevance' => $relevance, // higher relevance results in a higher
    					// position in the results listing
    					'meta' => array(
							'category' => $category,
							'id' => $item->id,
							'closest_entrance' => $item->closest_entrance,
							'floor_level' => $item->floor_level
						), // optional, any other information you want
    					// to associate with this result
    					'model' => $item,       // optional, pass along the original model
    			];
    		});

			return [
				'provider' => 'Store', // The badge to display for this result
				'results'  => $results,
			];
		});
		
		
        /*
         * Register menu items for the RainLab.Pages plugin
         */
        Event::listen('pages.menuitem.listTypes', function() {
            return [
				'all-stores'		  => 'All Stores',
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function($type) {
			if ($type == 'all-stores') {
                return Store::getMenuTypeInfo($type);
            }
        });

        Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
            if ($type == 'all-stores') {
                return Store::resolveMenuItem($item, $url, $theme);
            }
        });
    }
}
