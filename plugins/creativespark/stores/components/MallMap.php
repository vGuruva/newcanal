<?php namespace CreativeSpark\Stores\Components;

use Cms\Classes\ComponentBase;
use Input;
use CreativeSpark\Stores\Models\Store;
use CreativeSpark\Stores\Models\Category;
use Hyprop\Malls\Models\Floor;
use Cms\Classes\Theme;

class MallMap extends ComponentBase {

	public $store_obj_arr;
	public $floor_obj_arr;
	public $active_floor;
	public $category_obj_arr;

	public function componentDetails() {
		return [
			'name' => 'Mall Map',
			'description' => 'Loads the mall map'
		];
	}

	public function defineProperties() {
		return [
				'display_option' => [
					'title' => 'Display Option',
					'description' => 'Please check if this is a store page',
					'default' => 0,
					'type' => 'checkbox'
				]
		];
	}

	public function onRun() {
		$this->store_obj_arr = false;
		$this->floor_obj_arr = false;
		$this->active_floor = false;
		$this->category_obj_arr = false;

		$this->category_obj_arr = Category::get();

		$themeName = Theme::getActiveTheme()->getDirName();

		// get store with custom query for speed from external function
		if($this->property('display_option') && $this->param('slug')){
			$this->store_obj_arr = Store::buildMapplicObj($this->param('slug'));

			$this->active_floor = Floor::select('hyprop_malls_floors.*')
										->leftJoin('creativespark_stores_', 'hyprop_malls_floors.id', '=', 'creativespark_stores_.hyprop_floor_id')
										->where('creativespark_stores_.slug', '=', $this->param('slug'))
										->first();
		}

		$floor_slug = $this->param('floor');
		
		// get floors if not store map
		if($this->store_obj_arr === false){
			$this->floor_obj_arr = Floor::select('hyprop_malls_floors.*')
				->where('hyprop_malls_.theme_name', '=', $themeName)
				->leftJoin('hyprop_malls_', 'hyprop_malls_.id', '=', 'hyprop_malls_floors.mall_id')
				->get();

			if($floor_slug){
				$this->active_floor = $this->floor_obj_arr->where('slug', '=', $floor_slug)->first();
				$this->store_obj_arr = Store::buildMapplicObj(false, $this->active_floor->id);
			} else {
				$this->active_floor = $this->floor_obj_arr->first();
				$this->store_obj_arr = Store::buildMapplicObj(false, $this->active_floor->id);
			}
		}
	}
}
