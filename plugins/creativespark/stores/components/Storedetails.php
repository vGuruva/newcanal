<?php namespace CreativeSpark\Stores\Components;

use Cms\Classes\ComponentBase;
use Input;
use CreativeSpark\Stores\Models\Store;
use CreativeSpark\Stores\Models\Category;
use CreativeSpark\Stores\Models\Hour;
use RainLab\Blog\Models\Post;
use Hyprop\Malls\Models\Floor;
use JorgeAndrade\Events\Models\Event as Offers;
use Session;
use Redirect;

class Storedetails extends ComponentBase {

	public $storeDetail;
	public $favoriteArray;
	public $blogPosts;
	public $similarStores;
    public $is_open;
    public $trading_hours;
	// public $map_control_html;
	public $offers;

	public function componentDetails(){
		return [
			'name' => 'Store Details',
			'description' => 'List of Stores'
		];
	}

	public function defineProperties() {
		return [
			'results' => [
				'title' => 'Number of Similar Stores',
				'description' => 'please select the amount of similar stores loaded on the page',
				'default' => 10,
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Only numbers allowed'
			]
		];
	}

	public function onRun() {
		$this->storeDetail = $this->loadStore();

		if(!$this->storeDetail){
			return Redirect::to('/404');
		}

		$this->offers = $this->loadOffers();

		$floor_obj_arr = Floor::where('mall_id', 1)->get();

		// // commented out requested by client
		// $this->map_control_html = '';
		// foreach ($floor_obj_arr as $floor_obj) {
		// 	$this->map_control_html .= '<a class="mapplic-button mapplic-levels-'.$floor_obj->floor_code.'" href="javascript:none;"
		// onclick="jQuery(\'.mapplic-levels-select\').val(\''.$floor_obj->floor_code.'\').change();jQuery(\'.mapplic-button\').removeClass(\'mapplic-disabled\');jQuery(\'.mapplic-levels-'.$floor_obj->floor_code.'\').addClass(\'mapplic-disabled\');">'.$floor_obj->name.'</a>';
		// }

		$this->page['store'] = $this->storeDetail;

        if($this->storeDetail->hours->count() > 0){
            $this->trading_hours = $this->storeDetail->hours->first();
            $this->is_open = Hour::whereRaw('"'.date('H:i:s', strtotime('now')).'" between ' . strtolower(date('l')).'_start and '.strtolower(date('l')).'_end')->where('store_id','=', $this->storeDetail->id)->count();
        }else{
            $this->trading_hours = Hour::where('id', '=', 1)->first();
            $this->is_open = Hour::whereRaw('"'.date('H:i:s', strtotime('now')).'" between ' . strtolower(date('l')).'_start and '.strtolower(date('l')).'_end')->where('id','=', 1)->count();
            if(!$this->trading_hours){
                $this->trading_hours = array(
                    'monday_start'      => '09:00',
                    'monday_end'        => '21:00',
                    'tuesday_start'     => '09:00',
                    'tuesday_end'       => '21:00',
                    'wednesday_start'   => '09:00',
                    'wednesday_end'     => '21:00',
                    'thursday_start'    => '09:00',
                    'thursday_end'      => '21:00',
                    'friday_start'      => '09:00',
                    'friday_end'        => '21:00',
                    'saturday_start'    => '09:00',
                    'saturday_end'      => '21:00',
                    'sunday_start'      => '09:00',
                    'sunday_end'        => '21:00',
                );
            }
        }

		$this->favoriteArray = Session::get('store.favorite');

		$this->blogPosts = Post::select('rainlab_blog_posts.*')
		->where(function ($query) {
			$query->orWhere('title', 'like', '%'.$this->storeDetail->name.'%')
			->orWhere('content', 'like', '%'.$this->storeDetail->name.'%')
			->orWhere('rahman_blogtags_tags.name', '=', $this->storeDetail->name);
		})->join('rahman_blogtags_posts_tags', 'rahman_blogtags_posts_tags.post_id', 'rainlab_blog_posts.id')
		->join('rahman_blogtags_tags', 'rahman_blogtags_tags.id', 'rahman_blogtags_posts_tags.tag_id')
		->orderBy('rainlab_blog_posts.published_at', 'DESC')
		->groupBy('rainlab_blog_posts.id')->get();

		// dd($this->blogPosts->first()->slug);

		$this->similarStores = $this->loadSimilarStore();

		Session::put('store.recentlyViewed.'.$this->storeDetail->id, $this->storeDetail->id);

		$_GET['location'] = 'LL267';
	}

	protected function loadOffers() {
		$offers = Offers::listFrontEnd([
			'event_type' => 2,
			'filter_store' => $this->storeDetail->id
		]);

		return $offers;
	}

	protected function loadStore() {
		$storeDetail = Store::where('slug', '=', $this->param('slug'))->first();

		if(!empty($storeDetail->website)){
			$storeDetail->website_url = $storeDetail->website;
			$storeDetail->website_display = $storeDetail->website;

			if(strstr($storeDetail->website, '://') == false) {
				$storeDetail->website_display = $storeDetail->website;
				$storeDetail->website_url = 'http://'.$storeDetail->website;
			} else {
				$storeDetail->website_display = substr(strstr($storeDetail->website, '://'), 3);
				$storeDetail->website_url = $storeDetail->website;
			}
		}
		return $storeDetail;
	}

	protected function loadSimilarStore() {

		if($this->storeDetail->similar_stores->count() < $this->property('results') && $this->storeDetail->similar_stores->count() !== 0){

			$limitStores = $this->property('results') - $this->storeDetail->similar_stores->count();

			$storeIdArr = array();
			if($this->storeDetail->similar_stores->count() != 1) {
				foreach($this->storeDetail->similar_stores as $similarStoreObj){
					$storeIdArr[$similarStoreObj->id] = $similarStoreObj->id;
				}
			} else {
				$storeIdArr[$this->storeDetail->similar_stores->first()->id] = $this->storeDetail->similar_stores->first()->id;
			}


			$similarStoresBefore = Store::select('creativespark_stores_.*')
			->where('creativespark_stores_stores_categories.category_id', '=', $this->storeDetail->categories->first()->id)
			->where('creativespark_stores_.id', '<>', $this->storeDetail->id)
			->join('creativespark_stores_stores_categories', 'creativespark_stores_.id', '=', 'creativespark_stores_stores_categories.store_id')
			->take($limitStores)
			->inRandomOrder();


			$similarStores = Store::select('creativespark_stores_.*')->whereIn('creativespark_stores_.id', $storeIdArr)
			->union($similarStoresBefore)->get();

		} else if($this->storeDetail->similar_stores->count()) {
			$similarStores = $this->storeDetail->similar_stores->take($this->property('results'));
		} else {
			if($this->storeDetail->categories->first()){
				$similarStores = Store::select('creativespark_stores_.*')->where('creativespark_stores_stores_categories.category_id', '=', $this->storeDetail->categories->first()->id)
									->where('creativespark_stores_.id', '<>', $this->storeDetail->id)
									->join('creativespark_stores_stores_categories', 'creativespark_stores_.id', '=', 'creativespark_stores_stores_categories.store_id')
									->inRandomOrder()->take($this->property('results'))->get();
			} else {
				$similarStores = Store::inRandomOrder()->take($this->property('results'))->get();
			}
		}

		return $similarStores;
	}
}
