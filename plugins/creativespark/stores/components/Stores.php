<?php namespace CreativeSpark\Stores\Components;

use Cms\Classes\ComponentBase;
use Input;
use CreativeSpark\Stores\Models\Store;
use CreativeSpark\Stores\Models\Category;
use CreativeSpark\Stores\Models\Hour;
use Session;
use Cms\Classes\Theme;

class Stores extends ComponentBase {

	public $stores;
	public $search;
	public $tab;
	public $categories;
	public $selectedCategories;
	public $setLetter;
	public $title;
	public $display_option;
	public $favoriteArray;
	public $hasEcommerce;
	public $recentlyViewedArray;
	public $pageNumber;
        public $is_open;
        public $trading_hours;

	public function componentDetails(){
		return [
			'name' => 'Store List',
			'description' => 'List of Stores'
		];
	}

	public function defineProperties() {
		return [
			'results' => [
				'title' => 'Number of Stores',
				'description' => 'please select the amount of stores loaded when "load More" is clicked',
				'default' => 10,
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Only numbers allowed'
			],
			'display_option' => [
				'title' => 'Display Option',
				'description' => 'Please check if this is a category list',
				'default' => 0,
				'type' => 'checkbox'
			]
		];
	}

	public function handelForm() {
		$this['search'] = post('search');
		$this['category1'] = post('category1');
		$this['category2'] = post('category2');
		$this['category3'] = post('category3');
	}

	public function onRun() {
		if($this->param('category1') || $this->param('category2') || $this->param('category3')){
			$cat_slug = $this->param('category3', $this->param('category2', $this->param('category1')));

			$this->page['store-listing-categories'] = Category::where('slug', '=', $cat_slug)->first();
		}

		$this->search = $this->param('search');
		$this->setLetter = $this->param('letter');
		$this->tab = $this->param('tab');
		$this->pageNumber = 1;

		if(Input::get('tab')){
			$this->tab = Input::get('tab');
		}

		if(Input::get('search')){
			$this->search = Input::get('search');
		}

		if(Input::get('letter')){
			$this->setLetter = Input::get('letter');
		}

		if(Input::get('pageNumber')){
			$this->pageNumber = $this->page['pageNumber'] = Input::get('pageNumber');
		}

		$this->selectedCategories = $this->page['selectedCategories'] = $this->getSelectedCategories();

		$this->categories = $this->page['categories'] = $this->loadCategories();

		$this->stores = $this->page['stores'] = $this->loadStores();

		$this->title = $this->page['title'] = $this->buildTitle();
		$this->dynamic_title_old = $this->page['dynamic_title_old'] = Input::get('dynamic_title_old', null);

		$this->display_option = $this->page['display_option'] = $this->property('display_option');

		$this->favoriteArray = $this->page['favoriteArray'] = Session::get('store.favorite');
		$this->recentlyViewedArray = $this->page['recentlyViewedArray'] = Session::get('store.recentlyViewed');
		$this->hasEcommerce = Store::where('has_ecommerce', '=', '1')->count();
	}

	protected function loadStores() {
		$primary_category = $this->param('category1');
		$secondary_category = $this->param('category2');
		$tertiary_category = $this->param('category3');

		if(Input::get('category1')){
			$primary_category = Input::get('category1');
		}

		if(Input::get('category2')){
			$secondary_category = Input::get('category2');
		}

		if(Input::get('category3')){
			$tertiary_category = Input::get('category3');
		}

		$stores = Store::listFrontEnd([
				'page' => $this->pageNumber,
				'perPage' => $this->property('results'),
				'letter' => $this->setLetter,
				'category1' => $primary_category,
				'category2' => $secondary_category,
				'category3' => $tertiary_category,
				'search' => $this->search,
				'tab' => $this->tab
		]);

        return $stores;
	}

	protected function loadCategories() {
		$primary_category = Category::where('parent_id', 0)->orWhereNull('parent_id')->listFrontend()->get();
		$secondary_category = array();
		$tertiary_category = array();

		if($this->param('category1')){
			$secondary_category = Category::where('slug', '=', $this->param('category1'))->first();

			if($secondary_category) {
				$secondary_category = $secondary_category->children()->get();
			}
		}

		if($this->param('category2')){
			$tertiary_category = Category::where('slug', '=', $this->param('category2'))->first();

			if($tertiary_category) {
				$tertiary_category = $tertiary_category->children()->get();
			}
		}

		if(Input::get('category1')){
			$secondary_category = Category::where('slug', '=', $this->param('category1'))->first();

			if($secondary_category) {
				$secondary_category = $secondary_category->children()->get();
			}
		}

		if(Input::get('category2')){
			$tertiary_category = Category::where('slug', '=', $this->param('category2'))->first();

			if($tertiary_category) {
				$tertiary_category = $tertiary_category->children()->get();
			}
		}

		return array('primaryCategory' => $primary_category, 'secondaryCategory' => $secondary_category, 'tertiaryCategory' => $tertiary_category);
	}

	protected function getSelectedCategories() {
		$primary_category = $this->param('category1');
		$secondary_category = $this->param('category2');
		$tertiary_category = $this->param('category3');

		if(Input::get('category1')){
			$primary_category = Input::get('category1');
		}

		if(Input::get('category2')){
			$secondary_category = Input::get('category2');
		}

		if(Input::get('category3')){
			$tertiary_category = Input::get('category3');
		}

		return array('primaryCategory' => $primary_category, 'secondaryCategory' => $secondary_category, 'tertiaryCategory' => $tertiary_category);
	}

	protected function buildTitle() {
		$title = 'No Stores';

		if($this->stores->first()) {
			$title = str_limit(strtoupper($this->stores->first()->name), 1, '');
		}

		$letter = $this->param('letter');
		if(Input::get('letter')){
			$letter = Input::get('letter');
		}

		$search = $this->param('search');
		if(Input::get('search')){
			$search = Input::get('search');
		}

		$primary_category = $this->param('category1');
		if(Input::get('category1')){
			$primary_category = Input::get('category1');
		}

		$secondary_category = $this->param('category2');
		if(Input::get('category2')){
			$secondary_category = Input::get('category2');
		}

		$tertiary_category = $this->param('category3');
		if(Input::get('category3')){
			$tertiary_category = Input::get('category3');
		}

		if($letter){
			$title = $letter;
		}

		if($search){
			$title = $search;
		}

		if($primary_category){
			$title = $primary_category;
		}

		if($secondary_category){
			$title .= ', '.$secondary_category;
		}

		if($tertiary_category){
			$title .= ', '.$tertiary_category;
		}

		return $title;
	}
}
