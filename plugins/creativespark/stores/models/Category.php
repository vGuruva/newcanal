<?php


namespace CreativeSpark\Stores\Models;

use Model;
use Cms\Classes\Theme;
use BackendAuth;
use Illuminate\Support\Facades\Redirect;

/**
 * Model
 */
class Category extends Model {
	use \October\Rain\Database\Traits\Validation;

	/**
	 *
	 * @var array Validation rules
	 */
	public $rules = [ ];

	public $implement = ['@Renatio.SeoManager.Behaviors.SeoModel'];

	/**
	 *
	 * @var string The database table used by the model.
	 */
	public $table = 'creativespark_stores_categories';

	/* Relations */
	public $belongsToMany = [
		'stores' => [
			'CreativeSpark\Stores\Models\Store',
			'table' => 'creativespark_stores_stores_categories',
			'order' => 'name'
		]
	];

	/* Relations */
	public $belongsTo = [
		'parent' => ['CreativeSpark\Stores\Models\Category']
	];

	public $hasMany = [
		'children' => ['CreativeSpark\Stores\Models\Category', 'key' => 'parent_id']
	];
        
        
        public function scopeListFrontEnd($query)
        {
        
            $themeName = Theme::getActiveTheme()->getDirName();

            if( !empty($themeName) ) {
               $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first(); 
            }

            if( !empty($mall) ){
                $query->where('creativespark_stores_categories.hyprop_malls_id', $mall->id );
            }
        }        
        
        
        public function beforeSave()
        {

            $user = BackendAuth::getUser();  
            
            if( is_int($user->role->mall_id) ):
                
                $this->hyprop_malls_id = $user->role->mall_id;
            
            else:
                
                return Redirect::back();
            
            endif;     
        }        
        
        
}
