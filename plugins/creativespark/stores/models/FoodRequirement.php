<?php namespace CreativeSpark\Stores\Models;

use Model;

/**
 * Model
 */
class FoodRequirement extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'creativespark_stores_food_requirements';
    
    /* relations */
    
    /* Relations */
	public $belongsToMany = [
		'stores' => [
			'CreativeSpark\Stores\Models\Store',
			'table' => 'creativespark_stores_stores_categories',
			'order' => 'name'
		]
	];
}
