<?php namespace CreativeSpark\Stores\Models;

class StoresExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $stores = Store::all();
        $stores->each(function($store) use ($columns) {
            $store->addVisible($columns);
        });
        return $stores->toArray();
    }
}