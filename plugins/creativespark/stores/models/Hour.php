<?php namespace CreativeSpark\Stores\Models;

use Model;

/**
 * Model
 */
class Hour extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'creativespark_stores_hours';
    
    /* Relations */
    public $belongsToMany = [
            'stores' => [
                    'CreativeSpark\Stores\Models\Store',
                    'table' => 'creativespark_stores_',
                    'order' => 'name'
            ]
    ];
}
