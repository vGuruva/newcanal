<?php namespace CreativeSpark\Stores\Models;

use Model;
use Session;
use CreativeSpark\Stores\Models\Category;
use Hyprop\Malls\Models\Floor;
use Hyprop\Malls\Models\Malls;
use Cms\Classes\Page as CmsPage;
use Cms\Classes\Theme;
use BackendAuth;
use Illuminate\Support\Facades\Redirect;
use Config;

/**
 * Model
 */
class Store extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
		'name' => 'required',
		'slug' => 'required',
		'description' => 'required',
		'closest_entrance' => 'required',
		'floor_level' => 'required',
		'shop_number' => 'required',
		'logo' => 'required'
    ];

    public $mall_hour = '';

	public $implement = ['@Renatio.SeoManager.Behaviors.SeoModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'creativespark_stores_';

    /* Relations */
	public $belongsTo = [
		'hyprop_malls' => ['Hyprop\Malls\Models\Malls'],
		'hyprop_floor' => ['Hyprop\Malls\Models\Floor'],
		'backend_user' => ['Backend\Models\User']
	];

	public $belongsToMany = [
		'categories' => [
			'CreativeSpark\Stores\Models\Category',
			'table' => 'creativespark_stores_stores_categories',
			'order' => 'name'
		],
		'food_requirements' => [
			'CreativeSpark\Stores\Models\FoodRequirement',
			'table' => 'creativespark_stores_stores_food_requirements',
			'order' => 'name'
		],
		'similar_stores' => [
			'CreativeSpark\Stores\Models\Store',
			'table'    => 'creativespark_stores_stores_stores',
			'key'      => 'parent_id',
			'otherKey' => 'child_id'
		],
		'favorite_stores' => [
			'RainLab\User\Models\User',
			'table'    => 'creativespark_stores_stores_users'
		]
	];

    public $hasMany = [
		'events' => ['JorgeAndrade\Events\Models\Event'],
		'hours' => ['CreativeSpark\Stores\Models\Hour']
	];

    public $attachOne = [
    	'logo' => 'System\Models\File'
    ];

    public $attachMany = [
    	'image_gallery' => 'System\Models\File'
    ];

    public static $allowedSortingOptions = array (
    		'name desc' => 'Name - desc',
    		'name asc' => 'Name - asc'
    );

    public function scopeListFrontEnd($query, $options = []){
		// settings passed to the scope functions
    	extract(array_merge([
    			'page' => 1, // int
    			'perPage' => 10, // int
    			'letter' => 'a', // alphabetical listing
    			'category1' => false, // primary category filtering (slug of the category)
    			'category2' => false, // secondary category filtering (slug of the category)
    			'category3' => false, // tertiary category filtering (slug of the category)
    			'search' => '', // search query
    			'tab' => false // tab filtering (slugified string)
    	], $options));

		// build custom query that includes fields from time management. finds if the store is open or closed and opening times
    	$query->select('creativespark_stores_.*')
		->selectRaw("(if ((select ".strtolower(date('l')) . "_start from creativespark_stores_hours where store_id = creativespark_stores_.id) is null,
				(select ".strtolower(date('l')) . "_start from creativespark_stores_hours where id = 1),
				(select ".strtolower(date('l')) . "_start from creativespark_stores_hours where store_id = creativespark_stores_.id))) as trade_start,
		 (if ((select ".strtolower(date('l')) . "_end from creativespark_stores_hours where store_id = creativespark_stores_.id) is null,
				(select ".strtolower(date('l')) . "_end from creativespark_stores_hours where id = 1),
				(select ".strtolower(date('l')) . "_end from creativespark_stores_hours where store_id = creativespark_stores_.id))) as trade_end,
		 (if ((select ".strtolower(date('l')) . "_end from creativespark_stores_hours where store_id = creativespark_stores_.id) is null,
				(select count(".strtolower(date('l')) . "_end) from creativespark_stores_hours where '".date('H:i:s', strtotime('now'))."' between ".strtolower(date('l')) . "_start and ".strtolower(date('l')) . "_end AND id = 1),
				(select count(".strtolower(date('l')) . "_end) from creativespark_stores_hours where '".date('H:i:s', strtotime('now'))."' between ".strtolower(date('l')) . "_start and ".strtolower(date('l')) . "_end AND store_id = creativespark_stores_.id))) as is_open")
    	->leftJoin('creativespark_stores_stores_categories', 'creativespark_stores_.id', '=', 'creativespark_stores_stores_categories.store_id')
    	->leftJoin('creativespark_stores_categories', 'creativespark_stores_stores_categories.category_id', '=', 'creativespark_stores_categories.id')
    	->whereNull('deleted_at')
    	->groupBy('creativespark_stores_.id')
    	->orderBy('creativespark_stores_.name');

		// finds the category based off its slug only if no tabs
    	if($category3 && $tab == false) {
    		$query->where('creativespark_stores_categories.slug', '=', $category3);
    	} else if($category2 && $tab == false) {
    		$query->where('creativespark_stores_categories.slug', '=', $category2);
    	} else if($category1 && $tab == false) {
    		$query->where('creativespark_stores_categories.slug', '=', $category1);
    	}

		// finds results based off search query if no tabs
    	if($search && $tab == false) {
    		$query->where('creativespark_stores_.name', 'like', '%'.$search.'%');
    	}

		// filters with tabs
    	if($tab == 'e-commerce'){
    		$query->where('has_ecommerce', '=', '1');
    	}

    	if($tab == 'recently-viewed') {
    		$query->whereIn('creativespark_stores_.id', Session::get('store.recentlyViewed'));
    	}

    	if($tab == 'favorited-stores') {
    		$query->whereIn('creativespark_stores_.id', Session::get('store.favorite'));
    	}
		// end filtering tabs

		// filtering based off first letter
    	if($letter && $tab == false){
    		$query->where('creativespark_stores_.name', 'like', $letter.'%');
    	}
        
		$themeName = Theme::getActiveTheme()->getDirName();
        
        if( !empty($themeName) ) {
           $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first(); 
        }
        
        if( !empty($mall) ){
            $query->where('creativespark_stores_.hyprop_malls_id', $mall->id );
        }

	// paginate result set from settings
        $store_data = $query->paginate($perPage, $page);

		return $store_data;
    }

	public function beforeSave()
	{

		$user = BackendAuth::getUser();  
		
		if( is_int($user->role->mall_id) ):
			
			$this->hyprop_malls_id = $user->role->mall_id;
		
		else:
			
			return Redirect::back();
		
		endif;     
	}

	public static function buildMapplicObj($display_single = false, $floor = false)
	{
		// get currently active theme
		$themeName = Theme::getActiveTheme()->getDirName();

		$results_obj_query = Store::select('creativespark_stores_.*', 'creativespark_stores_categories.name as cat_name', 'creativespark_stores_categories.slug as cat_slug', 'system_files.disk_name')
		->leftJoin('creativespark_stores_stores_categories', 'creativespark_stores_stores_categories.store_id', '=', 'creativespark_stores_.id')
		->leftJoin('creativespark_stores_categories', 'creativespark_stores_categories.id', '=', 'creativespark_stores_stores_categories.category_id')
		->leftJoin('system_files', 'system_files.attachment_id', '=', 'creativespark_stores_.id')
		->leftJoin('hyprop_malls_floors', 'hyprop_malls_floors.id', '=', 'creativespark_stores_.hyprop_floor_id')
		->whereNull('creativespark_stores_.deleted_at')
		->where('system_files.attachment_type', '=', 'CreativeSpark\Stores\Models\Store')
		->where('system_files.field', '=', 'logo');
							
		if($floor){
			$results_obj_query->where('hyprop_malls_floors.id', '=', $floor);
		}

		if($display_single) {
			$results_obj_arr = $results_obj_query->where('creativespark_stores_.slug', '=', $display_single)->groupBy('creativespark_stores_.id')->get();
		} else {
			$results_obj_arr = $results_obj_query->groupBy('creativespark_stores_.id')->get();
		}

		$shops = array();
		foreach($results_obj_arr as $results_obj){
			$path = Config::get('app.url').'/storage/app/uploads/public/'.substr($results_obj->disk_name, 0, 3).'/'.substr($results_obj->disk_name, 3, 3).'/'.substr($results_obj->disk_name, 6, 3).'/'.$results_obj->disk_name;
			$results_obj->logo_path = $path;
			$shops[] = $results_obj;
		}

		return $shops;
	}

	public static function buildMapplicJS($display_single = false)
	{
		// set the floor object
		$floor_obj_arr = Floor::where('mall_id', 1)->orderBy('sort_order', 'ASC')->get();

		// set the category object
		$category_object = Category::orderBy('name')->pluck('name', 'id');

		// set category list JSON
		$mapplic_js_cat_list = '';
		foreach($category_object as $categoryName) {
			$mapplic_js_cat_list .= '{
				"id": "'.$categoryName.'",
				"title": "'.$categoryName.'",
				"color": "#666666",
				"show": "false"
			},';
		}

		// set the floor list
		$mapplic_js_floor_list = '';
		$mapplic_js_setLocation = '';
		foreach ($floor_obj_arr as $floor_obj) {
			// get the stores that are linked to a store
			$store_obj_arr = Store::select('creativespark_stores_.*')
						->selectRaw('(SELECT COUNT(*)
										FROM creativespark_stores_stores_categories
										WHERE creativespark_stores_.id = creativespark_stores_stores_categories.store_id
									) AS has_category')
						->selectRaw('(SELECT creativespark_stores_categories.name
										FROM creativespark_stores_categories
										LEFT JOIN creativespark_stores_stores_categories ON creativespark_stores_stores_categories.category_id = creativespark_stores_categories.id
										WHERE creativespark_stores_stores_categories.store_id = creativespark_stores_.id
										LIMIT 1
									) AS category_name')

			    		->where('creativespark_stores_.hyprop_floor_id', '=', $floor_obj->id)
						->whereNull('creativespark_stores_.deleted_at')
						->groupBy('creativespark_stores_.id')
						->orderBy('creativespark_stores_.name');

			// build the JSON for stores
			$mapplic_js_store_list = '';
			if($display_single) {
				$store_obj = $store_obj_arr->where('slug', '=', $display_single)->first();

				if($store_obj) {
					$mapplic_js_store_list .= '{
						"id": "'.$store_obj->store_id.'",
						"title": "'.$store_obj->name.'",
						"pin": "pin-classic",';

					if($store_obj->has_category){
						$mapplic_js_store_list .= '"category": "'.$store_obj->category_name.'",';
					}

					$mapplic_js_store_list .= '"action": "default",
						"x": "'.$store_obj->store_x.'",
						"y": "'.$store_obj->store_y.'",
						"fill": "#000000",
						"zoom": "2"
					},';

					$mapplic_js_setLocation = "map.data('mapplic').showLocation('".$store_obj->store_id."', 100);";
				}
			} else {
				$store_obj_arr = $store_obj_arr->get();

				foreach($store_obj_arr as $store_obj){
					$jsonHtml = '';
					if($store_obj->has_category){
						$jsonHtml .= '<h4 class=\"mapplic-map-store-category\">'.$store_obj->category_name.'</h4>';
					}

					$fetchedUrl = url(str_replace(':slug', $store_obj->slug, CmsPage::load(Theme::getActiveTheme(), 'store')->url));

					$jsonHtml .= '<h3 class=\"mapplic-map-store-name\">'.$store_obj->name.'</h3>';
					$jsonHtml .= '<div class=\"row mapplic-map-store-level-entrance\">';
					$jsonHtml .= '    <div class=\"col-6\">Level <strong>Lower Level</strong></div>';
					$jsonHtml .= '    <div class=\"col-6\">Closest Entrance <strong>'.$store_obj->closest_entrance.'</strong></div>';
					$jsonHtml .= '</div>';
					$jsonHtml .= '<div class=\"row mapplic-map-store-buttons\">';
					$jsonHtml .= '    <div class=\"col-6\"><a href=\"'. $fetchedUrl .'?location='.$store_obj->store_id.'\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\"></i></a></div>';
					$jsonHtml .= '    <div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: \''.$store_obj->id.'\'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i></a></div>';
					$jsonHtml .= '</div>';

					$mapplic_js_store_list .= '{
						"id": "'.$store_obj->store_id.'",';

						if($store_obj->logo){
							$mapplic_js_store_list .= '"thumbnail": "'.$store_obj->logo->getThumb(268, 231).'",';
						}

						$mapplic_js_store_list .= '"title": "'.$store_obj->name.'",
						"pin": "blue",';

						if($store_obj->has_category){
							$mapplic_js_store_list .= '"category": "'.$store_obj->category_name.'",';
						}

						$mapplic_js_store_list .= '"description": "'.$jsonHtml.'",
						"x": "'.$store_obj->store_x.'",
						"y": "'.$store_obj->store_y.'",
						"fill": "#ffffff",
						"zoom": "2"
					},';

				}
			}

			$mapplic_js_floor_list .= '{
				"id": "'.$floor_obj->floor_code.'",
				"title": "'.$floor_obj->name.'",
				"map": "'.$floor_obj->mall_map->path.'",';

			if($floor_obj->floor_code == 'LL'){
				$mapplic_js_floor_list .= '"show": true,';
			}

			$mapplic_js_floor_list .= '"locations": [
					'.$mapplic_js_store_list.'
				]
			},';
		}

		// set the JS needed for the mapplic plugin
		$mapplic_js = '(function( $ ){
	        var map = $("#mapplic").mapplic({
	            source : {
	                "mapwidth":"1200",
	                "mapheight":"746",
	                "minimap": false,
	                "clearbutton": false,
	                "zoombuttons": true,
	                "sidebar": true,
	                "search": true,
	                "hovertip": false,
	                "mousewheel": false,
	                "fullscreen": false,
	                "deeplinking": true,
	                "mapfill": true,
	                "zoom": false,
	                "alphabetic": true,
	                "zoomlimit": "2",
	                "categories": ['.$mapplic_js_cat_list.'],
	                "levels": ['.$mapplic_js_floor_list.'],
	                "maxscale": 2
				},
				deeplinking : false,
	            sidebar : true,
	            minimap : false,
	            markers : false,
	            fullscreen : true,
	            maxscale : 3
			});
			
			'.$mapplic_js_setLocation.'
	    })( jQuery );';
		// mapplic settings found here: http://www.athsites.com/athtestsites.com/map/docs/#options

		return $mapplic_js;
	}

	public static function saveMapFile() {
		$mapplic_js = Store::buildMapplicJS();

		$fileName = themes_path().'/canal-walk/assets/js/canal_walk_map.js';
		file_put_contents($fileName, $mapplic_js);

		$floor_obj_arr = Floor::where('mall_id', 1)->orderBy('sort_order')->get();

		// set the floor list
		$mapplic_js_floor_list = '';
		foreach ($floor_obj_arr as $floor_obj) {
			$mapplic_js_floor_list .= '{
				"id": "'.$floor_obj->floor_code.'",
				"title": "'.$floor_obj->name.'",
				"map": "'.$floor_obj->mall_map->path.'",
				"show": "true",
				"locations": [ ]
			},';
		}

		$mapplic_js_backend = '(function( $ ){
				var mapplic = $(\'#mapplic\').mapplic({
					source : {
						"mapwidth":"1200",
						"mapheight":"746",
						"minimap": false,
						"clearbutton": false,
						"zoombuttons": true,
						"sidebar": false,
						"search": false,
						"hovertip": false,
						"mousewheel": false,
						"fullscreen": false,
						"deeplinking": true,
						"mapfill": true,
						"zoom": false,
						"alphabetic": true,
						"zoomlimit": "2",
						"action": "tooltip",
						"categories": [
						],
						"levels": [
							'.$mapplic_js_floor_list.'
						],
						"maxscale": 2
					},
					sidebar : false, // Enable sidebar
					minimap : false, // Enable minimap
					markers : false, // Disable markers
					fullscreen : true, // Enable fullscreen
					developer: true,
					maxscale : 3
					// Setting maxscale to 3 times bigger than the original file
				});
			})( jQuery );';

		$file_name_backend = themes_path().'/canal-walk/assets/js/canal_walk_map_backend.js';
		file_put_contents($file_name_backend, $mapplic_js_backend);
	}

    //
    // Menu helpers
    //

    /**
     * Handler for the pages.menuitem.getTypeInfo event.
     * Returns a menu item type information. The type information is returned as array
     * with the following elements:
     * - references - a list of the item type reference options. The options are returned in the
     *   ["key"] => "title" format for options that don't have sub-options, and in the format
     *   ["key"] => ["title"=>"Option title", "items"=>[...]] for options that have sub-options. Optional,
     *   required only if the menu item type requires references.
     * - nesting - Boolean value indicating whether the item type supports nested items. Optional,
     *   false if omitted.
     * - dynamicItems - Boolean value indicating whether the item type could generate new menu items.
     *   Optional, false if omitted.
     * - cmsPages - a list of CMS pages (objects of the Cms\Classes\Page class), if the item type requires a CMS page reference to
     *   resolve the item URL.
     * @param string $type Specifies the menu item type
     * @return array Returns an array
     */
    public static function getMenuTypeInfo($type)
    {
        $result = [];

        if ($type == 'all-stores') {
            $result = [
                'dynamicItems' => true
            ];
        }

        if ($result) {
            $theme = Theme::getActiveTheme();

            $pages = CmsPage::listInTheme($theme, true);
            $cmsPages = [];

            foreach ($pages as $page) {
                if (!$page->hasComponent('storedetails')) {
                    continue;
				}

                $cmsPages[] = $page;
            }

            $result['cmsPages'] = $cmsPages;
        }

        return $result;
    }

    /**
     * Handler for the pages.menuitem.resolveItem event.
     * Returns information about a menu item. The result is an array
     * with the following keys:
     * - url - the menu item URL. Not required for menu item types that return all available records.
     *   The URL should be returned relative to the website root and include the subdirectory, if any.
     *   Use the Url::to() helper to generate the URLs.
     * - isActive - determines whether the menu item is active. Not required for menu item types that
     *   return all available records.
     * - items - an array of arrays with the same keys (url, isActive, items) + the title key.
     *   The items array should be added only if the $item's $nesting property value is TRUE.
     * @param \RainLab\Pages\Classes\MenuItem $item Specifies the menu item.
     * @param \Cms\Classes\Theme $theme Specifies the current theme.
     * @param string $url Specifies the current page URL, normalized, in lower case
     * The URL is specified relative to the website root, it includes the subdirectory name, if any.
     * @return mixed Returns an array. Returns null if the item cannot be resolved.
     */
    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = null;

        if ($item->type == 'all-stores') {
            $result = [
                'items' => []
            ];

            $posts = self::orderBy('name')
            ->get();

            foreach ($posts as $post) {
                $postItem = [
                    'title' => $post->name,
                    'url'   => self::getPostPageUrl($item->cmsPage, $post, $theme),
                    'mtime' => $post->updated_at,
                ];

                $postItem['isActive'] = $postItem['url'] == $url;

                $result['items'][] = $postItem;
            }
        }

        return $result;
    }

    /**
     * Returns URL of a post page.
     *
     * @param $pageCode
     * @param $category
     * @param $theme
     */
    protected static function getPostPageUrl($pageCode, $category, $theme)
    {
        $page = CmsPage::loadCached($theme, $pageCode);
        if (!$page) return;

        $paramName = 'slug';
        $params = [
            $paramName => $category->slug,
            'year' => $category->created_at->format('Y'),
            'month' => $category->created_at->format('m'),
            'day' => $category->created_at->format('d'),
        ];
        $url = CmsPage::url($page->getBaseFileName(), $params);

        return $url;
    }

}