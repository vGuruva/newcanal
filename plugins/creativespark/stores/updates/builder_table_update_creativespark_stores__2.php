<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStores2 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->string('id_external')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->dropColumn('id_external');
        });
    }
}
