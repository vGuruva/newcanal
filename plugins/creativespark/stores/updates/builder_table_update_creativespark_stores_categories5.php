<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresCategories5 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_categories', function($table)
        {
            $table->integer('hyprop_malls_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_categories', function($table)
        {
            $table->dropColumn('hyprop_malls_id');
        });
    }
}