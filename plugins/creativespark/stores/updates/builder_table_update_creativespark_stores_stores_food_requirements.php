<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresStoresFoodRequirements extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_stores_food_requirements', function($table)
        {
            $table->renameColumn('food_requirements_id', 'food_requirement_id');
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_stores_food_requirements', function($table)
        {
            $table->renameColumn('food_requirement_id', 'food_requirements_id');
        });
    }
}
