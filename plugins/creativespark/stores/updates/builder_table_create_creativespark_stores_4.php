<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStores4 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->integer('hyprop_malls_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->dropColumn('hyprop_malls_id');
        });
    }
}
