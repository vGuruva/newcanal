<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresCategories2 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_categories', function($table)
        {
            $table->integer('parent_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_categories', function($table)
        {
            $table->integer('parent_id')->nullable(false)->change();
        });
    }
}
