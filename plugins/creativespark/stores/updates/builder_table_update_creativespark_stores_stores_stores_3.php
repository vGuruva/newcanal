<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresStoresStores3 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_stores_stores', function($table)
        {
            $table->renameColumn('child_id', 'store_id');
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_stores_stores', function($table)
        {
            $table->renameColumn('store_id', 'child_id');
        });
    }
}
