<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresHours extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_hours', function($table)
        {
            $table->time('monday_start')->nullable()->change();
            $table->time('monday_end')->nullable()->change();
            $table->time('tuesday_start')->nullable()->change();
            $table->time('tuesday_end')->nullable()->change();
            $table->time('wednesday_start')->nullable()->change();
            $table->time('wednesday_end')->nullable()->change();
            $table->time('thursday_start')->nullable()->change();
            $table->time('thursday_end')->nullable()->change();
            $table->time('friday_start')->nullable()->change();
            $table->time('friday_end')->nullable()->change();
            $table->time('saturday_start')->nullable()->change();
            $table->time('saturday_end')->nullable()->change();
            $table->time('sunday_start')->nullable()->change();
            $table->time('sunday_end')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_hours', function($table)
        {
            $table->time('monday_start')->nullable(false)->change();
            $table->time('monday_end')->nullable(false)->change();
            $table->time('tuesday_start')->nullable(false)->change();
            $table->time('tuesday_end')->nullable(false)->change();
            $table->time('wednesday_start')->nullable(false)->change();
            $table->time('wednesday_end')->nullable(false)->change();
            $table->time('thursday_start')->nullable(false)->change();
            $table->time('thursday_end')->nullable(false)->change();
            $table->time('friday_start')->nullable(false)->change();
            $table->time('friday_end')->nullable(false)->change();
            $table->time('saturday_start')->nullable(false)->change();
            $table->time('saturday_end')->nullable(false)->change();
            $table->time('sunday_start')->nullable(false)->change();
            $table->time('sunday_end')->nullable(false)->change();
        });
    }
}
