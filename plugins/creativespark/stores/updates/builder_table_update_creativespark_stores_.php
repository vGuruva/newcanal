<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStores extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->integer('floor_level')->nullable();
            $table->string('shop_number');
            $table->text('description')->nullable()->change();
            $table->string('phone_number', 255)->nullable()->change();
            $table->string('email_address', 255)->nullable()->change();
            $table->integer('closest_entrance')->nullable()->change();
            $table->renameColumn('external_id', 'website');
            $table->dropColumn('store_level');
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->dropColumn('floor_level');
            $table->dropColumn('shop_number');
            $table->text('description')->nullable(false)->change();
            $table->string('phone_number', 255)->nullable(false)->change();
            $table->string('email_address', 255)->nullable(false)->change();
            $table->integer('closest_entrance')->nullable(false)->change();
            $table->renameColumn('website', 'external_id');
            $table->integer('store_level');
        });
    }
}
