<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresStoresStores extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_stores_stores', function($table)
        {
            $table->renameColumn('parent_id', 'store_id');
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_stores_stores', function($table)
        {
            $table->renameColumn('store_id', 'parent_id');
        });
    }
}
