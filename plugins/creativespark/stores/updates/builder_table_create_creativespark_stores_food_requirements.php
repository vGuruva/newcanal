<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkStoresFoodRequirements extends Migration
{
    public function up()
    {
        Schema::create('creativespark_stores_food_requirements', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_stores_food_requirements');
    }
}
