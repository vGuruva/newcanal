<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkStoresStoresFoodRequirements extends Migration
{
    public function up()
    {
        Schema::create('creativespark_stores_stores_food_requirements', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('store_id');
            $table->integer('food_requirements_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_stores_stores_food_requirements');
    }
}
