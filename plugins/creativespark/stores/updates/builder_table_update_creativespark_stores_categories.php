<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStoresCategories extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_categories', function($table)
        {
            $table->integer('parent_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_categories', function($table)
        {
            $table->dropColumn('parent_id');
        });
    }
}
