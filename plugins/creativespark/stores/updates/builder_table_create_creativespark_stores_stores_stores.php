<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkStoresStoresStores extends Migration
{
    public function up()
    {
        Schema::create('creativespark_stores_stores_stores', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('parent_id');
            $table->integer('child_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_stores_stores_stores');
    }
}
