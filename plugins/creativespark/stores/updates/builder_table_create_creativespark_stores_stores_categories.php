<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkStoresStoresCategories extends Migration
{
    public function up()
    {
        Schema::create('creativespark_stores_stores_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('store_id');
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_stores_stores_categories');
    }
}
