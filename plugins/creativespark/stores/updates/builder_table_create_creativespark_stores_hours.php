<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkStoresHours extends Migration
{
    public function up()
    {
        Schema::create('creativespark_stores_hours', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('store_id')->unsigned();
            $table->time('monday_start');
            $table->time('monday_end');
            $table->time('tuesday_start');
            $table->time('tuesday_end');
            $table->time('wednesday_start');
            $table->time('wednesday_end');
            $table->time('thursday_start');
            $table->time('thursday_end');
            $table->time('friday_start');
            $table->time('friday_end');
            $table->time('saturday_start');
            $table->time('saturday_end');
            $table->time('sunday_start');
            $table->time('sunday_end');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_stores_hours');
    }
}
