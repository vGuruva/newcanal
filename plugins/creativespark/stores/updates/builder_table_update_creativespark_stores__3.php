<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCreativesparkStores3 extends Migration
{
    public function up()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->decimal('store_x', 10, 0)->nullable();
            $table->decimal('store_y', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('creativespark_stores_', function($table)
        {
            $table->dropColumn('store_x');
            $table->dropColumn('store_y');
        });
    }
}
