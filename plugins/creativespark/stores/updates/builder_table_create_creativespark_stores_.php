<?php namespace CreativeSpark\Stores\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCreativesparkStores extends Migration
{
    public function up()
    {
        Schema::create('creativespark_stores_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->string('operating_hours_first')->nullable();
            $table->string('operating_hours_second')->nullable();
            $table->string('phone_number');
            $table->string('email_address');
            $table->integer('store_level');
            $table->integer('closest_entrance');
            $table->boolean('show_contact')->default(1);
            $table->boolean('has_ecommerce')->default(0);
            $table->string('external_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('creativespark_stores_');
    }
}
