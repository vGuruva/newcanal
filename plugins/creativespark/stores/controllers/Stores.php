<?php namespace CreativeSpark\Stores\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Redirect;
use Input;

class Stores extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
    	'Backend\Behaviors\FormController',
    	'Backend\Behaviors\ReorderController',
        'Backend.Behaviors.ImportExportController',
    	'Backend\Behaviors\RelationController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('CreativeSpark.Stores', 'main-menu-item');

	    $this->addCss("/plugins/creativespark/stores/assets/css/mapplic.css", "1.0.0");
	    $this->addCss("/plugins/creativespark/stores/assets/css/map.css", "1.0.0");

	    $this->addJs("/plugins/creativespark/stores/assets/js/hammer.min.js", "1.0.0");
	    $this->addJs("/plugins/creativespark/stores/assets/js/jquery.mousewheel.js", "1.0.0");
	    $this->addJs("/plugins/creativespark/stores/assets/js/map_script.js", "1.0.0");
        $this->addJs("/plugins/creativespark/stores/assets/js/mapplic.js", "1.0.0");
    }

    public function listExtendQuery($query)
    {
        // get the mall's items
        if( !empty($this->user->role->mall_id) ) {
            
            if( is_int($this->user->role->mall_id) ):
                $query->where( 'hyprop_malls_id', '=', $this->user->role->mall_id );
            endif;
            
        } else { // else get nothing
            
            $query->whereRaw( '1 = 0' );
            
        }

    }
    
}
