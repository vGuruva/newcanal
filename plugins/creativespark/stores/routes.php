<?php
use CreativeSpark\Stores\Models\Store;
use CreativeSpark\Stores\Models\Category;
use JorgeAndrade\Events\Models\Event;
use JorgeAndrade\Events\Models\Calendar;
use Hyprop\Movies\Models\Movie;
use Hyprop\Movies\Models\Genre;
use Creativespark\AppInterests\Models\AppInterest;
use RainLab\User\Models\User;
use Cms\Classes\Theme;

Route::get('/api/v0/{mall_id}/stores', function ($mall_id) {
    $stores_arr = Store::select('creativespark_stores_.*', 'system_files.disk_name')
    ->addSelect(Db::raw("(SELECT creativespark_stores_categories.name
                            FROM creativespark_stores_categories
                            LEFT JOIN creativespark_stores_stores_categories ON creativespark_stores_stores_categories.category_id = creativespark_stores_categories.id
                            WHERE creativespark_stores_stores_categories.store_id = creativespark_stores_.id limit 1) AS category_name"))
    ->where('hyprop_malls_id', $mall_id)
    ->where('system_files.attachment_type', '=', 'CreativeSpark\\Stores\\Models\\Store')
    ->where('system_files.field', '=', 'logo')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'creativespark_stores_.id')
    ->get();

    $stores = array();
    foreach($stores_arr as $store){
        $store->attributes['logo'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($store->disk_name, 0, 3).'/'.substr($store->disk_name, 3, 3).'/'.substr($store->disk_name, 6, 3).'/'.$store->disk_name;
        unset($store->attributes['disk_name']);

        $stores[] = $store;
    }

    return $stores;
});

Route::get('/api/v0/{mall_id}/stores/{id}', function ($mall_id, $id) {
    $store = Store::select('creativespark_stores_.*', 'system_files.disk_name')
    ->addSelect(Db::raw("(SELECT creativespark_stores_categories.name
                            FROM creativespark_stores_categories
                            LEFT JOIN creativespark_stores_stores_categories ON creativespark_stores_stores_categories.category_id = creativespark_stores_categories.id
                            WHERE creativespark_stores_stores_categories.store_id = creativespark_stores_.id limit 1) AS category_name"))
    ->where('creativespark_stores_.id', '=', $id)
    ->where('hyprop_malls_id', $mall_id)
    ->where('system_files.attachment_type', '=', 'CreativeSpark\Stores\Models\Store')
    ->where('system_files.field', '=', 'logo')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'creativespark_stores_.id')
    ->first();

    $store->attributes['logo'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($store->disk_name, 0, 3).'/'.substr($store->disk_name, 3, 3).'/'.substr($store->disk_name, 6, 3).'/'.$store->disk_name;
    $store->attributes['category_name'] = $store->categories->first()->name;
    unset($store->attributes['disk_name']);

    return $store;
});

Route::post('/api/v0/fav_stores_action', function (\Request $request) {
    
    if( !Request::get('store_id') || !Request::get('user_id') ) {
        return  response()->json(['error' => 'datails not found'], 500);
    }
    
    $store = DB::table('creativespark_stores_stores_users')->where( 'user_id', Request::get('user_id') )->where( 'store_id', Request::get('store_id'))->get();
    
    if( !count( $store ) ):
        
        Db::table('creativespark_stores_stores_users')->insert(
            ['store_id' => Request::get('store_id'), 'user_id' => Request::get('user_id')]
        );
    
        return response()->json(('added store to favorite'));
        
    endif;

    
    return  response()->json([ 'this store is already in your favorite list']);
    

})->middleware('jwt.auth');

Route::post('/api/v0/fav_stores', function (\Request $request) {
    if(! Request::get('user_id') ) {
        return  response()->json(['error' => 'user not found'], 500);
    }

    $stores = Store::select('creativespark_stores_.*', 'system_files.disk_name')
    ->addSelect(Db::raw("(SELECT creativespark_stores_categories.name
                            FROM creativespark_stores_categories
                            LEFT JOIN creativespark_stores_stores_categories ON creativespark_stores_stores_categories.category_id = creativespark_stores_categories.id
                            WHERE creativespark_stores_stores_categories.store_id = creativespark_stores_.id limit 1) AS category_name"))
    ->where('creativespark_stores_stores_users.user_id', Request::get('user_id') )
    ->where('system_files.attachment_type', '=', 'CreativeSpark\Stores\Models\Store')
    ->where('system_files.field', '=', 'logo')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'creativespark_stores_.id')
    ->leftJoin('creativespark_stores_stores_users', 'creativespark_stores_stores_users.store_id', '=', 'creativespark_stores_.id')
    ->get();
    
    foreach($stores as $store):
        
        $store->attributes['logo'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($store->disk_name, 0, 3).'/'.substr($store->disk_name, 3, 3).'/'.substr($store->disk_name, 6, 3).'/'.$store->disk_name;
        $store->attributes['category_name'] = $store->categories->first()->name;
        unset($store->attributes['disk_name']);        
        
    endforeach;

    return $stores;

    
})->middleware('jwt.auth');

Route::get('/api/v0/{mall_id}/stores-cat', function ($mall_id) {
    $category = Category::where('hyprop_malls_id', $mall_id)->get();

    return $category;
});

Route::get('/api/v0/{mall_id}/stores-cat/{id}', function ($mall_id, $id) {
    $category_arr = Store::select('creativespark_stores_.*', 'system_files.disk_name')
    ->addSelect(Db::raw("(SELECT creativespark_stores_categories.name
                            FROM creativespark_stores_categories
                            LEFT JOIN creativespark_stores_stores_categories ON creativespark_stores_stores_categories.category_id = creativespark_stores_categories.id
                            WHERE creativespark_stores_stores_categories.store_id = creativespark_stores_.id limit 1) AS category_name"))
    ->where('hyprop_malls_id', $mall_id)
    ->where('system_files.attachment_type', '=', 'CreativeSpark\Stores\Models\Store')
    ->where('system_files.field', '=', 'logo')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'creativespark_stores_.id')
    ->leftJoin('creativespark_stores_stores_categories', 'creativespark_stores_.id', '=', 'creativespark_stores_stores_categories.store_id')
    ->where('creativespark_stores_stores_categories.category_id', $id)
    ->get();

    $category = array();
    foreach($category_arr as $category_obj){
        $category_obj->attributes['logo'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($category_obj->disk_name, 0, 3).'/'.substr($category_obj->disk_name, 3, 3).'/'.substr($category_obj->disk_name, 6, 3).'/'.$category_obj->disk_name;
        unset($category_obj->attributes['disk_name']);

        $category[] = $category_obj;
    }

    return $category;
});

Route::get('/api/v0/{mall_id}/calendars/{calendars}', function ($mall_id, $calendars) {
    $event_arr = Calendar::select('jorgeandrade_events_events.*', 'system_files.disk_name')
    ->where('jorgeandrade_events_calendars.slug', $calendars)
    ->where('jorgeandrade_events_events.hyprop_malls_id', $mall_id)
    ->whereRaw("ends_at >= '".date('Y-m-d H:i:s')."'")
    ->where('system_files.attachment_type', '=', 'JorgeAndrade\Events\Models\Event')
    ->where('system_files.field', '=', 'banner')
    ->leftJoin('jorgeandrade_events_events', 'jorgeandrade_events_calendars.id', '=', 'jorgeandrade_events_events.calendar_id')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'jorgeandrade_events_events.id')
    ->get();

    $event = array();
    foreach($event_arr as $event_obj){
        if($event_obj->disk_name){
            $event_obj->attributes['poster'] = Config::get('app.url').'storage/app/uploads/public/'.substr($event_obj->disk_name, 0, 3).'/'.substr($event_obj->disk_name, 3, 3).'/'.substr($event_obj->disk_name, 6, 3).'/'.$event_obj->disk_name;
        } else{
            $event_obj->attributes['poster'] = Config::get('app.url').'storage/app/uploads/noimage.jpg';
        }
        unset($event_obj->attributes['disk_name']);

        $event[] = $event_obj;
    }

    return $event;
});

// USELESS CALENDAR VAR (needed for angular)
Route::get('/api/v0/{mall_id}/calendars/{calendars}/{id}', function ($mall_id, $calendars, $id) {
    $category = Event::select('jorgeandrade_events_events.*', 'system_files.disk_name')
    ->where('hyprop_malls_id', $mall_id)
    ->where('jorgeandrade_events_events.id', '=', $id)
    ->where('system_files.attachment_type', '=', 'JorgeAndrade\Events\Models\Event')
    ->where('system_files.field', '=', 'banner')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'jorgeandrade_events_events.id')
    ->groupBy('jorgeandrade_events_events.id')
    ->first();

    if($category->disk_name){
        $category->attributes['poster'] = Config::get('app.url').'storage/app/uploads/public/'.substr($category->disk_name, 0, 3).'/'.substr($category->disk_name, 3, 3).'/'.substr($category->disk_name, 6, 3).'/'.$category->disk_name;
    } else{
        $category->attributes['poster'] = Config::get('app.url').'storage/app/uploads/noimage.jpg';
    }
    unset($category->attributes['disk_name']);

    return $category;
});

Route::get('/api/v0/{mall_id}/movies', function ($mall_id) {
    $movies_arr = Movie::select('hyprop_movies_.*', 'system_files.disk_name')
    ->addSelect(Db::raw("(SELECT GROUP_CONCAT(hyprop_movies_genres.genre_title SEPARATOR ', ')
        FROM hyprop_movies_genres
        LEFT JOIN hyprop_movies_movies_genres ON hyprop_movies_genres.id = hyprop_movies_movies_genres.genre_id
        WHERE hyprop_movies_.id = hyprop_movies_movies_genres.movie_id) AS genres"))
    ->where('system_files.attachment_type', '=', 'Hyprop\Movies\Models\Movie')
    ->where('system_files.field', '=', 'movie_poster_land')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'hyprop_movies_.id')
    ->orderBy('release', 'desc')
    ->get();

    $movies = array();
    foreach($movies_arr as $movie_obj){
        if($movie_obj->disk_name){
            $movie_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($movie_obj->disk_name, 0, 3).'/'.substr($movie_obj->disk_name, 3, 3).'/'.substr($movie_obj->disk_name, 6, 3).'/'.$movie_obj->disk_name;
        } else{
            $movie_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/noimage.jpg';
        }
        unset($movie_obj->attributes['disk_name']);

        $movies[] = $movie_obj;
    }

    return $movies;
});

Route::get('/api/v0/{mall_id}/movies/{id}', function ($mall_id, $id) {
    $movie = Movie::select('hyprop_movies_.*', 'system_files.disk_name')
    ->where('hyprop_movies_.id', '=', $id)
    ->addSelect(Db::raw("(SELECT GROUP_CONCAT(hyprop_movies_genres.genre_title SEPARATOR ', ')
        FROM hyprop_movies_genres
        LEFT JOIN hyprop_movies_movies_genres ON hyprop_movies_genres.id = hyprop_movies_movies_genres.genre_id
        WHERE hyprop_movies_.id = hyprop_movies_movies_genres.movie_id) AS genres"))
    ->where('system_files.attachment_type', '=', 'Hyprop\Movies\Models\Movie')
    ->where('system_files.field', '=', 'movie_poster_land')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'hyprop_movies_.id')
    ->orderBy('release', 'desc')
    ->first();

    if($movie->disk_name){
        $movie->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($movie->disk_name, 0, 3).'/'.substr($movie->disk_name, 3, 3).'/'.substr($movie->disk_name, 6, 3).'/'.$movie->disk_name;
    } else{
        $movie->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/noimage.jpg';
    }
    unset($movie->attributes['disk_name']);

    return $movie;
});

Route::get('/api/v0/{mall_id}/movies-genre', function ($mall_id) {
    $genre = Genre::get();

    return $genre;
});

Route::get('/api/v0/{mall_id}/movies-genre/{slug}', function ($mall_id, $slug) {
    $genre_arr = Movie::select('hyprop_movies_.*', 'system_files.disk_name')
    ->where('hyprop_movies_genres.slug', '=', $slug)
    ->leftJoin('hyprop_movies_movies_genres', 'hyprop_movies_movies_genres.movie_id', '=', 'hyprop_movies_.id')
    ->leftJoin('hyprop_movies_genres', 'hyprop_movies_movies_genres.genre_id', '=', 'hyprop_movies_genres.id')
    ->addSelect(Db::raw("(SELECT GROUP_CONCAT(hyprop_movies_genres.genre_title SEPARATOR ', ')
        FROM hyprop_movies_genres
        LEFT JOIN hyprop_movies_movies_genres ON hyprop_movies_genres.id = hyprop_movies_movies_genres.genre_id
        WHERE hyprop_movies_.id = hyprop_movies_movies_genres.movie_id) AS genres"))
    ->where('system_files.attachment_type', '=', 'Hyprop\Movies\Models\Movie')
    ->where('system_files.field', '=', 'movie_poster_land')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'hyprop_movies_.id')
    ->orderBy('release', 'desc')
    ->get();

    $genre = array();
    foreach($genre_arr as $genre_obj){
        if($genre_obj->disk_name){
            $genre_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($genre_obj->disk_name, 0, 3).'/'.substr($genre_obj->disk_name, 3, 3).'/'.substr($genre_obj->disk_name, 6, 3).'/'.$genre_obj->disk_name;
        } else{
            $genre_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/noimage.jpg';
        }
        unset($genre_obj->attributes['disk_name']);

        $genre[] = $genre_obj;
    }

    return $genre;
});

Route::get('/api/v0/{mall_id}/movies-latest', function ($mall_id) {
    $latest_arr = Movie::select('hyprop_movies_.*', 'landPoster.disk_name as disk_landscape', 'poster_file.disk_name as disk_poster')
    ->addSelect(Db::raw("(SELECT GROUP_CONCAT(hyprop_movies_genres.genre_title SEPARATOR ', ')
        FROM hyprop_movies_genres
        LEFT JOIN hyprop_movies_movies_genres ON hyprop_movies_genres.id = hyprop_movies_movies_genres.genre_id
        WHERE hyprop_movies_.id = hyprop_movies_movies_genres.movie_id) AS genres"))
    ->where('landPoster.attachment_type', '=', 'Hyprop\Movies\Models\Movie')
    ->where('landPoster.field', '=', 'movie_poster_land')
    ->leftJoin('system_files as landPoster', 'landPoster.attachment_id', '=', 'hyprop_movies_.id')
    ->where('poster_file.attachment_type', '=', 'Hyprop\Movies\Models\Movie')
    ->where('poster_file.field', '=', 'movie_poster')
    ->leftJoin('system_files as poster_file', 'poster_file.attachment_id', '=', 'hyprop_movies_.id')
    ->where('release', '<=', date("Y-m-d"))
    ->orderBy('release', 'desc')
    ->get();

    $latest = array();
    foreach($latest_arr as $latest_obj){
        if($latest_obj->disk_landscape){
            $latest_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($latest_obj->disk_landscape, 0, 3).'/'.substr($latest_obj->disk_landscape, 3, 3).'/'.substr($latest_obj->disk_landscape, 6, 3).'/'.$latest_obj->disk_landscape;
        } else{
            $latest_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/noimage.jpg';
        }
        unset($latest_obj->attributes['disk_landscape']);

        if($latest_obj->disk_poster){
            $latest_obj->attributes['poster'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($latest_obj->disk_poster, 0, 3).'/'.substr($latest_obj->disk_poster, 3, 3).'/'.substr($latest_obj->disk_poster, 6, 3).'/'.$latest_obj->disk_poster;
        } else{
            $latest_obj->attributes['poster'] = Config::get('app.url').'/storage/app/uploads/noimage.jpg';
        }
        unset($latest_obj->attributes['disk_poster']);

        $latest[] = $latest_obj;
    }

    return $latest;
});


Route::get('/api/v0/{mall_id}/movies-forthcoming', function ($mall_id) {
    $forthcoming_arr = Movie::select('hyprop_movies_.*', 'system_files.disk_name')
    ->addSelect(Db::raw("(SELECT GROUP_CONCAT(hyprop_movies_genres.genre_title SEPARATOR ', ')
    FROM hyprop_movies_genres
    LEFT JOIN hyprop_movies_movies_genres ON hyprop_movies_genres.id = hyprop_movies_movies_genres.genre_id
    WHERE hyprop_movies_.id = hyprop_movies_movies_genres.movie_id) AS genres"))
    ->where('system_files.attachment_type', '=', 'Hyprop\Movies\Models\Movie')
    ->where('system_files.field', '=', 'movie_poster_land')
    ->leftJoin('system_files', 'system_files.attachment_id', '=', 'hyprop_movies_.id')
    ->where('release', '>', date("Y-m-d"))
    ->orderBy('release', 'desc')
    ->get();

    $forthcoming = array();
    foreach($forthcoming_arr as $forthcoming_obj){
        if($forthcoming_obj->disk_name){
            $forthcoming_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/public/'.substr($forthcoming_obj->disk_name, 0, 3).'/'.substr($forthcoming_obj->disk_name, 3, 3).'/'.substr($forthcoming_obj->disk_name, 6, 3).'/'.$forthcoming_obj->disk_name;
        } else{
            $forthcoming_obj->attributes['poster_landscape'] = Config::get('app.url').'/storage/app/uploads/noimage.jpg';
        }
        unset($forthcoming_obj->attributes['disk_name']);

        $forthcoming[] = $forthcoming_obj;
    }

    return $forthcoming;
});

Route::get('/api/v0/{mall_id}/app-interests', function ($mall_id) {
    return AppInterest::get();
});


Route::post('/api/v0/{mall_id}/user-details', function ($mall_id) {

    if(!Request::get('user_id')) {
        return  response()->json(['error' => 'user not found'], 500);
    }

    return User::get(Request::get('user_id'));

});

/*
Route::post('test', function (\Request $request) {
   return response()->json(('The test was successful'));
})->middleware('\Tymon\JWTAuth\Middleware\GetUserFromToken');


User::extend(function($model) {
        $model->addDynamicMethod('getAuthApiSignupAttributes', function () use ($model) {
            return [
                'my-attr' => $model->my_attr,
            ];
        });
    });

    */
