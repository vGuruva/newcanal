(function( $ ){
			var mapplic = $('#mapplic').mapplic({
				source : {
				    "mapwidth":"1200",
				    "mapheight":"650",
					"minimap": false,
					"clearbutton": false,
					"zoombuttons": true,
					"sidebar": true,
					"search": true,
					"hovertip": false,
					"mousewheel": false,
					"fullscreen": false,
					"deeplinking": true,
					"mapfill": true,
					"zoom": false,
					"alphabetic": true,
					"zoomlimit": "2",
					"categories": [
					    {
							"id": "landmarks",
							"title": "Landmarks",
							"color": "#666666",
							"show": "false"
						},{
				"id": "Banks & Forex",
				"title": "Banks & Forex",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Books, Cards & Gifts",
				"title": "Books, Cards & Gifts",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Coffee Shops",
				"title": "Coffee Shops",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Department Shops",
				"title": "Department Shops",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Eyewear & Optometrists",
				"title": "Eyewear & Optometrists",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Fashion & Footwear",
				"title": "Fashion & Footwear",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Fast Food & Confectionary",
				"title": "Fast Food & Confectionary",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Food & Beverage",
				"title": "Food & Beverage",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Footwear",
				"title": "Footwear",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Groceries & Supermarkets",
				"title": "Groceries & Supermarkets",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Health Food",
				"title": "Health Food",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Health, Hair & Beauty",
				"title": "Health, Hair & Beauty",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Home & D&eacute;cor",
				"title": "Home & D&eacute;cor",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Jewellery & Accessories",
				"title": "Jewellery & Accessories",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Kids Fashion",
				"title": "Kids Fashion",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Leather & Luggage",
				"title": "Leather & Luggage",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Liquor",
				"title": "Liquor",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Men's Fashion",
				"title": "Men's Fashion",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Movies & Entertainment",
				"title": "Movies & Entertainment",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Pet Shops",
				"title": "Pet Shops",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Restaurants",
				"title": "Restaurants",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Speciality",
				"title": "Speciality",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Speciality Shops & Services",
				"title": "Speciality Shops & Services",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Sport, Fitness & Lifestyle",
				"title": "Sport, Fitness & Lifestyle",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Technology",
				"title": "Technology",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Tobacconists & E-Cigarettes",
				"title": "Tobacconists & E-Cigarettes",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Toys & Hobbies",
				"title": "Toys & Hobbies",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Travel",
				"title": "Travel",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Unisex Fashion",
				"title": "Unisex Fashion",
				"color": "#666666",
				"show": "false"
			},{
				"id": "Womens Fashion",
				"title": "Womens Fashion",
				"color": "#666666",
				"show": "false"
			}
					],
					"levels": [
						{
							"id": "UL",
							"title": "Lower Level",
							"map": "http://18.216.225.141/themes/canal-walk/assets/img/mall/CW_mall_map_V1_lower.svg",
							"show": "true",
							"locations": [
								{
									"id": "FC",
									"title": "Food Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.4830",
									"y": "0.3525"
								},
								{
									"id": "MoonBay",
									"title": "Half Moon Bay",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.4826",
									"y": "0.2256"
								},
								{
									"id": "Entrance1",
									"title": "Entrance 1",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.1921",
									"y": "0.5834"
								},
								{
									"id": "Entrance2",
									"title": "Entrance 2",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.3869",
									"y": "0.7613"
								},
								{
									"id": "Entrance3",
									"title": "Entrance 3",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.5744",
									"y": "0.7613"
								},
								{
									"id": "Entrance4",
									"title": "Entrance 4",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.7667",
									"y": "0.5855"
								},
								{
									"id": "CSDE1",
									"title": "Customer Service Desk (Entrance 1)",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.2295",
									"y": "0.5256"
								},
								{
									"id": "CSDE4",
									"title": "Customer Service Desk (Entrance 4)",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.7353",
									"y": "0.5243"
								},
								{
									"id": "CPC",
									"title": "Checkers Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.2308",
									"y": "0.4885"
								},
								{
									"id": "PnPPC",
									"title": "Pick n Pay Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.7374",
									"y": "0.5089"
								},
								{
									"id": "CC",
									"title": "Centre Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.4823",
									"y": "0.4631"
								},
								{
									"id": "Market Lane",
									"title": "Market Lane",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.6437",
									"y": "0.5117"
								},
								{
									"id": "Trading Post",
									"title": "Trading Post",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.3230",
									"y": "0.5089"
								},{
				"id": "LL122","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/319/5a5f23319c1c5443967301.png","title": "@home",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">@home<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/home?location=LL122\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '2'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6241",
				"y": "0.5133",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL5","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76a/bcd/5a376abcdc324195708138.png","title": "Absolute Pets 1",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Absolute Pets 1<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/absolute-pets-1?location=LL5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '5'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.198",
				"y": "0.505",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL121","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76a/d18/5a376ad188248890795570.png","title": "Absolute Pets 2",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Absolute Pets 2<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/absolute-pets-2?location=LL121\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '6'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.736",
				"y": "0.4359",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL195","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76a/f97/5a376af97ac2c719476745.png","title": "Accessorize",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Accessorize<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/accessorize?location=LL195\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '7'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5628",
				"y": "0.5983",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL56","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/5d3/5a5f235d346f0814410450.png","title": "Adidas Kids",
				"pin": "hidden","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion<h3 class=\"mapplic-map-store-name\">Adidas Kids<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/adidas-kids?location=LL56\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '10'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5149",
				"y": "0.4533",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL170","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/5a9/5a376b5a9b26c079546334.png","title": "ALDO",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">ALDO<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/aldo?location=LL170\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '12'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2911",
				"y": "0.5167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB10","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6fa/ae8/5a26faae8cf8a903546169.png","title": "All Leather",
				"pin": "hidden","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage<h3 class=\"mapplic-map-store-name\">All Leather<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/all-leather?location=LLB10\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '13'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL229","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/712/5a376b71293d1293865913.png","title": "American Express Forex",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">American Express Forex<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/american-express-forex?location=LL229\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '14'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3839",
				"y": "0.7118",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC9","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/731/5a5f237311a9f621111124.png","title": "Anat Falafel & Shwarma",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Anat Falafel & Shwarma<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/anat-falafel-shwarma?location=LLFC9\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '17'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.505",
				"y": "0.3417",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL74","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/da1/5a376bda103c7018474775.png","title": "Audiolens",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Audiolens<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/audiolens?location=LL74\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '20'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6088",
				"y": "0.4617",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL13","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76c/1d8/5a376c1d8a6df439084933.png","title": "Barksole",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Barksole<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/barksole?location=LL13\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '22'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2125",
				"y": "0.445",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL17","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76c/5e2/5a376c5e22d00379451974.png","title": "Baskin Robbins",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Baskin Robbins<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/baskin-robbins?location=LL17\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '23'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2287",
				"y": "0.4233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL36","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76c/826/5a376c8268388857478781.png","title": "Bata Shoes",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Bata Shoes<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bata-shoes?location=LL36\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '24'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4066",
				"y": "0.4583",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA8","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76c/9c9/5a376c9c90ba9592573715.png","title": "Bayete Basics",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Bayete Basics<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bayete-basics?location=LLA8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '25'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL215","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/be5/5a5f23be5a279551420549.png","title": "Bella & Me Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Bella & Me Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bella-me-jewellers?location=LL215\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '27'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4237",
				"y": "0.59",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB1","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/d46/5a5f23d465b99571929685.png","title": "Besties",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Besties<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/besties?location=LLB1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '28'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL28","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/e98/5a5f23e9844a5593694049.png","title": "Big Blue",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Big Blue<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/big-blue?location=LL28\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '30'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3542",
				"y": "0.4583",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL57","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/774/bdf/5a3774bdf2f0b526284704.png","title": "Blades & Triggers",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Blades & Triggers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/blades-triggers?location=LL57\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '32'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.431",
				"y": "0.3983",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL99","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/774/f18/5a3774f18ae5f622355138.png","title": "Boardmans",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Boardmans<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/boardmans?location=LL99\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '35'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5916",
				"y": "0.3533",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL127","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/5c2/5a5f245c2d319557355083.png","title": "Boesmanland Biltong",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Boesmanland Biltong<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/boesmanland-biltong?location=LL127\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '38'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7632",
				"y": "0.4667",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL47","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/9cd/5a5f249cdb629517460143.png","title": "BT Games",
				"pin": "hidden","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment<h3 class=\"mapplic-map-store-name\">BT Games<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bt-games?location=LL47\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '42'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3903",
				"y": "0.385",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL233","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/785/19a/5a278519af9eb674469218.png","title": "Budget Keys",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Budget Keys<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>0<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/budget-keys?location=LL233\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '43'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.39",
				"y": "0.74",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC1","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/9a9/5a37759a9c629475744952.png","title": "Calamari King",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Calamari King<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/calamari-king?location=LLFC1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '47'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4616",
				"y": "0.3517",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB18","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/cc0/5a5f24cc06ffd684751089.png","title": "Cape Town Spice Market",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Cape Town Spice Market<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cape-town-spice-market?location=LLB18\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '49'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL152","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/dc8/5a3775dc810b7656473308.png","title": "Cape Union Mart",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Cape Union Mart<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cape-union-mart?location=LL152\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '50'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.44",
				"y": "0.4567",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL171","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/f26/5a3775f26030b512856059.png","title": "Capitec Bank",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">Capitec Bank<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/capitec-bank?location=LL171\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '52'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6088",
				"y": "0.6817",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL269","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/0d3/5a37760d3e8b7204360993.png","title": "Cardies",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Cardies<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cardies?location=LL269\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '53'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2405",
				"y": "0.575",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL255A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/519/5a3776519702c943826825.png","title": "Carrol Boyes",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Carrol Boyes<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/carrol-boyes?location=LL255A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '56'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3136",
				"y": "0.595",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA6","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/e88/5a5f24e886090360976399.png","title": "Casari",
				"pin": "hidden","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage<h3 class=\"mapplic-map-store-name\">Casari<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/casari?location=LLA6\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '57'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL15","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/7c1/5a37767c1f198235549463.png","title": "Cell C",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Cell C<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cell-c-2?location=LL15\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '59'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2206",
				"y": "0.435",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL180","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/9d4/5a37769d44c6c429850799.png","title": "Cellini",
				"pin": "hidden","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage<h3 class=\"mapplic-map-store-name\">Cellini<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cellini?location=LL180\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '60'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2522",
				"y": "0.51",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL273A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/b13/5a3776b138249428683295.png","title": "Cellucity",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Cellucity<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cellucity?location=LL273A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '61'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2143",
				"y": "0.565",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB4","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/787/5e9/5a27875e96e6a709747895.png","title": "Cellular Supreme",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Cellular Supreme<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cellular-supreme?location=LLB4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '62'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL181","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/787/887/5a2787887b851648206533.png","title": "Cellular Village",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Cellular Village<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cellular-village-1?location=LL181\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '63'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5655",
				"y": "0.7467",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL140","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/cad/5a3776cadc178188809569.png","title": "Charles & Keith",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Charles & Keith<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/charles-keith?location=LL140\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '67'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.519",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL11","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/e29/5a3776e2965a4176196233.png","title": "Checkers",
				"pin": "hidden","category": "Groceries & Supermarkets","description": "<h4 class=\"mapplic-map-store-category\">Groceries & Supermarkets<h3 class=\"mapplic-map-store-name\">Checkers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/checkers?location=LL11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '68'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.1682",
				"y": "0.4283",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB12","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/376/5a5f25376fc3b476799807.png","title": "Chinese Finger Press Massage",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Chinese Finger Press Massage<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/chinese-finger-press-massage?location=LLB12\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '70'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL141","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/777/1e5/5a37771e54eb4657668201.png","title": "Cinnabon",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Cinnabon<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cinnabon?location=LL141\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '71'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7596",
				"y": "0.5617",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFCK3","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/778/a98/5a3778a987eab169829176.png","title": "Cinnabon Kiosk",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Cinnabon Kiosk<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cinnabon-kiosk?location=LLFCK3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '72'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4845",
				"y": "0.3923",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL22","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/777/4a6/5a37774a671ef311898545.png","title": "Claire's",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Claire's<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/claires?location=LL22\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '73'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3262",
				"y": "0.4617",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL103","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/777/68a/5a377768a3014522820634.png","title": "Clicks",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Clicks<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/clicks?location=LL103\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '74'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.625",
				"y": "0.36",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL51","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/777/7ea/5a37777eabd7a469774275.png","title": "CNA",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">CNA<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cna?location=LL51\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '75'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4075",
				"y": "0.3667",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL167","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/777/f3b/5a3777f3b35c7894450457.png","title": "Col'Cacchio",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Col'Cacchio<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/colcacchio?location=LL167\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '77'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6034",
				"y": "0.6509",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL166","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/778/118/5a37781187d77534338982.png","title": "Colette by Colette Hayman",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Colette by Colette Hayman<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/colette-colette-hayman?location=LL166\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '78'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3244",
				"y": "0.5167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL11","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/778/d12/5a3778d12c0d9398936160.png","title": "Computicket",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Computicket<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/computicket?location=LL11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '80'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.1762",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL255","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/778/e9c/5a3778e9c2e48514394026.png","title": "Coricraft",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Coricraft<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/coricraft?location=LL255\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '81'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3561",
				"y": "0.6651",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL85","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/779/1d3/5a37791d38ac1511983875.png","title": "Cotton On Superstore",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Cotton On Superstore<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cotton-superstore?location=LL85\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '82'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5411",
				"y": "0.3567",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL33","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77b/c9b/5a377bc9ba01e169089625.png","title": "CUM Books",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">CUM Books<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cum-books?location=LL33\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '87'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3153",
				"y": "0.4133",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC3","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77b/f19/5a377bf199ab7665155475.png","title": "Debonairs Pizza",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Debonairs Pizza<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/debonairs-pizza?location=LLFC3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '88'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4562",
				"y": "0.32",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL20","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/6c5/5a5f256c5145d208160016.png","title": "Decor East Persian Carpets",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Decor East Persian Carpets<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/decor-east-persian-carpets?location=LL20\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '89'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3037",
				"y": "0.4717",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL109","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/823/5a5f25823cda2013481803.png","title": "Dion Wired",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Dion Wired<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/dion-wired?location=LL109\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '92'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6991",
				"y": "0.365",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL37","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77d/ab2/5a377dab244a4951698854.png","title": "Dis-Chem",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Dis-Chem<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/dis-chem?location=LL37\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '93'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3587",
				"y": "0.375",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL107","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/9fd/5a5f259fdb0d7098547104.png","title": "Divas Nails",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Divas Nails<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/divas-nails?location=LL107\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '94'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.663",
				"y": "0.41",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL48A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/78d/a7c/5a278da7c8d8a047330765.png","title": "Donut Centre",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Donut Centre<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/donut-centre?location=LL48A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '96'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4517",
				"y": "0.4417",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB7","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/78d/bf8/5a278dbf85b88408658629.png","title": "Dress Up",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Dress Up<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/dress?location=LLB7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '97'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL161","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/ed9/5a5f25ed93c97930200964.png","title": "Due South",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Due South<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/due-south?location=LL161\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '99'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6146",
				"y": "0.6162",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL201","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77d/e64/5a377de64e9a0712916640.png","title": "Dune",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Dune<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/dune?location=LL201\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '100'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5429",
				"y": "0.595",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL89","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77d/fe2/5a377dfe26f73929428651.png","title": "Dunkin' Donuts",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Dunkin' Donuts<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/dunkin-donuts?location=LL89\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '101'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5375",
				"y": "0.4017",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL261","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f26/065/5a5f2606501af776093347.png","title": "Earthchild",
				"pin": "hidden","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion<h3 class=\"mapplic-map-store-name\">Earthchild<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/earthchild?location=LL261\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '102'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2828",
				"y": "0.5867",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLM2","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f26/1e9/5a5f261e9b151176465846.png","title": "EB Cafe",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">EB Cafe<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/eb-cafe?location=LLM2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '103'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5925",
				"y": "0.435",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL151","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77e/30d/5a377e30d857f327507731.png","title": "Edgars",
				"pin": "hidden","category": "Department Shops","description": "<h4 class=\"mapplic-map-store-category\">Department Shops<h3 class=\"mapplic-map-store-name\">Edgars<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/edgars?location=LL151\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '104'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7099",
				"y": "0.66",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL117","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f26/485/5a5f26485c763031266555.png","title": "Elna",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Elna<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/elna?location=LL117\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '107'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7054",
				"y": "0.4233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB01","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/78f/c42/5a278fc4237d9234121250.png","title": "Eufrasia",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Eufrasia<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/eufrasia?location=LLB01\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '108'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL70","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/3bb/5a377f3bb09e9966286805.png","title": "Exclusive Books",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Exclusive Books<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/exclusive-books?location=LL70\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '112'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5953",
				"y": "0.4717",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB16","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/790/774/5a2790774ff2d471537066.png","title": "Exotic Fragrances",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Exotic Fragrances<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/exotic-fragrances?location=LLB16\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '114'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA13","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/790/8dd/5a27908dd34a3671979153.png","title": "Exotic Perfumes",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Exotic Perfumes<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/exotic-perfumes?location=LLA13\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '115'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL64","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f26/830/5a5f26830db5d676954507.png","title": "Factorie",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Factorie<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/factorie?location=LL64\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '120'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5682",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULM1A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/f75/5a377ff75b2c7518548745.png","title": "Falco Milano",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Falco Milano<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/falco-milano?location=ULM1A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '121'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL139","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/780/78a/5a378078a2b42466968690.png","title": "FNB - Branch",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">FNB - Branch<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fnb-branch?location=LL139\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '124'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.75",
				"y": "0.5865",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL144","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/780/952/5a37809524837146606243.png","title": "Folli Follie",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Folli Follie<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/folli-follie?location=LL144\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '125'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5041",
				"y": "0.505",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL30","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/780/c50/5a3780c502a44984275184.png","title": "Forever 21",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Forever 21<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/forever-21?location=LL30\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '127'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3705",
				"y": "0.4954",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL136","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/780/b3e/5a3780b3e96be313628906.png","title": "Forever New",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Forever New<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/forever-new?location=LL136\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '128'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5312",
				"y": "0.5233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL213","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/780/e42/5a3780e423f77742689243.png","title": "Foschini",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Foschini<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/foschini?location=LL213\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '129'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4373",
				"y": "0.6233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL207B","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f2b/44e/5a5f2b44ec811220374841.png","title": "Foschini Kids",
				"pin": "hidden","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion<h3 class=\"mapplic-map-store-name\">Foschini Kids<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/foschini-kids?location=LL207B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '130'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4635",
				"y": "0.5817",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL130","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3a/6de/5a5f3a6dee4f1427428478.png","title": "Frasers",
				"pin": "hidden","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage<h3 class=\"mapplic-map-store-name\">Frasers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/frasers?location=LL130\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '134'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5763",
				"y": "0.5367",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB19","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/792/c3e/5a2792c3e4f06406310544.png","title": "Frozen Lemons",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Frozen Lemons<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/frozen-lemons?location=LLB19\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '135'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL164","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/781/8a4/5a37818a403a0252470077.png","title": "G-Star Raw",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">G-Star Raw<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/g-star-raw?location=LL164\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '136'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3371",
				"y": "0.5133",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL90","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3a/8c8/5a5f3a8c8ccdc904199269.png","title": "Gadget Time",
				"pin": "hidden","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies<h3 class=\"mapplic-map-store-name\">Gadget Time<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/gadget-time?location=LL90\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '137'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.69",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL273","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3a/9df/5a5f3a9df2223359812086.png","title": "Galaxy & Co.",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Galaxy & Co.<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/galaxy-co?location=LL273\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '138'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2242",
				"y": "0.565",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL126","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/782/bdd/5a3782bddf38b116058646.png","title": "Green Cross",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Green Cross<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/green-cross?location=LL126\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '142'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6097",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB11","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3a/dec/5a5f3adec70c7537160637.png","title": "Grind Away Caff&eacute;",
				"pin": "hidden","category": "Coffee Shops","description": "<h4 class=\"mapplic-map-store-category\">Coffee Shops<h3 class=\"mapplic-map-store-name\">Grind Away Caff&eacute;<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/grind-away-caffe?location=LLB11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '143'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL255","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/782/e06/5a3782e062a71501211025.png","title": "Guess",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Guess<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/guess?location=LL255\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '144'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3172",
				"y": "0.6317",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL205","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/782/f77/5a3782f777396869486168.png","title": "H&M",
				"pin": "hidden","category": "Department Shops","description": "<h4 class=\"mapplic-map-store-category\">Department Shops<h3 class=\"mapplic-map-store-name\">H&M<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/hm?location=LL205\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '145'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4833",
				"y": "0.6233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL147","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6fe/557/5a26fe557291a352477885.png","title": "H20 Water Bar",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">H20 Water Bar<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/h20-water-bar?location=LL147\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '146'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7379",
				"y": "0.5583",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL87","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/783/73c/5a378373c408d006587773.png","title": "Haagen-Dazs",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Haagen-Dazs<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/haagen-dazs?location=LL87\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '147'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.52",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL18","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6fe/9b7/5a26fe9b73954398063228.png","title": "Hair Freedom",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Hair Freedom<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/hair-freedom?location=LL18\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '148'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2928",
				"y": "0.4717",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL219","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/783/981/5a3783981c3d0458227388.png","title": "Harris Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Harris Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/harris-jewellers?location=LL219\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '149'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3957",
				"y": "0.5983",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL144A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/783/b20/5a3783b20dbe6275518606.png","title": "Havaianas",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Havaianas<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/havaianas?location=LL144A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '150'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5095",
				"y": "0.5267",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL142","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6ff/171/5a26ff1712c0c633147960.png","title": "Hepkers",
				"pin": "hidden","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage<h3 class=\"mapplic-map-store-name\">Hepkers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/hepkers?location=LL142\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '151'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5005",
				"y": "0.4805",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL120","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/783/ca1/5a3783ca144d5211289697.png","title": "Hi",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Hi<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/hi?location=LL120\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '152'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6413",
				"y": "0.5317",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL88","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3b/0db/5a5f3b0db0a3b628341479.png","title": "Homemark",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Homemark<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/homemark?location=LL88\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '154'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.681",
				"y": "0.475",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL96","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/296/5a378429647fb365780373.png","title": "Huawei",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Huawei<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/huawei?location=LL96\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '156'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "7225",
				"y": "0.4817",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL134","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3b/30a/5a5f3b30aeafb265494318.png","title": "Identity",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Identity<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/identity?location=LL134\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '159'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5619",
				"y": "0.515",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA15","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/700/333/5a27003333ad9712961049.png","title": "Ilhaam's Flowers",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Ilhaam's Flowers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/ilhaams-flowers?location=LLA15\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '160'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL154","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/9d5/5a37849d55fc5914023955.png","title": "Inglot",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Inglot<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/inglot?location=LL154\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '162'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4337",
				"y": "0.5267",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFCk5","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/700/b24/5a2700b24821f123392299.png","title": "Jakura Sushi Express",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Jakura Sushi Express<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jakura-sushi-express?location=LLFCk5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '165'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.35",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL149","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/f32/5a3784f32d7b0158660559.png","title": "JDC Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">JDC Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jdc-jewellers?location=LL149\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '166'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7307",
				"y": "0.5667",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL159","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/700/e33/5a2700e334024916493456.png","title": "Jet",
				"pin": "hidden","category": "Department Shops","description": "<h4 class=\"mapplic-map-store-category\">Department Shops<h3 class=\"mapplic-map-store-name\">Jet<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jet?location=LL159\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '167'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6368",
				"y": "0.64",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL71A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/785/0d4/5a37850d466db132515334.png","title": "Jimmy's Killer Prawns",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Jimmy's Killer Prawns<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jimmys-killer-prawns?location=LL71A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '168'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5248",
				"y": "0.2217",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "170","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/2c5/5a37862c5d147287225354.png","title": "Joe Soap Car Wash",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Joe Soap Car Wash<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/joe-soap-car-wash?location=170\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '170'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.29",
				"y": "0.5139",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL189","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/574/5a37865743681692925750.png","title": "Joubert & Monty",
				"pin": "hidden","category": "Speciality","description": "<h4 class=\"mapplic-map-store-category\">Speciality<h3 class=\"mapplic-map-store-name\">Joubert & Monty<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/joubert-monty?location=LL189\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '172'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5844",
				"y": "0.6483",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLK37","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/660/5a3786660c516813566839.png","title": "Joubert & Monty Kiosk",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Joubert & Monty Kiosk<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/joubert-monty-kiosk?location=LLK37\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '173'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4535",
				"y": "0.4317",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL95","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/244/5a5f3c244f446751254405.png","title": "JT One",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">JT One<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jt-one?location=LL95\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '174'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5555",
				"y": "0.3933",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB20","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/3c8/5a5f3c3c80178349871977.png","title": "K's Silver",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">K's Silver<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/ks-silver?location=LLB20\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '175'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL62","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/90b/5a378690b2f11109910471.png","title": "Kauai",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Kauai<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/kauai?location=LL62\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '176'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5438",
				"y": "0.4567",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC5","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/a65/5a3786a6527f7520292372.png","title": "KFC",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">KFC<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/kfc?location=LLFC5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '178'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4589",
				"y": "0.285",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL9","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/787/e75/5a3787e756da2749383061.png","title": "King Pie",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">King Pie<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/king-pie?location=LL9\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '179'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.189",
				"y": "0.4833",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL137","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/787/f41/5a3787f41b751385351140.png","title": "King Pie",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">King Pie<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/king-pie-2?location=LL137\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '180'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7866",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL233","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/702/de2/5a2702de23c6d826852947.png","title": "Klinomat Dry Cleaners & Laundry",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Klinomat Dry Cleaners & Laundry<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/klinomat-dry-cleaners-laundry?location=LL233\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '182'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.393",
				"y": "0.7383",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA14","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/545/5a5f3c545c3b0191326405.png","title": "La Belle Jewellery",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">La Belle Jewellery<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/la-belle-jewellery?location=LLA14\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '185'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL10","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/686/5a378868685f6055989338.png","title": "La Rocca",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">La Rocca<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/la-rocca?location=LL10\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '186'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2721",
				"y": "0.4883",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL176","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/d01/5a3788d01e4e9218491336.png","title": "Le Creuset",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Le Creuset<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/le-creuset?location=LL176\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '191'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2703",
				"y": "0.515",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA11","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/69c/5a5f3c69cc7d2173368476.png","title": "Leather Affair",
				"pin": "hidden","category": "Leather & Luggage","description": "<h4 class=\"mapplic-map-store-category\">Leather & Luggage<h3 class=\"mapplic-map-store-name\">Leather Affair<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/leather-affair?location=LLA11\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '192'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL128","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/7ea/5a5f3c7ea226e059827339.png","title": "Legit",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Legit<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/legit?location=LL128\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '193'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5953",
				"y": "0.5133",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL156","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/985/5a5f3c9858b57469828645.png","title": "Limnos Bakery",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Limnos Bakery<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/limnos-bakery?location=LL156\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '196'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.42",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL182","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/789/031/5a37890315837914044897.png","title": "Lindt",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Lindt<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/lindt?location=LL182\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '197'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2441",
				"y": "0.5167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL69","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/de6/5a5f3cde662ff428654188.png","title": "Lounge Relax Play",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Lounge Relax Play<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/lounge-relax-play?location=LL69\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '199'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4463",
				"y": "0.2683",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL265","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/789/aaa/5a3789aaac82e925235547.png","title": "Lovisa",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Lovisa<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/lovisa?location=LL265\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '200'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2757",
				"y": "0.5783",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL263","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/459/5a378a459b6f1322847965.png","title": "Lush Cosmetics",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Lush Cosmetics<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/lush-cosmetics?location=LL263\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '201'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2829",
				"y": "0.5833",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL8","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/5c8/5a378a5c8a236338703257.png","title": "Magents",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Magents<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/magents?location=LL8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '202'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2549",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL50","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3d/503/5a5f3d5036926866847949.png","title": "Magnifico Bistro",
				"pin": "hidden","category": "Coffee Shops","description": "<h4 class=\"mapplic-map-store-category\">Coffee Shops<h3 class=\"mapplic-map-store-name\">Magnifico Bistro<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/magnifico-bistro?location=LL50\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '203'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.46",
				"y": "0.49",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA18","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/706/244/5a2706244cdbe812724657.png","title": "Malaikah Curios",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Malaikah Curios<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/malaikah-curios?location=LLA18\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '204'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL48","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/797/5a378a7971763063604597.png","title": "Marcel's Frozen Yoghurt",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Marcel's Frozen Yoghurt<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/marcels-frozen-yoghurt?location=LL48\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '205'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4571",
				"y": "0.455",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL158","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3d/74e/5a5f3d74e1dc5111714612.png","title": "Marios Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Marios Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/marios-jewellers?location=LL158\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '206'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4102",
				"y": "0.5333",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA20","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/706/983/5a27069832478852915938.png","title": "Market Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Market Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/market-jewellers?location=LLA20\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '207'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA4","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/706/de3/5a2706de3c741389843699.png","title": "Massai Mara Curios",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Massai Mara Curios<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/massai-mara-curios?location=LLA4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '209'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC6","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/a74/5a378aa74962e032902216.png","title": "McCafe",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">McCafe<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mccafe?location=LLFC6\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '211'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5",
				"y": "0.27",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC6","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/b96/5a378ab96a84c170983706.png","title": "McDonalds",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">McDonalds<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mcdonalds?location=LLFC6\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '212'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5131",
				"y": "0.27",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL155","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/d69/5a378ad691b18292016278.png","title": "Miladys",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Miladys<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/miladys?location=LL155\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '215'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6639",
				"y": "0.5917",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLA3","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/707/f01/5a2707f01b414330001705.png","title": "Mombasa",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Mombasa<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mombasa?location=LLA3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '216'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL191","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/edc/5a378aedc0ecc037198978.png","title": "Montagu",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Montagu<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/montagu?location=LL191\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '217'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5826",
				"y": "0.6217",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL14A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/708/347/5a2708347ea48562458324.png","title": "Move With Time",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Move With Time<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>0<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/move-time?location=LL14A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '218'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2865",
				"y": "0.4667",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL101","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/128/5a378b1282c23565463308.png","title": "MTN",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">MTN<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mtn?location=LL101\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '223'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6142",
				"y": "0.38",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL221","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/341/5a378b3412eea902918826.png","title": "Mugg & Bean",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Mugg & Bean<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mugg-bean?location=LL221\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '224'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.33822",
				"y": "0.6317",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL115","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f40/375/5a5f40375c9d9092943830.png","title": "Mullers Optometrists",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Mullers Optometrists<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mullers-optometrists?location=LL115\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '226'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6937",
				"y": "0.425",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL261","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f40/49c/5a5f4049c32c7540528983.png","title": "Naartjie",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Naartjie<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>0<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/naartjie?location=LL261\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '229'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2955",
				"y": "0.5867",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL45","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f40/660/5a5f406603723846904967.png","title": "Nail Fantasy",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Nail Fantasy<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nail-fantasy?location=LL45\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '230'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3795",
				"y": "0.3967",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC10","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/c0d/5a378bc0dfc90376710349.png","title": "Nando's",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Nando's<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nandos?location=LLFC10\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '231'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5086",
				"y": "0.3567",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL227","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/e69/5a378be69a5ee595975285.png","title": "Nedbank",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">Nedbank<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nedbank-branch?location=LL227\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '232'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3985",
				"y": "0.69",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL54","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/418/5a378c4184058574813539.png","title": "Next Kids",
				"pin": "hidden","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion<h3 class=\"mapplic-map-store-name\">Next Kids<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/next-kids?location=LL54\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '235'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5131",
				"y": "0.4717",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL271","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/cd5/5a378ccd50ca2808553838.png","title": "NWJ Jewellery",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">NWJ Jewellery<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>0<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nwj-jewellery?location=LL271\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '241'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2314",
				"y": "0.57",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL92","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a8/149/1a9/5a81491a9e2af886096936.png","title": "Ocean Basket",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Ocean Basket<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/ocean-basket?location=LL92\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '244'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7063",
				"y": "0.4917",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL203","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78d/0e9/5a378d0e903b3326177463.png","title": "Office London",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Office London<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/office-london?location=LL203\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '245'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5239",
				"y": "0.6183",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL60","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78d/2f5/5a378d2f5d4e0548522952.png","title": "Old Khaki",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Old Khaki<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/old-khaki?location=LL60\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '246'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5285",
				"y": "0.4617",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL123","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/18d/5a379018deba5375185562.png","title": "Peacock Tea & Coffee",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Peacock Tea & Coffee<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/peacock-tea-coffee?location=LL123\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '255'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7433",
				"y": "0.4483",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL150","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/2e7/5a37902e7557e901784303.png","title": "Pens Unlimited",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Pens Unlimited<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pens-unlimited?location=LL150\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '256'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.458",
				"y": "0.5267",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL177A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/45c/5a379045c4d8f357063317.png","title": "Pentravel",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Pentravel<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pentravel?location=LL177A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '257'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5944",
				"y": "0.7317",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL129","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/61b/5a379061baa1f696702872.png","title": "Pick n Pay",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Pick n Pay<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pick-n-pay?location=LL129\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '258'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.8336",
				"y": "0.4497",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL1","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/6f3/5a37906f376f0975627957.png","title": "Pick n Pay Clothing",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Pick n Pay Clothing<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pick-n-pay-clothing?location=LL1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '259'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.1755",
				"y": "0.5117",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL131","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/7cd/5a37907cd0265860724263.png","title": "Pick n Pay Liquor",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Pick n Pay Liquor<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pick-n-pay-liquor?location=LL131\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '260'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7894",
				"y": "0.5033",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL277","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/9f3/5a37909f33382694111702.png","title": "Pick n Pay Liquor",
				"pin": "hidden","category": "Liquor","description": "<h4 class=\"mapplic-map-store-category\">Liquor<h3 class=\"mapplic-map-store-name\">Pick n Pay Liquor<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pick-n-pay-liquor-1?location=LL277\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '261'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2044",
				"y": "0.585",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL251","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/bb1/5a3790bb11b4f411673149.png","title": "Poetry",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Poetry<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/poetry?location=LL251\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '263'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3425",
				"y": "0.6083",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL140","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a8/3fe/5ca/5a83fe5ca5e25906417010.png","title": "Prime Time",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Prime Time<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/prime-time?location=LL140\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '266'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5185",
				"y": "0.5167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL19","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/7d6/5a5f427d6af7f188276405.png","title": "Refinery",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Refinery<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/refinery?location=LL19\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '273'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2441",
				"y": "0.42",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL38","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/8f1/5a37918f1f3d6612145267.png","title": "Samsung",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Samsung<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/samsung?location=LL38\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '283'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4264",
				"y": "0.455",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC2","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a6/6e8/8e5/5a66e88e5eb4b242893824.png","title": "Sausage Saloon",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Sausage Saloon<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sausage-saloon?location=LLFC2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '284'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4598",
				"y": "0.3317",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL97","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f43/565/5a5f435651d77005103700.png","title": "Sheet Street",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Sheet Street<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sheet-street?location=LL97\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '285'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5709",
				"y": "0.3883",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL119","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/c97/5a3791c97ac27935455247.png","title": "Shoe City",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Shoe City<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/shoe-city?location=LL119\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '288'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7235",
				"y": "0.4233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL199","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6f5/9e1/5a26f59e15eac277094219.png","title": "Shoe Connection",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Shoe Connection<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/shoe-connection?location=LL199\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '289'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5564",
				"y": "0.595",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL157","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/dee/5a3791deeca9a663328316.png","title": "Shoerama",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Shoerama<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/shoerama?location=LL157\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '290'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6467",
				"y": "0.6",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLR5","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/34c/5a5f4434cf278503416726.png","title": "Simply Asia",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Simply Asia<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/simply-asia?location=LLR5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '291'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.28",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL37B","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/1ea/5a5f441ead86c758303444.png","title": "Simply Natural",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Simply Natural<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/simply-natural?location=LL37B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '292'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3425",
				"y": "0.415",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL253","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/f7a/5a3791f7a287d024445234.png","title": "Sissy Boy",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Sissy Boy<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sissy-boy?location=LL253\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '293'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3362",
				"y": "0.5933",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL84","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/636/5a5f44636dbfb316252225.png","title": "Smart Spaces",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Smart Spaces<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/smart-spaces?location=LL84\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '295'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6648",
				"y": "0.4683",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL239","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/298/5a3792298266a512336608.png","title": "Sorbet Face & Body Professionals",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Sorbet Face & Body Professionals<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sorbet-face-body-professionals-1?location=LL239\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '297'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3569",
				"y": "0.6967",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL4","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/7e1/5a5f447e1c222408728120.png","title": "Soviet",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Soviet<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/soviet?location=LL4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '299'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2441",
				"y": "0.4817",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL114","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/4dd/5a37924dd90e5885164357.png","title": "Spec-Savers",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Spec-Savers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/spec-savers?location=LL114\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '300'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6855",
				"y": "0.5167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL112","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/90c/5a5f4490c0e3e141944481.png","title": "Splush",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Splush<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/splush?location=LL112\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '303'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6964",
				"y": "0.515",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC8","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/f04/5a5f44f042442762320521.png","title": "Steers",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Steers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/steers?location=LLFC8\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '307'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5122",
				"y": "0.3267",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL21","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f45/1f2/5a5f451f22af9247640643.png","title": "Street Fever",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Street Fever<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/street-fever?location=LL21\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '310'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.263",
				"y": "0.4217",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL24","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/18a/5a379318ab244920845944.png","title": "Style Studio",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Style Studio<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/style-studio?location=LL24\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '311'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3398",
				"y": "0.4583",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC7","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f45/3cb/5a5f453cb7683395230462.png","title": "Subway",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Subway<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/subway?location=LLFC7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '312'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5104",
				"y": "0.3167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL257","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f45/cc5/5a5f45cc59457461157961.png","title": "Sunglass Hut",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Sunglass Hut<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sunglass-hut-1?location=LL257\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '313'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3046",
				"y": "0.585",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL93","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f45/dca/5a5f45dca49f7958209677.png","title": "Sunglass Hut",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Sunglass Hut<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sunglass-hut-2?location=LL93\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '314'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5492",
				"y": "0.395",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL168","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/39c/5a379339c85fe637373821.png","title": "Superdry",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Superdry<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/superdry?location=LL168\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '316'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2991",
				"y": "0.5183",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL14A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/0ad/5a5f460addaa8180661136.png","title": "Superfoto",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Superfoto<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/superfoto?location=LL14A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '317'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.27",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL148","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/81d/5a379381df3ad216531515.png","title": "Swarovski",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Swarovski<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/swarovski?location=LL148\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '319'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.468",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB5","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6f0/fa3/5a26f0fa311c3693622645.png","title": "Sweet Centre",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Sweet Centre<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sweet-centre?location=LLB5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '321'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.65",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL61","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/1fe/5a5f461fe42c1160690991.png","title": "Sweets From Heaven",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Sweets From Heaven<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sweets-heaven?location=LL61\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '322'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4418",
				"y": "0.3983",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLB16","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/5d7/5a5f465d7d10f895048323.png","title": "TeAwesome Tea & Smoothie Bar",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">TeAwesome Tea & Smoothie Bar<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/teawesome-tea-smoothie-bar?location=LLB16\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '325'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.64",
				"y": "0.51",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL193","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/728/5a5f46728e52b298250194.png","title": "Tekkie Town",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Tekkie Town<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tekkie-town?location=LL193\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '326'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5655",
				"y": "0.6633",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL133","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/846/5a5f46846c1d3567864434.png","title": "Telkom",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Telkom<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/telkom?location=LL133\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '327'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7758",
				"y": "0.5233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL76","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/a75/5a5f46a7526dd226741229.png","title": "Telkom Direct",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Telkom Direct<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/telkom-direct?location=LL76\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '328'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6178",
				"y": "0.4633",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL78","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/be3/5a3793be37aa4915713250.png","title": "Tempur",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Tempur<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tempur?location=LL78\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '329'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6305",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL174","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/47d/5a5f2447dcc59328324029.png","title": "The Body Shop",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">The Body Shop<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/body-shop?location=LL174\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '330'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2793",
				"y": "0.5167",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL125","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/c46/5a5f46c467273777758071.png","title": "The Cock 'n Bull",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">The Cock 'n Bull<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cock-n-bull?location=LL125\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '331'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7541",
				"y": "0.4533",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL27","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/d69/5a5f46d69cb12515290002.png","title": "The Cross Trainer",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">The Cross Trainer<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cross-trainer?location=LL27\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '332'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.291",
				"y": "0.4117",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL7","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6ee/f15/5a26eef15687b739677443.png","title": "The Tobacconist",
				"pin": "hidden","category": "Tobacconists & E-Cigarettes","description": "<h4 class=\"mapplic-map-store-category\">Tobacconists & E-Cigarettes<h3 class=\"mapplic-map-store-name\">The Tobacconist<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tobacconist?location=LL7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '337'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.1944",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL179","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/3be/5a5f473be34d6187778356.png","title": "Tidy Tuc's Tailors",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Tidy Tuc's Tailors<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tidy-tucs-tailors?location=LL179\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '338'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6034",
				"y": "0.7433",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC4","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6ee/077/5a26ee077e8ec980977394.png","title": "Tong Lok",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Tong Lok<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tong-lok?location=LLFC4\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '342'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4607",
				"y": "0.3067",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL151","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/226/5a37942266317691816742.png","title": "Topman",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Topman<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/topman?location=LL151\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '343'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7",
				"y": "0.63",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL217","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/4d2/5a37944d2d247635988973.png","title": "Totalsports",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Totalsports<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/totalsports?location=LL217\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '345'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3975",
				"y": "0.6267",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL65","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/7b7/5a5f477b75dd8672310600.png","title": "Toys R Us & Babies R Us",
				"pin": "hidden","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies<h3 class=\"mapplic-map-store-name\">Toys R Us & Babies R Us<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/toys-r-us-babies-r-us?location=LL65\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '347'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4273",
				"y": "0.3117",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL237","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/8d5/5a5f478d588dc060307389.png","title": "Travelex",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">Travelex<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/travelex?location=LL237\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '348'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3578",
				"y": "0.7217",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL207A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/77a/5a379477a2675981608621.png","title": "Tread+Miller",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Tread+Miller<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>2<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/treadmiller?location=LL207A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '349'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4535",
				"y": "0.5867",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLK3","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/8ef/5a37948ef3146204792904.png","title": "Trollbeads",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Trollbeads<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/trollbeads?location=LLK3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '350'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4869",
				"y": "0.5233",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL37A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/ca3/5a5f47ca3e054900477783.png","title": "Twisp",
				"pin": "hidden","category": "Tobacconists & E-Cigarettes","description": "<h4 class=\"mapplic-map-store-category\">Tobacconists & E-Cigarettes<h3 class=\"mapplic-map-store-name\">Twisp<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/twisp?location=LL37A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '353'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.33",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL104","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/a24/5a3794a247c54548446667.png","title": "Typo",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Typo<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/typo?location=LL104\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '354'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.718",
				"y": "0.5117",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLM1B","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/d70/5a3794d70c4af507452769.png","title": "VapeShop",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">VapeShop<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/vapeshop?location=LLM1B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '357'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3921",
				"y": "0.435",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL86","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/f12/5a5f47f12444b537622671.png","title": "Verimark",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Verimark<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/verimark?location=LL86\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '358'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6738",
				"y": "0.4717",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL118","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/0a6/5a5f480a6dc40207416157.png","title": "Vodacom Shop Repairs",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Vodacom Shop Repairs<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/vodacom-shop-repairs?location=LL118\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '360'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6729",
				"y": "0.515",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LLFC1-7","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/3ed/5a5f483ed429f885706286.png","title": "Waffle Mania",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Waffle Mania<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/waffle-mania?location=LLFC1-7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '363'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.39",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL82","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/795/020/5a3795020e5dc219966569.png","title": "weFix",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">weFix<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/wefix?location=LL82\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '364'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6386",
				"y": "0.4667",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL2","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/789/714/5a37897141a3a835633891.png","title": "Wimpy",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Wimpy<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/wimpy?location=LL2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '367'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2287",
				"y": "0.495",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL73","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6e9/a94/5a26e9a94eb89946549393.png","title": "Wonderland",
				"pin": "hidden","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment<h3 class=\"mapplic-map-store-name\">Wonderland<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/wonderland?location=LL73\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '370'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5916",
				"y": "0.295",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "LL267","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/812/5a5f488121a54351639870.png","title": "Woolworths",
				"pin": "hidden","category": "Department Shops","description": "<h4 class=\"mapplic-map-store-category\">Department Shops<h3 class=\"mapplic-map-store-name\">Woolworths<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/woolworths?location=LL267\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '371'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2486",
				"y": "0.6633",
				"fill": "#ffffff",
				"zoom": "2"
			}
							]
						},
						{
							"id": "LL",
							"title": "Upper Level",
							"map": "http://18.216.225.141/themes/canal-walk/assets/img/mall/CW_mall_map_V1_upper.svg",
							"locations": [
								{
									"id": "TS",
									"title": "Trend Showcase",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.5582",
									"y": "0.3897"
								},
								{
									"id": "VIC",
									"title": "Visitor Information Centre",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.4773",
									"y": "0.4218"
								},
								{
									"id": "Entrance5",
									"title": "Entrance 5",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.1948",
									"y": "0.5134"
								},
								{
									"id": "Entrance6",
									"title": "Entrance 6",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.3827",
									"y": "0.6496"
								},
								{
									"id": "Entrance7",
									"title": "Entrance 7",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.5686",
									"y": "0.6511"
								},
								{
									"id": "Entrance8",
									"title": "Entrance 8",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.7565",
									"y": "0.5157"
								},
								{
									"id": "VCPC",
									"title": "Vida e Cafe Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.2771",
									"y": "0.4036"
								},
								{
									"id": "MPHPC",
									"title": "MRP Home Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.6796",
									"y": "0.4067"
								},
								{
									"id": "GPC",
									"title": "Game Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.7263",
									"y": "0.4375"
								},
								{
									"id": "CWET",
									"title": "Canal Walk Express Train",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.4779",
									"y": "0.3736"
								},
								{
									"id": "EPC",
									"title": "Edgars Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.6771",
									"y": "0.4652"
								},
								{
									"id": "WPC",
									"title": "Woolworths Promotions Court",
									"pin": "hidden",
									"category": "landmarks",
									"action": "default",
									"x": "0.2746",
									"y": "0.4717"
								},{
				"id": "UL627","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76a/a35/5a376aa35fd83022201099.png","title": "ABSA Bank",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">ABSA Bank<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/absa-bank?location=UL627\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '4'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3876",
				"y": "0.575",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL407A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/144/5a376b144e987121907482.png","title": "Ackermans",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Ackermans<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/ackermans?location=UL407A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '8'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL452","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/412/5a376b412bbf4289228900.png","title": "Adidas",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Adidas<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/adidas?location=UL452\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '9'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5188",
				"y": "0.4277",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL669","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/2c5/5a376b2c58f08485391911.png","title": "Adidas Originals",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Adidas Originals<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/adidas-originals?location=UL669\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '11'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5183",
				"y": "0.3462",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL609","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/85b/5a376b85b4c1d724826188.png","title": "American Swiss",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">American Swiss<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/american-swiss?location=UL609\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '15'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4607",
				"y": "0.514",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULK1","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/9ba/5a376b9ba19c7141713051.png","title": "American Swiss Kiosk",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">American Swiss Kiosk<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/american-swiss-kiosk?location=ULK1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '16'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4657",
				"y": "0.4921",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL509","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/979/5a5f23979f3a2161186947.png","title": "Archive",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Archive<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/archive?location=UL509\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '18'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5913",
				"y": "0.3654",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL497","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76b/b51/5a376bb517ef4392751799.png","title": "Asics",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Asics<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/asics?location=UL497\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '19'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5433",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL629","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76c/041/5a376c0416ef2751057674.png","title": "Bargain Books",
				"pin": "hidden","category": "Books, Cards & Gifts","description": "<h4 class=\"mapplic-map-store-category\">Books, Cards & Gifts<h3 class=\"mapplic-map-store-name\">Bargain Books<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bargain-books?location=UL629\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '21'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3871",
				"y": "0.6269",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL553","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/ab7/5a5f23ab751aa628046992.png","title": "Beds From Home",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Beds From Home<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/beds-home?location=UL553\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '26'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7871",
				"y": "0.4549",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL637","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/76c/e59/5a376ce59b33b289062294.png","title": "Bidvest Bank",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">Bidvest Bank<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bidvest-bank?location=UL637\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '29'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3555",
				"y": "0.6277",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL442","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/774/a47/5a3774a4728b2003465331.png","title": "Billabong",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Billabong<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/billabong?location=UL442\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '31'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4471",
				"y": "0.4192",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL545","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f23/fdb/5a5f23fdb6c77402086798.png","title": "Blockhouse",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Blockhouse<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/blockhouse?location=UL545\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '33'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7567",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL619","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/774/d81/5a3774d813f91686821720.png","title": "Blue Collar White Collar",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Blue Collar White Collar<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/blue-collar-white-collar?location=UL619\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '34'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.377",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL436","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/138/5a5f24138f47a957382998.png","title": "Boardriders",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Boardriders<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/boardriders?location=UL436\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '36'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4238",
				"y": "0.4238",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL603","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/0b3/5a37750b32411420430200.png","title": "Bobbi Brown",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Bobbi Brown<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bobbi-brown?location=UL603\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '37'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5833",
				"y": "0.5469",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL615","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/34d/5a377534d8838450699372.png","title": "Bogart Man",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Bogart Man<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bogart-man?location=UL615\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '39'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4004",
				"y": "0.5292",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "40","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/750/5a5f24750ff69870259735.png","title": "Bride & Co.",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Bride & Co.<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/bride-co?location=40\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '40'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL544","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/8bc/5a5f248bc08a7972664142.png","title": "Browns The Diamond Store",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Browns The Diamond Store<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/browns-diamond-store?location=UL544\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '41'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.43",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL575","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f24/b54/5a5f24b548393426163353.png","title": "Build A Bear",
				"pin": "hidden","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies<h3 class=\"mapplic-map-store-name\">Build A Bear<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/build-bear?location=UL575\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '44'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.65",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL481","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/6b1/5a37756b1a81b209892588.png","title": "Burger King",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Burger King<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/burger-king?location=UL481\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '45'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.44",
				"y": "0.28",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL657","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/847/5a37758471e0f947002790.png","title": "Cafe Coton",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Cafe Coton<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cafe-coton?location=UL657\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '46'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2664",
				"y": "0.5139",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL485","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/b78/5a3775b789b3d437632864.png","title": "Cape Town Fish Market",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Cape Town Fish Market<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cape-town-fish-market?location=UL485\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '48'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5191",
				"y": "0.2515",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "51","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/775/c92/5a3775c922dc4201948800.png","title": "Cape Union Mart Adventure Centre",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Cape Union Mart Adventure Centre<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cape-union-mart-adventure-centre?location=51\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '51'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4107",
				"y": "0.374",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL655","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/251/5a3776251601a127803339.png","title": "Carducci",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Carducci<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/carducci?location=UL655\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '54'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.279",
				"y": "0.5158",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL416A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/3b1/5a37763b16071508369074.png","title": "Carlton Hair",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Carlton Hair<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/carlton-hair?location=UL416A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '55'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2932",
				"y": "0.41",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULC7","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/776/665/5a37766653d83131570765.png","title": "Cell C",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Cell C<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cell-c-1?location=ULC7\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '58'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7392",
				"y": "0.4985",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL633","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/787/74a/5a278774a788c387054806.png","title": "Cellular Village",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Cellular Village<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cellular-village-2?location=UL633\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '64'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.34",
				"y": "0.655",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL575","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/777/048/5a37770485300448148505.png","title": "Cherry Melon",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Cherry Melon<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cherry-melon?location=UL575\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '69'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6495",
				"y": "0.524",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL641","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a8/2d6/9b3/5a82d69b361ab478051052.png","title": "Computer Mania",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Computer Mania<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/computer-mania?location=UL641\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '79'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3569",
				"y": "0.6",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL462A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/779/4a4/5a37794a4cfdf577923996.png","title": "Crocs",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Crocs<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/crocs?location=UL462A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '83'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5651",
				"y": "0.4246",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL519","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/55b/5a5f2555b7ef4373946235.png","title": "Crossover",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Crossover<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/crossover?location=UL519\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '84'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6351",
				"y": "0.381",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL635","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/779/849/5a3779849cfe9181986999.png","title": "Cruise About",
				"pin": "hidden","category": "Travel","description": "<h4 class=\"mapplic-map-store-category\">Travel<h3 class=\"mapplic-map-store-name\">Cruise About<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/cruise-about?location=UL635\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '85'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3561",
				"y": "0.6466",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL554","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/779/9ac/5a37799ac207e506741476.png","title": "CSquared",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">CSquared<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/csquared?location=UL554\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '86'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3479",
				"y": "0.4851",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL478","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77c/081/5a377c0815e76176908652.png","title": "Deluxe Laser & Spa",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Deluxe Laser & Spa<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/deluxe-laser-spa?location=UL478\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '90'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5761",
				"y": "0.42",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL462","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77d/4d8/5a377d4d8c562872811243.png","title": "Diesel",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Diesel<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/diesel?location=UL462\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '91'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5662",
				"y": "0.42",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL213","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77d/c52/5a377dc52a0b6990779265.png","title": "Donna",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Donna<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/donna?location=UL213\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '95'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4315",
				"y": "0.5231",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULR3","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f25/b9b/5a5f25b9b6bf1591271692.png","title": "Dros",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Dros<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/dros?location=ULR3\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '98'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4058",
				"y": "0.32",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL412","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77e/9c2/5a377e9c2987f007498162.png","title": "Edge for Men",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Edge for Men<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/edge-men?location=UL412\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '105'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.2937",
				"y": "0.4769",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL597","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f26/346/5a5f26346b7c1731654484.png","title": "Elegance Fabrics",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Elegance Fabrics<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/elegance-fabrics?location=UL597\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '106'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5888",
				"y": "0.67",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL649","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/060/5a377f06058f6027787421.png","title": "Europa Art",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Europa Art<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/europa-art?location=UL649\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '109'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3316",
				"y": "0.5833",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "110","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f26/5b7/5a5f265b7fbd0748233763.png","title": "Eurosuit",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Eurosuit<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/eurosuit?location=110\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '110'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0",
				"y": "0",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL407B","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/221/5a377f221dcf9741731944.png","title": "Exact",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Exact<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/exact?location=UL407B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '111'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.253",
				"y": "0.455",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL510A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/525/5a377f525a728410553892.png","title": "Execuspecs",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Execuspecs<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/execuspecs?location=UL510A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '113'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6375",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL625","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/6b5/5a377f6b508f3525345828.png","title": "Experimax",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Experimax<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/experimax?location=UL625\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '116'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.39",
				"y": "0.67",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL607","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/9d0/5a377f9d00d80095055119.png","title": "Eye Q",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Eye Q<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/eye-q?location=UL607\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '117'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.57",
				"y": "0.58",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL651A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/b48/5a377fb48134d341383511.png","title": "Fabiani",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Fabiani<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fabiani?location=UL651A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '118'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.36",
				"y": "0.57",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL550A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/77f/cf8/5a377fcf8bdde541945990.png","title": "Fabiani Women",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Fabiani Women<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fabiani-women?location=UL550A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '119'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.39",
				"y": "0.55",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "122","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f2b/0d0/5a5f2b0d06e8f815364077.png","title": "Fielli",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Fielli<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fielli?location=122\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '122'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6246",
				"y": "0.5267",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL591","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/780/281/5a37802819d4e455090518.png","title": "Flight Centre",
				"pin": "hidden","category": "Travel","description": "<h4 class=\"mapplic-map-store-category\">Travel<h3 class=\"mapplic-map-store-name\">Flight Centre<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/flight-centre?location=UL591\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '123'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6",
				"y": "0.63",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL532","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/781/01b/5a378101b6f07659868285.png","title": "Fossil",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Fossil<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fossil?location=UL532\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '131'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.5",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL424","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f2b/5b0/5a5f2b5b0ce06945499564.png","title": "Fox",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Fox<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fox?location=UL424\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '132'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.32",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULK38","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f2b/713/5a5f2b7132263821787623.png","title": "Fragrance Boutique",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Fragrance Boutique<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fragrance-boutique?location=ULK38\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '133'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.87",
				"y": "0.57",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL549","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/782/4b3/5a37824b379a9016218768.png","title": "Game",
				"pin": "hidden","category": "Department Shops","description": "<h4 class=\"mapplic-map-store-category\">Department Shops<h3 class=\"mapplic-map-store-name\">Game<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/game?location=UL549\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '139'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.81",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL541","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3a/f48/5a5f3af483711379929238.png","title": "Home etc.",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Home etc.<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/home-etc?location=UL541\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '153'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.72",
				"y": "0.43",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL562","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/3eb/5a37843eb8e7c158869719.png","title": "Hugo Boss Menswear",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Hugo Boss Menswear<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/hugo-boss-menswear?location=UL562\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '157'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL469A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/552/5a3784552e1ed799288649.png","title": "Hurley",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Hurley<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/hurley?location=UL469A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '158'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.42",
				"y": "0.37",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL526","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/bbd/5a3784bbda640175095281.png","title": "iStore",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">iStore<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/istore?location=UL526\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '163'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.53",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL647","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/784/d76/5a3784d7655df585190187.png","title": "Jack Friedman Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Jack Friedman Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jack-friedman-jewellers?location=UL647\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '164'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.33",
				"y": "0.55",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL653","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/175/5a37861753860918908320.png","title": "Jo Malone",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Jo Malone<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/jo-malone?location=UL653\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '169'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL483","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/408/5a3786408b663996268340.png","title": "John Dory's Fish & Grill Restaurant",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">John Dory's Fish & Grill Restaurant<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/john-dorys-fish-grill-restaurant?location=UL483\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '171'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.47",
				"y": "0.37",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL560","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/262/5a378c262b2eb480671257.png","title": "Keedo",
				"pin": "hidden","category": "Kids Fashion","description": "<h4 class=\"mapplic-map-store-category\">Kids Fashion<h3 class=\"mapplic-map-store-name\">Keedo<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/keedo?location=UL560\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '177'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL548","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/786/fdb/5a3786fdbe6eb004567390.png","title": "Kingsley Heath",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Kingsley Heath<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/kingsley-heath?location=UL548\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '181'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.37",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL550B","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/0cc/5a37880ccb4c7326921994.png","title": "Kurt Geiger",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Kurt Geiger<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/kurt-geiger?location=UL550B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '183'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.35",
				"y": "0.55",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL655A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/789/21e/5a378921eda5c677880212.png","title": "L'Occitane en Provence",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">L'Occitane en Provence<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/loccitane-en-provence?location=UL655A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '184'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL605","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/81c/5a378881cb67d464617194.png","title": "La Senza",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">La Senza<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/la-senza?location=UL605\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '187'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.54",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL564","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/9c2/5a37889c23201718388705.png","title": "Lacoste",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Lacoste<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/lacoste?location=UL564\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '188'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.31",
				"y": "0.57",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL495","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/b65/5a3788b6565bc393361084.png","title": "Le Coq Sportif",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Le Coq Sportif<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/le-coq-sportif?location=UL495\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '190'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.53",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL501","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/788/e98/5a3788e98cd87381018098.png","title": "Levi's Store",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Levi's Store<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/levis-store?location=UL501\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '194'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.55",
				"y": "0.43",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULK13","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/ae1/5a5f3cae17b00882390092.png","title": "Limnos Bakers Patisserie Boutique",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Limnos Bakers Patisserie Boutique<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/limnos-kiosk?location=ULK13\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '195'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.56",
				"y": "0.39",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL482","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3c/c9c/5a5f3cc9c523b539418050.png","title": "Loads of Living",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Loads of Living<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/loads-living?location=UL482\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '198'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.62",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL468","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78a/905/5a378a905116c414392851.png","title": "Markham",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Markham<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/markham?location=UL468\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '208'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.56",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL518","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3d/8e1/5a5f3d8e15a6f366601582.png","title": "Mat & May",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Mat & May<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mat-may?location=UL518\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '210'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.61",
				"y": "0.49",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL467","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3d/a86/5a5f3da86bf03175696558.png","title": "Melissa",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Melissa<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/melissa?location=UL467\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '213'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4",
				"y": "0.42",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULR1","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/d6c/5a3792d6caa22505646463.png","title": "Mexico Spur",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Mexico Spur<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mexico-spur?location=ULR1\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '214'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.35",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL401","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/789/91d/5a378991d59a6623780111.png","title": "Mr Price",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Mr Price<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mr-price?location=UL401\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '219'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.18",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL535","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3f/c49/5a5f3fc4941ac206332907.png","title": "Mr Price Home Zone",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Mr Price Home Zone<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mr-price-home-zone?location=UL535\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '220'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.72",
				"y": "0.44",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL529","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f3f/87f/5a5f3f87f2760844952079.png","title": "Mr Price Sport",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Mr Price Sport<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mr-price-sport?location=UL529\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '221'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7",
				"y": "0.44",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL663","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/026/5a378b02659ae985667415.png","title": "MTN",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">MTN<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>4<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mtn?location=UL663\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '222'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.25",
				"y": "0.56",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULK5","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f40/0bc/5a5f400bcfa34982663567.png","title": "Mugg & Bean On The Move",
				"pin": "hidden","category": "Fast Food & Confectionary","description": "<h4 class=\"mapplic-map-store-category\">Fast Food & Confectionary<h3 class=\"mapplic-map-store-name\">Mugg & Bean On The Move<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/mugg-bean-move?location=ULK5\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '225'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.75",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL651","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/674/5a378b674167d989706165.png","title": "Musica Mega Store",
				"pin": "hidden","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment<h3 class=\"mapplic-map-store-name\">Musica Mega Store<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/musica-mega-store?location=UL651\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '227'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.37",
				"y": "0.58",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL558","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78b/fab/5a378bfab7291748308772.png","title": "Nespresso",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Nespresso<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nespresso?location=UL558\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '233'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL668","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/0fc/5a378c0fcf194810947580.png","title": "New Balance",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">New Balance<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/new-balance?location=UL668\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '234'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.44",
				"y": "0.39",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL512","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/519/5a378c519e3ea332889294.png","title": "Next Step",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Next Step<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/next-step?location=UL512\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '236'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.64",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL422","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/6a9/5a378c6a9bea8634680360.png","title": "Nike",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Nike<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nike?location=UL422\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '237'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.33",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL420","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/81e/5a378c81ee491525758106.png","title": "Nixon",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Nixon<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nixon?location=UL420\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '238'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.32",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL404A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/999/5a378c99947c5533993348.png","title": "Nu Health Food Caf&eacute;",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Nu Health Food Caf&eacute;<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nu-health-food-cafe?location=UL404A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '239'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.23",
				"y": "0.49",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL479","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78c/ac7/5a378cac78367464821559.png","title": "Nu Metro",
				"pin": "hidden","category": "Movies & Entertainment","description": "<h4 class=\"mapplic-map-store-category\">Movies & Entertainment<h3 class=\"mapplic-map-store-name\">Nu Metro<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nu-metro?location=UL479\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '240'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.45",
				"y": "0.3",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL579","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f40/9a8/5a5f409a8d26e051107005.png","title": "NYX Professional Makeup",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">NYX Professional Makeup<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/nyx-professional-makeup?location=UL579\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '242'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.52",
				"y": "0.61",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL659","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/70b/d52/5a270bd529c98599636931.png","title": "Occassions",
				"pin": "hidden","category": "Womens Fashion","description": "<h4 class=\"mapplic-map-store-category\">Womens Fashion<h3 class=\"mapplic-map-store-name\">Occassions<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/occassions?location=UL659\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '243'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.24",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL643","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f40/de9/5a5f40de9fd5b649901952.png","title": "Pak Persian Carpets",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Pak Persian Carpets<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pak-persian-carpets?location=UL643\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '247'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.34",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL513","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78d/480/5a378d480d1f3100833567.png","title": "Palladium Boots",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Palladium Boots<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/palladium-boots?location=UL513\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '248'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL487","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78f/c6c/5a378fc6c3328461138176.png","title": "Panarottis",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Panarottis<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/panarottis?location=UL487\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '249'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.51",
				"y": "0.3",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL572","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f41/04b/5a5f4104bc42d138291394.png","title": "Pandora",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Pandora<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pandora?location=UL572\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '250'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.23",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL449","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f41/1d5/5a5f411d5b4a8334018740.png","title": "Partners Hair Design",
				"pin": "hidden","description": "<h3 class=\"mapplic-map-store-name\">Partners Hair Design<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/partners-hair-design?location=UL449\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '251'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.27",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL527","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f41/2fe/5a5f412fed68b073403119.png","title": "Partners Hair Design for Men",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Partners Hair Design for Men<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/partners-hair-design-men?location=UL527\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '252'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.63",
				"y": "0.39",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "253","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/78f/e27/5a378fe272615242883925.png","title": "Patio Warehouse",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Patio Warehouse<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/patio-warehouse?location=253\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '253'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.336",
				"y": "0.599",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL597","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/0ab/5a5f420ab5d10027446513.png","title": "Paul van Zyl Couture & Designer Fabric Collection",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Paul van Zyl Couture & Designer Fabric Collection<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/paul-van-zyl-couture-designer-fabric-collection?location=UL597\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '254'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.58",
				"y": "0.62",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL525","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f41/472/5a5f414724e60087523468.png","title": "Placecol Skin Care Clinic",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Placecol Skin Care Clinic<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/placecol-skin-care-clinic?location=UL525\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '262'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.64",
				"y": "0.39",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL520","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f41/c09/5a5f41c097449417788632.png","title": "Polo",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Polo<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/polo?location=UL520\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '264'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.56",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL589","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/d9a/5a3790d9a2da5778071109.png","title": "Postnet",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Postnet<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/postnet?location=UL589\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '265'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.59",
				"y": "0.59",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL490","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f41/e62/5a5f41e629773653677676.png","title": "Primi",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Primi<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/primi?location=UL490\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '267'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.69",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL556","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/790/f28/5a3790f286565521535855.png","title": "Pringle of Scotland",
				"pin": "hidden","description": "<h3 class=\"mapplic-map-store-name\">Pringle of Scotland<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/pringle-scotland?location=UL556\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '268'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.38",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL505","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/0fe/5a37910fe9c0d440415989.png","title": "Puma",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Puma<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/puma?location=UL505\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '269'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.52",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL466","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/267/5a37912677617508023126.png","title": "Queenspark",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Queenspark<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/queenspark?location=UL466\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '270'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.53",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL560A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/3b9/5a5f423b941b0812100103.png","title": "Red Square",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Red Square<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/red-square?location=UL560A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '271'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.31",
				"y": "0.49",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL491","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/66d/5a5f4266d2efd275541030.png","title": "Reebok",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Reebok<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/reebok?location=UL491\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '272'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.53",
				"y": "0.41",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL426","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/4a9/5a37914a9b44e174560762.png","title": "Rip Curl",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Rip Curl<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/rip-curl?location=UL426\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '274'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.33",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL470","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/5f6/5a37915f60e14549693353.png","title": "Rockport",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Rockport<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/rockport?location=UL470\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '275'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.52",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL540","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/9ca/5a5f429cace94874588181.png","title": "Romens",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Romens<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/romens?location=UL540\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '276'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.41",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL555","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/bd4/5a5f42bd4915a826976172.png","title": "Rugs Original",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Rugs Original<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/rugs-original?location=UL555\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '277'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.78",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL447","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f42/d1e/5a5f42d1eb5ef913207451.png","title": "RVCA The Store",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">RVCA The Store<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/rvca-store?location=UL447\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '278'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.29",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL581","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6f6/a60/5a26f6a602c36163370811.png","title": "SA Art Gallery & Framing",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">SA Art Gallery & Framing<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sa-art-gallery-framing?location=UL581\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '279'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.59",
				"y": "0.62",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL561","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6f6/8bb/5a26f68bb7c12151454606.png","title": "SA Framing",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">SA Framing<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sa-framing?location=UL561\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '280'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.75",
				"y": "0.56",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL469A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/7ae/5a37917ae4650580790616.png","title": "Salomon",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Salomon<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/salomon?location=UL469A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '281'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL623","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6f6/1c0/5a26f61c0a79b976747618.png","title": "Salon Annique",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Salon Annique<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/salon-annique?location=UL623\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '282'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.37",
				"y": "0.56",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL459","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f43/fa3/5a5f43fa36110842481953.png","title": "Shesha",
				"pin": "hidden","category": "Men's Fashion","description": "<h4 class=\"mapplic-map-store-category\">Men's Fashion<h3 class=\"mapplic-map-store-name\">Shesha<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/shesha?location=UL459\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '286'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.39",
				"y": "0.4",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL538","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/791/aa6/5a3791aa6abac096161438.png","title": "Shimansky",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Shimansky<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/shimansky?location=UL538\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '287'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.48",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL517","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/50b/5a5f4450bdd5e190547581.png","title": "Skechers",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Skechers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/skechers?location=UL517\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '294'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.56",
				"y": "0.39",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL482A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/130/5a37921308c63909106370.png","title": "SODA Bloc",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">SODA Bloc<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/soda-bloc?location=UL482A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '296'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6",
				"y": "0.44",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL583","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/390/5a37923905acf111647503.png","title": "Sorbet Face & Body Professionals",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Sorbet Face & Body Professionals<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sorbet-face-body-professionals-2?location=UL583\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '298'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6",
				"y": "0.6",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL456A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/6be/5a37926be5664878137677.png","title": "Speedo",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Speedo<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/speedo?location=UL456A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '301'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.53",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL659A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/86c/5a379286c26b1510562029.png","title": "Spitz",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Spitz<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/spitz?location=UL659A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '302'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.26",
				"y": "0.51",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL407A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/ab2/5a5f44ab247d7442776472.png","title": "Sportscene",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Sportscene<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sportscene?location=UL407A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '304'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.21",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL451","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/9b2/5a37929b23bc9869985320.png","title": "Sportsmans Warehouse",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Sportsmans Warehouse<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sportsmans-warehouse?location=UL451\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '305'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.3",
				"y": "0.42",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL599","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f44/c5a/5a5f44c5a6d3b056658026.png","title": "Standard Bank - Branch",
				"pin": "hidden","category": "Banks & Forex","description": "<h4 class=\"mapplic-map-store-category\">Banks & Forex<h3 class=\"mapplic-map-store-name\">Standard Bank - Branch<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/standard-bank-branch?location=UL599\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '306'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.59",
				"y": "0.56",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL528","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f45/076/5a5f4507676c3848389112.png","title": "Sterns",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Sterns<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sterns?location=UL528\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '308'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.51",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL444","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/792/eec/5a3792eec0b9c338343083.png","title": "Steve Madden",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Steve Madden<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/steve-madden?location=UL444\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '309'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.37",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULK2","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f45/f12/5a5f45f1298e4616363887.png","title": "Sunglass Hut",
				"pin": "hidden","category": "Eyewear & Optometrists","description": "<h4 class=\"mapplic-map-store-category\">Eyewear & Optometrists<h3 class=\"mapplic-map-store-name\">Sunglass Hut<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/sunglass-hut-3?location=ULK2\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '315'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL477","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/66c/5a379366c81f5457083264.png","title": "Superga",
				"pin": "hidden","category": "Footwear","description": "<h4 class=\"mapplic-map-store-category\">Footwear<h3 class=\"mapplic-map-store-name\">Superga<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/superga?location=UL477\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '318'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.46",
				"y": "0.44",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL524","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/a70/5a3793a70cde6863663789.png","title": "Swatch",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Swatch<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/swatch?location=UL524\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '320'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.51",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL593","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/321/5a5f463217258146397520.png","title": "Tailors Shop",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Tailors Shop<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tailors-shop?location=UL593\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '323'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.58",
				"y": "0.7",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL408","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/4d6/5a5f464d68938462274295.png","title": "Tashas",
				"pin": "hidden","category": "Restaurants","description": "<h4 class=\"mapplic-map-store-category\">Restaurants<h3 class=\"mapplic-map-store-name\">Tashas<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tashas?location=UL408\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '324'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.25",
				"y": "0.52",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL577","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/20c/5a5f4720c5fdf136949443.png","title": "The Fix",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">The Fix<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/fix?location=UL577\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '333'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.63",
				"y": "0.53",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL439","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/da5/5a3793da5a8b4016635922.png","title": "The North Face",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">The North Face<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/north-face?location=UL439\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '334'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.25",
				"y": "0.55",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL617","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/029/5a5f4702995f1104424727.png","title": "The Scoin Shop",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">The Scoin Shop<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/scoin-shop?location=UL617\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '335'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.38",
				"y": "0.56",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL482A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f46/eae/5a5f46eae6cf8530633595.png","title": "The Space",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">The Space<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/space?location=UL482A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '336'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.69",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL631","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/4b3/5a5f474b3f528552675583.png","title": "Tidy Tuc's Tailors",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Tidy Tuc's Tailors<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>3<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tidy-tucs-2?location=UL631\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '339'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.39",
				"y": "0.67",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL515","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/646/5a5f476462306816435910.png","title": "Timberland",
				"pin": "hidden","category": "Unisex Fashion","description": "<h4 class=\"mapplic-map-store-category\">Unisex Fashion<h3 class=\"mapplic-map-store-name\">Timberland<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/timberland?location=UL515\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '340'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.57",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL546","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/793/f81/5a3793f8136cb844152330.png","title": "Tissot Boutique",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Tissot Boutique<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/tissot-boutique?location=UL546\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '341'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.38",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL151","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/350/5a3794350313e410501330.png","title": "Topshop",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Topshop<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/topshop?location=UL151\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '344'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.7",
				"y": "0.67",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL407","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/633/5a379463378cf426278855.png","title": "Toy Kingdom",
				"pin": "hidden","category": "Toys & Hobbies","description": "<h4 class=\"mapplic-map-store-category\">Toys & Hobbies<h3 class=\"mapplic-map-store-name\">Toy Kingdom<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/toy-kingdom?location=UL407\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '346'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.22",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL203","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/a12/5a5f47a126b8f502083369.png","title": "Truworths",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Truworths<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/truworths?location=UL203\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '351'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.53",
				"y": "0.62",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL402","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/b55/5a5f47b5513b5066052564.png","title": "Twill",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Twill<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/twill?location=UL402\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '352'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.23",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL465","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f47/dcc/5a5f47dcc3e8a524579623.png","title": "Uzzi",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Uzzi<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/uzzi?location=UL465\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '355'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.39",
				"y": "0.46",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL456B","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/b94/5a3794b947ff7125010055.png","title": "Vans Concept Store",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Vans Concept Store<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/vans-concept-store?location=UL456B\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '356'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.55",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL404","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/794/ee1/5a3794ee15c56351327626.png","title": "Vida e Caffe",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Vida e Caffe<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/vida-e-caffe?location=UL404\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '359'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.24",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL416","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/1d9/5a5f481d9d461090456588.png","title": "Vodashop",
				"pin": "hidden","category": "Technology","description": "<h4 class=\"mapplic-map-store-category\">Technology<h3 class=\"mapplic-map-store-name\">Vodashop<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>5<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/vodashop?location=UL416\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '361'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.29",
				"y": "0.55",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL563","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/2ed/5a5f482ed1f61027145869.png","title": "Volpes",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Volpes<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/volpes?location=UL563\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '362'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.73",
				"y": "0.57",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL478A","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/543/5a5f48543ed60178653184.png","title": "Wildfire Bodypiercing",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Wildfire Bodypiercing<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/wildfire-bodypiercing?location=UL478A\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '365'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.65",
				"y": "0.5",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL480","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a2/6eb/540/5a26eb5401b05020446096.png","title": "Wildfire Tattoos",
				"pin": "hidden","category": "Speciality Shops & Services","description": "<h4 class=\"mapplic-map-store-category\">Speciality Shops & Services<h3 class=\"mapplic-map-store-name\">Wildfire Tattoos<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/wildfire-tattoos?location=UL480\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '366'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.69",
				"y": "0.55",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL585","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/6bd/5a5f486bdd5cb192387125.png","title": "Winston Sahd",
				"pin": "hidden","category": "Home & D&eacute;cor","description": "<h4 class=\"mapplic-map-store-category\">Home & D&eacute;cor<h3 class=\"mapplic-map-store-name\">Winston Sahd<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>7<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/winston-sahd?location=UL585\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '368'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.6",
				"y": "0.67",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL536","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/795/0f2/5a37950f21941327690671.png","title": "Wolf Bros. Jewellers",
				"pin": "hidden","category": "Jewellery & Accessories","description": "<h4 class=\"mapplic-map-store-category\">Jewellery & Accessories<h3 class=\"mapplic-map-store-name\">Wolf Bros. Jewellers<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/wolf-bros-jewellers?location=UL536\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '369'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.48",
				"y": "0.54",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL417","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/900/5a5f4890031b8280452476.png","title": "Xpresso Caf&eacute;",
				"pin": "hidden","category": "Food & Beverage","description": "<h4 class=\"mapplic-map-store-category\">Food & Beverage<h3 class=\"mapplic-map-store-name\">Xpresso Caf&eacute;<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>1<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/xpresso-cafe?location=UL417\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '372'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.25",
				"y": "0.47",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL457","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/a21/5a5f48a2182bf363499296.png","title": "Xtreme Nutrition",
				"pin": "hidden","category": "Sport, Fitness & Lifestyle","description": "<h4 class=\"mapplic-map-store-category\">Sport, Fitness & Lifestyle<h3 class=\"mapplic-map-store-name\">Xtreme Nutrition<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>6<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/xtreme-nutrition?location=UL457\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '373'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.38",
				"y": "0.45",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "ULK19","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a5/f48/b2c/5a5f48b2c487a877938292.png","title": "Yemaya Express Nail Bar",
				"pin": "hidden","category": "Health, Hair & Beauty","description": "<h4 class=\"mapplic-map-store-category\">Health, Hair & Beauty<h3 class=\"mapplic-map-store-name\">Yemaya Express Nail Bar<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>8<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/yemaya-express-nail-bar?location=ULK19\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '374'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.66",
				"y": "0.51",
				"fill": "#ffffff",
				"zoom": "2"
			},{
				"id": "UL432","thumbnail": "http://18.216.225.141/storage/app/uploads/public/5a3/795/252/5a379525241a4558476241.png","title": "Young Designers Emporium (YDE)",
				"pin": "hidden","category": "Fashion & Footwear","description": "<h4 class=\"mapplic-map-store-category\">Fashion & Footwear<h3 class=\"mapplic-map-store-name\">Young Designers Emporium (YDE)<div class=\"mapplic-map-store-level-entrance\"><div class=\"row\">	<div class=\"col-6\">Level <strong>Lower Level	<div class=\"col-6\">Closest Entrance <strong>10<div class=\"row mapplic-map-store-buttons\">	<div class=\"col-6\"><a href=\"http://18.216.225.141/store/young-designers-emporium-yde?location=UL432\" class=\"mapplic-map-store-buttons-visit-store\">Visit Store <i class=\"fa fa-angle-right\">	<div class=\"col-6 text-right\"><a href=\"\" data-request=\"onFavorite\" data-request-data=\"favId: '375'\" data-request-success=\"$(this).html(data.result)\"><i class=\"fa fa-heart-o\" aria-hidden=\"true\">",
				"x": "0.4",
				"y": "0.51",
				"fill": "#ffffff",
				"zoom": "2"
			}
							]
						}
					],
					"maxscale": 2
				},
				sidebar : true, // Enable sidebar
				minimap : false, // Enable minimap
				markers : false, // Disable markers
				fullscreen : true, // Enable fullscreen
				maxscale : 3
				// Setting maxscale to 3 times bigger than the original file
			});
		})( jQuery );
