<?php namespace CreativeSpark\Storecontact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {

      return [
        'CreativeSpark\Storecontact\Components\Storecontact' => 'storecontact'
      ];

    }

    public function registerSettings()
    {
    }
}
