<?php namespace CreativeSpark\Storecontact\Components;

use Cms\Classes\ComponentBase;
use CreativeSpark\Stores\Models\Store;
use Input;
use Mail;

class Storecontact extends ComponentBase {

	public $store;

	public function componentDetails() {

	    return [
		    'name'=>'Store Contact',
		    'description'=>'Store Contact Form',
	    ];

	}

	public function onSend(){

		$storeDetail = Store::where('slug', $this->param('slug'))->first();

		if(!$storeDetail){
			Flash::error('Error sending mail!');
			end();
		}

		$vars = ['name' => Input::get('name'), 'email' => Input::get('email'), 'message' => Input::get('message')];

		$mailSent = Mail::send('creativespark.contact::mail.message', $vars, function($message) {

			// if(env('APP_DEBUG') == 'dev') {
			// 	$message->to('nicos@creativespark.co.za', 'Admin Person')->bcc('bryce.ferguson@creativespark.co.za', 'Bryce Ferguson');
			// } else {
			// }
			$message->to($storeDetail->email_address)->bcc('bryce.ferguson@creativespark.co.za', 'Bryce Ferguson');
			$message->subject('Enquiry from Canalwalk.co.za');

		});

		if($mailSent) {
			return Redirect::back()->with('message', 'Mail sent thanks!');
		} else {
			return Redirect::back()->with('message', 'Mail not sent!');
		}

	}


}
