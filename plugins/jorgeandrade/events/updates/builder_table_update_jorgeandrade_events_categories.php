<?php namespace JorgeAndrade\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJorgeandradeEventsCategories extends Migration
{
    public function up()
    {
        Schema::table('jorgeandrade_events_categories', function($table)
        {
            $table->integer('hyprop_malls_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('jorgeandrade_events_categories', function($table)
        {
            $table->dropColumn('hyprop_malls_id');
        });
    }
}
