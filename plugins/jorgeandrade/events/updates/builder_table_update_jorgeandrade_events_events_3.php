<?php namespace JorgeAndrade\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJorgeandradeEventsEvents3 extends Migration
{
    public function up()
    {
        Schema::table('jorgeandrade_events_events', function($table)
        {
            $table->string('price')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jorgeandrade_events_events', function($table)
        {
            $table->dropColumn('price');
        });
    }
}
