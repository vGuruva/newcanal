<?php namespace JorgeAndrade\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJorgeandradeEventsCategoriesEvents extends Migration
{
    public function up()
    {
        Schema::create('jorgeandrade_events_categories_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('category_id')->unsigned();
            $table->integer('event_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jorgeandrade_events_categories_events');
    }
}
