<?php namespace JorgeAndrade\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJorgeandradeEventsEvents5 extends Migration
{
    public function up()
    {
        Schema::table('jorgeandrade_events_events', function($table)
        {
            $table->integer('store_id')->nullable()->unsigned();
            $table->date('published')->nullable();
            $table->string('featured_image')->nullable();
            $table->string('external_link')->nullable();
            $table->string('external_cta')->nullable();

        });
    }
    
    public function down()
    {
        Schema::table('jorgeandrade_events_events', function($table)
        {
            $table->dropColumn('store_id');
            $table->dropColumn('published');
            $table->dropColumn('featured_image');
            $table->dropColumn('external_link');
            $table->dropColumn('external_cta');
        });
    }
}
