<?php namespace JorgeAndrade\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJorgeandradeEventsCategories extends Migration
{
    public function up()
    {
        Schema::create('jorgeandrade_events_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jorgeandrade_events_categories');
    }
}
