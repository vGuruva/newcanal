<?php namespace JorgeAndrade\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJorgeandradeEventsEvents2 extends Migration
{
    public function up()
    {
        Schema::table('jorgeandrade_events_events', function($table)
        {
            $table->integer('category_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('jorgeandrade_events_events', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
