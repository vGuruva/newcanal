<?php

namespace JorgeAndrade\Events;

use OFFLINE\SiteSearch\Classes\Providers\ResultsProvider;
use OFFLINE\SiteSearch\Classes\Result;
use JorgeAndrade\Events\Models\Event as Offers;

class OffersProvider extends ResultsProvider
{
    public function search()
    {
		$searchQuery = $this->query;
        // Get your matching models
        $matching = Offers::where('calendar_id', '=', '2')
			    		->where(function ($matching) use ($searchQuery) {
							$matching->orWhere('title', 'like', "%{$searchQuery}%")->orWhere('content', 'like', "%{$searchQuery}%");
			    		})->get();

        // Create a new Result for every match
        foreach ($matching as $match) {
            $result            = $this->newResult();

            $result->relevance = 1;
            $result->title     = $match->name;
            $result->text      = $match->detail;
            $result->url       = '/offers/' . $item->slug;
            $result->thumb     = $match->banner;
            $result->model     = $match;
            $result->meta      = [];

            // Add the results to the results collection
            $this->addResult($result);
        }

        return $this;
    }

    public function displayName()
    {
        return 'Offers';
    }

    public function identifier()
    {
        return 'Offers';
    }
}
