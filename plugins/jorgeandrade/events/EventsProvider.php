<?php

namespace JorgeAndrade\Events;

use OFFLINE\SiteSearch\Classes\Providers\ResultsProvider;
use OFFLINE\SiteSearch\Classes\Result;
use JorgeAndrade\Events\Models\Event as Events;
use Cms\Classes\Theme;

class EventsProvider extends ResultsProvider
{
    public function search()
    {
	$searchQuery = $this->query;
        
        $themeName = Theme::getActiveTheme()->getDirName();
        $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first();         
        
        // Get your matching models
        $matching = Events::where('hyprop_malls_id', $mall->id )
			    		->where(function ($matching) use ($searchQuery) {
                                                            $matching
                                                            ->orWhere('name', 'like', "%{$searchQuery}%")
                                                            ->orWhere('detail', 'like', "%{$searchQuery}%");
			    		})->get();

        // Create a new Result for every match
        foreach ($matching as $match) {
            
            $result            = $this->newResult();
            $result->relevance = 1;
            $result->title     = $match->name;
            $result->text      = $match->detail;
            $result->url       = '/events/' . $match->slug;
            $result->thumb     = $match->banner;
            $result->model     = $match;
            $result->meta      = [
                'date' => date('d M', strtotime($match->start_at)) . ' - ' . date('d M Y', strtotime($match->ends_at)),
				'price' => $match->excerpt
            ];

            // Add the results to the results collection
            $this->addResult($result);
        }

        return $this;
    }

    public function displayName()
    {
        return 'Events';
    }

    public function identifier()
    {
        return 'Events';
    }
}
