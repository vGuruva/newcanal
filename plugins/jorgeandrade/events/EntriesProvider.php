<?php

namespace JorgeAndrade\Events;

use OFFLINE\SiteSearch\Classes\Providers\ResultsProvider;
use JorgeAndrade\Events\Models\Event as Entries;

class EntriesProvider extends ResultsProvider
{
    public function search()
    {
		$searchQuery = $this->query;
        // Get your matching models
        $matching = Entries::where('calendar_id', '<>', '2')->where('calendar_id', '<>', '3')
			    		->where(function ($matching) use ($searchQuery) {
							$matching->orWhere('title', 'like', "%{$searchQuery}%")->orWhere('content', 'like', "%{$searchQuery}%");
			    		})->get();

        // Create a new Result for every match
        foreach ($matching as $match) {
            $result            = $this->newResult();

            $result->relevance = 1;
            $result->title     = $match->name;
            $result->text      = $match->detail;
            $result->url       = '/' . $item->slug;
            $result->thumb     = $match->banner;
            $result->model     = $match;
            $result->meta      = [];

            // Add the results to the results collection
            $this->addResult($result);
        }

        return $this;
    }

    public function displayName()
    {
        return 'Entries';
    }

    public function identifier()
    {
        return 'Entries';
    }
}
