<?php namespace JorgeAndrade\Events;

use Cms\Classes\Controller;
use Event;
use System\Classes\PluginBase;
use JorgeAndrade\Events\Models\Event as CalendarEvent;

/**
 * Events Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Adds date translation to the plugin
     */
    public function boot()
    {
        
        Event::Listen('cms.page.beforeDisplay', function (Controller $controller, $url, $page) {
            if (extension_loaded('intl')) {
                $twigIntlExtension = new \Twig_Extensions_Extension_Intl();

                if (!$controller->getTwig()->hasExtension($twigIntlExtension->getName())) {
                    $controller->getTwig()->addExtension($twigIntlExtension);
                }
            }
        });

        Event::listen('offline.sitesearch.query', function ($query) {
			return [new OffersProvider(), new EventsProvider(), new EntriesProvider()];
        });
        
        Event::listen('offline.sitesearch.extend', function () {
			return new EventsProvider();
        });        
        
    }

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Calendar Events',
            'description' => 'Manage properly your own events',
            'author' => 'Jorge Andrade',
            'icon' => 'icon-calendar',
            'homepage' => 'http://andradedev.com',
        ];
    }

    public function registerComponents()
    {
        return [
            'JorgeAndrade\Events\Components\Offerlist' => 'offerlist',
            'JorgeAndrade\Events\Components\EventList' => 'eventlist',
            'JorgeAndrade\Events\Components\Event' => 'event',
            'JorgeAndrade\Events\Components\OfferDetail' => 'offerdetail',
            'JorgeAndrade\Events\Components\WinDetail' => 'windetail',
            'JorgeAndrade\Events\Components\WinListing' => 'winlisting',
            'JorgeAndrade\Events\Components\Homepageevents' => 'homepageevents',
            'JorgeAndrade\Events\Components\HomepageOffers' => 'homepageoffers',
            'JorgeAndrade\Events\Components\MustHaves' => 'musthaves',
        ];
    }
}
