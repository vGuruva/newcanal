<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;

class HomepageOffers extends ComponentBase
{
	public $offers;

    public function componentDetails() {
        return [
            'name' => 'Homepage Offer List',
            'description' => 'Displays current offers to display on the Homepage.'
        ];
    }

    public function onRun() {
    	$this->offers = $this->loadOffers();
    }

    protected function loadOffers()
    {
    	$offers = Events::listFrontEnd([
				'event_type' => 2,
        		'limit' => 3,
				'orderBy' => 'start_at',
				'orderDirection' => 'DESC'
		]);
		
    	return $offers;
    }

}