<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Calendar;
use JorgeAndrade\Events\Models\Event as Events;
use CreativeSpark\Stores\Models\Category;
use Input;
use Session;
use Redirect;

class offerlist extends ComponentBase
{
    public $locale = 'en';
    public $events;
    public $tab;
    public $calendar;
    public $search;
	public $categories;
	public $selectedCategories;
	public $recentlyViewedArray;
	public $filter_date;
	public $pageNumber;

    public function componentDetails()
    {
        return [
            'name' => 'Offer List',
            'description' => 'Show a list of offers.',
        ];
    }

    public function defineProperties()
    {
        return [
            'eventsPerPage' => [
                'title' => 'Events per page',
                'description' => 'Max event shown per page.',
                'default' => 10,
            ],
            'noEventMessage' => [
                'title' => 'No Event Message',
                'description' => 'Message shown when is not events.',
                'default' => 'No events.',
            ],
            'eventPage' => [
                'title' => 'Event Page',
                'description' => 'Name of the event page file for the "Read more" links. This property is used by the default component partial.',
                'default' => 'events/event',
                'group' => 'Links',
            ],
        ];
    }

    public function onRun()
    {
		$this->pageNumber = 1;

		if(Input::get('pageNumber')){
			$this->pageNumber = $this->page['pageNumber'] = Input::get('pageNumber');
		}

    	$this->tab = $this->page['tab'] = Input::get('tab');
        $this->noEventMessage = $this->page['noEventMessage'] = $this->property('noEventMessage');
        $this->search = $this->page['search'] = Input::get('search');
        $this->events = $this->page['events'] = $this->listEvents();
        $this->calendar = $this->page['calendar'] = Calendar::where('id', 2)->first();
		$this->recentlyViewedArray = $this->page['recentlyViewedArray'] = Session::get('store.recentlyViewed');
		$this->filter_date = $this->page['filter_date'] = Input::get('filter_date');

		//category handle
		$this->selectedCategories = $this->page['selectedCategories'] = $this->getSelectedCategories();
		$this->categories = $this->page['categories'] = $this->loadCategories();
    }

	protected function loadCategories() {
		$primary_category = Category::where('parent_id', 0)->orWhereNull('parent_id')->get();
		$secondary_category = array();
		$tertiary_category = array();

		if($this->param('category1')){
			$secondary_category = Category::where('slug', '=', $this->param('category1'))->first()->children()->get();
		}

		if($this->param('category2')){
			$tertiary_category = Category::where('slug', '=', $this->param('category2'))->first()->children()->get();
		}

		if(Input::get('category1')){
			$secondary_category = Category::where('slug', '=', Input::get('category1'))->first()->children()->get();
		}

		if(Input::get('category2')){
			$tertiary_category = Category::where('slug', '=', Input::get('category2'))->first()->children()->get();
		}

		return array('primaryCategory' => $primary_category, 'secondaryCategory' => $secondary_category, 'tertiaryCategory' => $tertiary_category);
	}

	protected function getSelectedCategories() {
		$primary_category = $this->param('category1');
		$secondary_category = $this->param('category2');
		$tertiary_category = $this->param('category3');

		if(Input::get('category1')){
			$primary_category = Input::get('category1');
		}

		if(Input::get('category2')){
			$secondary_category = Input::get('category2');
		}

		if(Input::get('category3')){
			$tertiary_category = Input::get('category3');
		}

		return array('primaryCategory' => $primary_category, 'secondaryCategory' => $secondary_category, 'tertiaryCategory' => $tertiary_category);
	}

    protected function listEvents()
    {
		$primary_category = $this->param('category1');
		$secondary_category = $this->param('category2');
		$tertiary_category = $this->param('category3');

		if(Input::get('category1')){
			$primary_category = Input::get('category1');
		}

		if(Input::get('category2')){
			$secondary_category = Input::get('category2');
		}

		if(Input::get('category3')){
			$tertiary_category = Input::get('category3');
		}

    	$events = Events::listFrontEnd([
			'page' => $this->pageNumber,
			'perPage' => $this->property('eventsPerPage'),
			'event_type' => 2,
			'search' => trim($this->search),
			'tab' => $this->tab,
			'category1' => $primary_category,
			'category2' => $secondary_category,
			'category3' => $tertiary_category,
			'filter_date' => Input::get('filter_date'),
			'limit' => $this->property('eventsPerPage')
    	]);

    	return $events;
    }

}
