<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;

class WinDetail extends ComponentBase
{
	public $locale = 'en';
	public $winEvent;

	public function componentDetails()
	{
		return [
				'name' => 'Win Detail',
				'description' => 'The detail code for win.',
		];
	}

	public function onRun()
	{
		$this->winEvent = $this->loadWin();

		if(!$this->winEvent){
			return Redirect::to('/404');
		}
	}

	protected function loadWin()
	{
		$winEvent = Events::where('slug', '=', $this->param('slug'));

		return $winEvent->first();
	}

}
