<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as CalendarEvent;
use Session;
use Redirect;

class Event extends ComponentBase {

	public $eventDetail;
	public $similarEvents;

    public function componentDetails()
    {
        return [
            'name' => 'Event Details',
            'description' => 'Displays an event on the page',
        ];
    }

    public function defineProperties()
    {
    	return [
    			'results' => [
    					'title' => 'Number of similar Events',
    					'description' => 'please select the amount of random Events loaded on the page',
    					'default' => 10,
    					'validationPattern' => '^[0-9]+$',
    					'validationMessage' => 'Only numbers allowed'
    			]
    	];
    }

    public function onRun() {

    	// $this->eventDetail = $this->loadEvent();
        $this->eventDetail = $this->page['eventDetail'] = $this->loadEvent();

		if(!$this->eventDetail){
			return Redirect::to('/404-events');
		}

        //$this->similarEvents = $this->getEvents();
    	$this->similarEvents = $this->page['similarEvents'] = $this->getEvents();

		if($this->eventDetail->count() == 0){
			return Redirect::to('/404-events');
		}

        //Session::put('event.event.recentlyViewed.'.$this->eventDetail->id, $this->page['eventDetail']->id);
    	Session::put('event.event.recentlyViewed.'.$this->page['eventDetail']->id, $this->page['eventDetail']->id);
    }

    protected function loadEvent(){
    	$eventDetail = CalendarEvent::where('slug', '=', $this->param('slug'));

    	return $eventDetail->first();
    }

    protected function getEvents(){

    	$similarEvents = CalendarEvent::listFrontEnd ([
    			'event_type' => 3,
    			'limit' => $this->property('results'),
    			'orderBy' => 'random',
                //'notThisID' => $this->eventDetail->id
    			'notThisID' => $this->page['eventDetail']->id,
                        'showCurrent' => false
    	]);

    	return $similarEvents;
    }

}
