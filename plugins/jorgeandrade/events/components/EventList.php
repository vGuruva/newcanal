<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;
use JorgeAndrade\Events\Models\Calendar;
use JorgeAndrade\Events\Models\Category;
use Cms\Classes\Theme;
use Input;
use Redirect;

class EventList extends ComponentBase {
	public $events;
    public $search;
	public $categories;
	public $selectedCategories;
	public $filter_date;

	public function componentDetails() {
		return [
				'name' => 'Events List Component',
				'description' => 'Show a list of events.'
		];
	}

	public function defineProperties() {
		return [
				'eventsPerPage' => [
					'title' => 'Events per page',
					'description' => 'Max event shown per page.',
					'default' => 0
				],
				'eventCalandar' => [
					'title' => 'Select Calendar',
					'description' => 'Choose the calendar to show.',
					'default' => 3,
					'type' => 'dropdown'
				]
		];
	}

        public function getEventCalandarOptions()
        {
            return Calendar::lists('name', 'id');
        }

	public function onRun() {
            
                $this->search = $this->page['search'] = Input::get('search');
		$this->filter_date = $this->page['filter_date'] = Input::get('filter_date');

		//category handle
		$this->selectedCategories = $this->page['selectedCategories'] = $this->param('category');
                
                $themeName = Theme::getActiveTheme()->getDirName();

                if( !empty($themeName) ) {
                   $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first(); 
                }                

		$this->categories = $this->page['categories'] = Category::whereNull('deleted_at')->where('name','<>','Uncategorized')->where('jorgeandrade_events_categories.hyprop_malls_id', $mall->id )->get();

		$this->events = $this->loadEvents();
	}

	protected function loadEvents() {
		$filter_date = Input::get('filter_date', 6);
		$category1 = ($this->param('category')? $this->param('category'): false);
		$search = Input::get('search');
		$this->pageNumber = 1;

		if(Input::get('pageNumber')){
			$this->pageNumber = $this->page['pageNumber'] = Input::get('pageNumber');
		}

		// if($filter_date == 1 || $filter_date == 0) {
			$orderBy = 'is_allday';
			$orderDirection = 'DESC';
		// } else {
		// 	$orderBy = 'name';
		// 	$orderDirection = 'ASC';
		// }

		$events = Events::listFrontEnd ([
			'page' => $this->pageNumber,
			'perPage' => $this->property('eventsPerPage'),
			'search' => $search,
			'event_type' => $this->property('eventCalandar'),
			'limit' => $this->property('eventsPerPage'),
			'orderBy' => $orderBy,
			'orderDirection' => $orderDirection,
			'filter_date' => $filter_date,
			'category1' => $category1
		]);

		return $events;
	}
}
