<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;

class Homepageevents extends ComponentBase
{
    public function componentDetails() {
        return [
            'name' => 'Homepage Events List',
            'description' => 'Displays current events to display on the Homepage.'
        ];
    }

    public function defineProperties()
    {
    	return [
			'eventsPerPage' => [
				'title' => 'Events per page',
				'description' => 'Max event shown per page.',
				'default' => 5,
			],
			'isUpComing' => [
				'title' => 'Up coming events',
				'description' => 'Only shows up-coming events.',
				'default' => 0,
				'type' => 'checkbox'
			]
    	];
    }

    public function onRun() {
        $this->events = $this->loadEvents();
    }

    protected function loadEvents()
    {
		if($this->property('isUpComing')) {
			$filter_date = 4;
		} else {
			$filter_date = false;
		}

        $events = Events::listFrontEnd([
				'event_type' => 3,
        		'limit' => $this->property('eventsPerPage'),
				'orderBy' => 'is_allday',
				'orderDirection' => 'DESC',
				'filter_date' => $filter_date
		]);

		$this->isUpComing = $this->property('isUpComing');

		return $events;
    }

    public $events;
	public $isUpComing;
}
