<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;
use CreativeSpark\Stores\Models\Hour;
use Input;
use Session;
use Redirect;

class OfferDetail extends ComponentBase {

	public $offerDetail;
	public $similarOffers;
	public $favoriteArray;

	public function componentDetails(){
		return [
			'name' => 'Offer Details',
			'description' => 'Offer details page'
		];
	}

	public function defineProperties() {
		return [
				'results' => [
						'title' => 'Number of similar Offers',
						'description' => 'please select the amount of random offers loaded on the page',
						'default' => 10,
						'validationPattern' => '^[0-9]+$',
						'validationMessage' => 'Only numbers allowed'
				]
		];
	}

	public function onRun() {
		$this->offerDetail = $this->loadOffer();

		if(!$this->offerDetail){
			return Redirect::to('/404-offers');
		}

		$this->favoriteArray = Session::get('store.favorite');

		$this->similarOffers = $this->getOffers();

		$this->page['events'] = $this->offerDetail;

		Session::put('event.offer.recentlyViewed.'.$this->offerDetail->id, $this->offerDetail->id);
	}

	protected function loadOffer(){
		$offerDetail = Events::where('slug', '=', $this->param('slug'))->first();

		if(!empty($offerDetail->store->website)){
			$offerDetail->store->website_url = $offerDetail->store->website;
			$offerDetail->store->website_display = $offerDetail->store->website;

			if(strstr($offerDetail->store->website, '://') == false) {
				$offerDetail->store->website_display = $offerDetail->store->website;
				$offerDetail->store->website_url = 'http://'.$offerDetail->store->website;
			} else {
				$offerDetail->store->website_display = substr(strstr($offerDetail->store->website, '://'), 3);
				$offerDetail->store->website_url = $offerDetail->store->website;
			}
		}

        if($offerDetail->store->hours->count() > 0){
            $offerDetail->store->trading_hours = $offerDetail->store->hours->first();
            $offerDetail->store->is_open = Hour::whereRaw('"'.date('H:i:s', strtotime('now')).'" between ' . strtolower(date('l')).'_start and '.strtolower(date('l')).'_end')->where('store_id','=', $offerDetail->store->id)->count();
        }else{
            $offerDetail->store->trading_hours = Hour::where('id', '=', 1)->first();
            $offerDetail->store->is_open = Hour::whereRaw('"'.date('H:i:s', strtotime('now')).'" between ' . strtolower(date('l')).'_start and '.strtolower(date('l')).'_end')->where('id','=', 1)->count();
            if(!$offerDetail->store->trading_hours){
                $offerDetail->store->trading_hours = array(
                    'monday_start'      => '09:00',
                    'monday_end'        => '21:00',
                    'tuesday_start'     => '09:00',
                    'tuesday_end'       => '21:00',
                    'wednesday_start'   => '09:00',
                    'wednesday_end'     => '21:00',
                    'thursday_start'    => '09:00',
                    'thursday_end'      => '21:00',
                    'friday_start'      => '09:00',
                    'friday_end'        => '21:00',
                    'saturday_start'    => '09:00',
                    'saturday_end'      => '21:00',
                    'sunday_start'      => '09:00',
                    'sunday_end'        => '21:00',
                );
            }
        }

		return $offerDetail;
	}

	protected function getOffers(){

		$similarOffers = Events::listFrontEnd ([
				'event_type' => 2,
				'limit' => $this->property('results'),
				'orderBy' => 'random',
				'notThisID' => $this->offerDetail->id
		]);

		return $similarOffers;
	}
}
?>
