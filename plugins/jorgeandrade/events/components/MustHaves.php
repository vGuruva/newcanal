<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;

class MustHaves extends ComponentBase
{
	public $locale = 'en';
	public $products;
	
	public function componentDetails()
	{
		return [
				'name' => 'Must Haves',
				'description' => 'Show a list of must have products for the homepage.',
		];
	}
	
	public function onRun()
	{
		$this->products = $this->page['products'] = $this->listProducts();
	}

	protected function listProducts()
	{
		$products = Events::listFrontEnd([
				'event_type' => 4,
				'showCurrent' => false
		]);
		
		return $products;
	}
	
}
