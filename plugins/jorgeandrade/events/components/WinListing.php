<?php namespace JorgeAndrade\Events\Components;

use Cms\Classes\ComponentBase;
use JorgeAndrade\Events\Models\Event as Events;

class WinListing extends ComponentBase
{
	public $locale = 'en';
	public $events;

	public function componentDetails()
	{
		return [
			'name' => 'Win Listing',
			'description' => 'Show a list of competitions.',
		];
	}

	public function onRun()
	{
		$this->events = $this->page['events'] = $this->listWins();
	}

	protected function listWins()
	{
		$events = Events::listFrontEnd([
			'page' => 1,
			'perPage' => 1000,
			'event_type' => 1
		]);

		return $events;
	}

}
