<?php namespace JorgeAndrade\Events\Models;

use Model;
use BackendAuth;
use Illuminate\Support\Facades\Redirect;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jorgeandrade_events_categories';

    /**
     * @var array Relations
     */
    public $hasMany = [
        'event' => 'JorgeAndrade\Events\Models\Event'
    ];

	public function beforeSave()
	{

		$user = BackendAuth::getUser();  
		
		if( is_int($user->role->mall_id) ):
			
			$this->hyprop_malls_id = $user->role->mall_id;
		
		else:
			
			return Redirect::back();
		
		endif;     
	}
}
