<?php namespace JorgeAndrade\Events\Models;

use Model;
use BackendAuth;
use Illuminate\Support\Facades\Redirect;

/**
 * Calendar Model
 */
class Calendar extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jorgeandrade_events_calendars';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'events' => 'JorgeAndrade\Events\Models\Event',
    ];

    public function getEventsCountAttribute()
    {
        return $this->events()->count();
    }

	public function beforeSave()
	{

		$user = BackendAuth::getUser();  
		
		if( is_int($user->role->mall_id) ):
			
			$this->hyprop_malls_id = $user->role->mall_id;
		
		else:
			
			return Redirect::back();
		
		endif;     
	}

}
