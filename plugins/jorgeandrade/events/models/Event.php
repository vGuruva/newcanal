<?php namespace JorgeAndrade\Events\Models;

use Model;
use CreativeSpark\Stores\Models\Store;
use Session;
use BackendAuth;
use Illuminate\Support\Facades\Redirect;
use Cms\Classes\Theme;

/**
 * Event Model
 */
class Event extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jorgeandrade_events_events';

	public $implement = ['@Renatio.SeoManager.Behaviors.SeoModel'];

	public function __construct()
    {
		if(!BackendAuth::check())
			return parent::__construct();


		$user = BackendAuth::getUser();
		
		if($user->role->name == "Store Owners" && $user){
			$this->implement = [];
		}
		
		return parent::__construct();
    }


    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'slug', 'detail', 'calendar_id', 'course_id'];

    protected $dates = ['start_at', 'ends_at', 'created_at', 'updated_at'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'calendar' => 'JorgeAndrade\Events\Models\Calendar',
        'store' => 'CreativeSpark\Stores\Models\Store',
		'category' => 'JorgeAndrade\Events\Models\Category'
    ];

    public $hasMany = [
        'dates' => 'JorgeAndrade\Events\Models\Date',
    ];

    public $attachOne = [
        'banner' => 'System\Models\File',
	];

    public function setUrl($pageName, $controller)
    {
        $params = [
            'id' => $this->id,
            'slug' => $this->slug,
        ];
        return $this->url = $controller->pageUrl($pageName, $params);
	}

	public function beforeSave() {
		$user = BackendAuth::getUser();
		if($user->role->name == "Store Owners"){
		
			$store_id = Store::select('id')->where('backend_user_id', '=', $user->id)->first();

			$this->store_id = $store_id->id;
			$this->calendar = 2;
			$this->status = 0;

		}
            
		if( is_int($user->role->mall_id) ):
			
			$this->hyprop_malls_id = $user->role->mall_id;
		
		else:
			
			return Redirect::back();
		
		endif;                  
                
                
	}

    public function scopeListFrontEnd($query, $options = []){
    	extract(array_merge([
    			'page' => 1, // int
    			'perPage' => 10, // int
    			'event_type' => '',
    			'search' => '',
    			'tab' => '',
				'limit' => 0,
				'orderBy' => 'name',
				'orderDirection' => 'ASC',
    			'notThisID' => 0,
    			'showCurrent' => true,
				'category1' => false,
				'category2' => false,
				'category3' => false,
				'filter_date' => false,
				'filter_store' => false
    	], $options));

    	$query->select('jorgeandrade_events_events.*')
    	->join('jorgeandrade_events_calendars', 'jorgeandrade_events_events.calendar_id', '=', 'jorgeandrade_events_calendars.id')
    	->groupBy('jorgeandrade_events_events.id')
		->where('status', '=', 1);

    	if ($orderBy == 'random') {
    		$query->inRandomOrder();
    	} elseif($orderBy == 'is_allday') {
    		$query->orderBy('jorgeandrade_events_events.'.$orderBy, $orderDirection)->orderBy('jorgeandrade_events_events.start_at', 'ASC');
    	} else {
    		$query->orderBy('jorgeandrade_events_events.'.$orderBy, $orderDirection);
    	}

    	if($showCurrent) {
			switch ($filter_date) {
				case 1: // shows current events
					$query->whereRaw('"'.date('Y-m-d H:i:s').'" between start_at and ends_at');
					break;
				case 2: // shows next weeks events
					$query->whereRaw("('".date('Y-m-d H:i:s',strtotime('next monday'))."' between start_at and ends_at OR start_at between '".date('Y-m-d',strtotime('next monday'))."' AND '".date('Y-m-d',strtotime('next Sunday + 7 days'))."')");
					$query->whereRaw("('".date('Y-m-d H:i:s',strtotime('next Sunday + 7 days'))."' between start_at and ends_at OR ends_at between '".date('Y-m-d',strtotime('next monday'))."' AND '".date('Y-m-d',strtotime('next Sunday + 7 days'))."')");
					break;
				case 3: // shows next months events
					$query->whereRaw("((MONTH(`start_at`) = ".date('m',strtotime('next month'))." AND YEAR(`start_at`) = ".date('Y',strtotime('next month')).") OR (MONTH(`ends_at`) = ".date('m',strtotime('next month'))." AND YEAR(`ends_at`) = ".date('Y',strtotime('next month'))."))");
					break;
				case 4: // shows upcoming events
					$query->whereRaw("start_at > '".date('Y-m-d H:i:s')."'");
					break;
				case 5: // shows past events going back 2 years
					$query->whereRaw("ends_at between '".date('Y-m-d',strtotime('- 2 years'))."' and '".date('Y-m-d')."'");
					break;
				case 6: // shows former default
					$query->whereRaw('"'.date('Y-m-d H:i:s').'" between start_at and ends_at');
					break;

				default: // shows current and upcoming events
					$query->whereRaw("ends_at >= '".date('Y-m-d H:i:s')."'");
					break;
			}
    	} else {
    		$query->whereRaw('ends_at >= "'.date('Y-m-d H:i:s').'"');
		}

		switch ($event_type) {
			case 2 :
				if($notThisID) {
					$query->where('jorgeandrade_events_calendars.name', '=', 'Offers')->where('jorgeandrade_events_events.id', '<>', $notThisID);
				} else {
					$query->where('jorgeandrade_events_calendars.name', '=', 'Offers');
				}

				$query->leftJoin('creativespark_stores_', 'jorgeandrade_events_events.store_id', '=', 'creativespark_stores_.id')
		    	->leftJoin('creativespark_stores_stores_categories', 'creativespark_stores_.id', '=', 'creativespark_stores_stores_categories.store_id')
		    	->leftJoin('creativespark_stores_categories', 'creativespark_stores_stores_categories.category_id', '=', 'creativespark_stores_categories.id')
		    	->whereNull('creativespark_stores_.deleted_at');

				if($filter_store){
					$query->where('creativespark_stores_.id', $filter_store);
				}

		    	if($category3) {
		    		$query->where('creativespark_stores_categories.slug', '=', $category3);
		    	} else if($category2) {
		    		$query->where('creativespark_stores_categories.slug', '=', $category2);
		    	} else if($category1) {
		    		$query->where('creativespark_stores_categories.slug', '=', $category1);
		    	}
				break;
			case 3 :
				if($notThisID) {
					$query->where('jorgeandrade_events_calendars.name', '=', 'Events')->where('jorgeandrade_events_events.id', '<>', $notThisID);
				} else {
					$query->where('jorgeandrade_events_calendars.name', '=', 'Events');
				}

				if($category1) {
					$query->leftJoin('jorgeandrade_events_categories', 'jorgeandrade_events_events.category_id', '=', 'jorgeandrade_events_categories.id');

		    		$query->where('jorgeandrade_events_categories.slug', '=', $category1);
		    	}
				break;
			case 1 :
				$query->where('jorgeandrade_events_calendars.name', '=', 'Win');
				break;
			case 4 :
				$query->where('jorgeandrade_events_calendars.name', '=', 'Top Must Haves');
				break;
			default:
				$query->where('jorgeandrade_events_calendars.name', '=', 'Offers');
		};

        if($limit != 0){
        	$query->take($limit);
        }

    	if($search) {
    		$query->where(function ($query) use ($search) {
    			$query->orWhere('jorgeandrade_events_events.name', 'like', '%'.$search.'%')->orWhere('jorgeandrade_events_events.detail', 'like', '%'.$search.'%');
    		});
    	}

    	if($tab == 'recently-viewed' && $event_type == 2) {
    		$query->whereIn('jorgeandrade_events_events.id', Session::get('event.offer.recentlyViewed'));
    	}

    	if($tab == 'favorited-stores' && $event_type == 2) {
    		$query->whereIn('jorgeandrade_events_events.id', Session::get('event.offer.favorite'));
    	}
        
        $themeName = Theme::getActiveTheme()->getDirName();
        
        if( !empty($themeName) ) {
           $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first(); 
        }
        
        if( !empty($mall) ){
            $query->where('jorgeandrade_events_events.hyprop_malls_id', $mall->id );
        }        

	// paginate result set from settings
        $event_data = $query->paginate($perPage, $page);

    	return $event_data;
    }

}
