<?php namespace JorgeAndrade\Events\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Events extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController','Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('JorgeAndrade.Events', 'main-menu-item', 'events');
    }
    
    
    public function formExtendFields($form)
    {
        if ($this->user->role->name == "Store Owners") {
            $form->removeField('calendar');
            $form->removeField('category');
            $form->removeField('lat_long');
            $form->removeField('address');
            $form->removeField('is_allday');
        }
    }
}
