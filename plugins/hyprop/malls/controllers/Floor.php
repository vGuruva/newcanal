<?php namespace Hyprop\Malls\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Floor extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
		'Backend\Behaviors\FormController',
		'Backend\Behaviors\ReorderController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Hyprop.Malls', 'main-menu-item', 'side-menu-item2');
    }

    public function listExtendQuery($query)
    {
        // get the mall's items
        if( !empty($this->user->role->mall_id) ) {

            if( is_int($this->user->role->mall_id) ):
                $query->where( 'mall_id', '=', $this->user->role->mall_id );
            endif;

        } else { // else get all

            $query->whereRaw( '1 = 1' );

        }
    }
}
