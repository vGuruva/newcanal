<?php
namespace Hyprop\Malls\Models;
use Cms\Classes\Theme;

use Model;

/**
 * Model
 */
class Malls extends Model
{
    use \October\Rain\Database\Traits\Validation;
	use \October\Rain\Database\Traits\Sortable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'hyprop_malls_';

    /* Relations */
    public $attachOne = [
        'site_logo' => 'System\Models\File'
    ];

    public $attachMany = [
    'mall_maps' => 'System\Models\File'
    ];

    public $hasMany = [
        'stores' => ['CreativeSpark\Stores\Models\Store'],
        'floors' => ['Hyprop\Malls\Models\Floor'],
        'userRole' => ['Backend\Models\UserRole']
    ];

    public function getThemeNameOptions($value, $formData)
    {
        $theme_arr = Theme::all();
        $return_arr = array();
        foreach($theme_arr as $theme){
            $theme->getConfig();
            $return_arr[$theme->getCustomData()->theme] = $theme->getConfig()['name'];
        }

        return $return_arr;
    }


}
