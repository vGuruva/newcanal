<?php namespace Hyprop\Malls\Models;

use Model;
use CreativeSpark\Stores\Models\Store;

/**
 * Model
 */
class Floor extends Model
{
    use \October\Rain\Database\Traits\Validation;
	use \October\Rain\Database\Traits\Sortable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'hyprop_malls_floors';

    /* Relations */
	public $attachOne = [
		'mall_map' => 'System\Models\File'
	];

	public $hasMany = [
		'stores' => ['CreativeSpark\Stores\Models\Store']
	];

	public $belongsTo = [
		'mall' => ['Hyprop\Malls\Models\Malls']
	];

    public function beforeSave()
    {
    
        $user = BackendAuth::getUser();  
        
        if( is_int($user->role->mall_id) ):
            
            $this->mall_id = $user->role->mall_id;
        
        else:
            
            return Redirect::back();
        
        endif;     
    }

	// after successful save write the javascript file, as with the cron
	public function afterSave() {
		Store::saveMapFile();
	}

}
