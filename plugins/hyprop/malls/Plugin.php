<?php namespace Hyprop\Malls;

use System\Classes\PluginBase;
use Hyprop\Malls\Models\Malls;
use Backend\Controllers\UserRoles as RolesController;
use Backend\Models\UserRole as RoleModel;
use Event;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Hyprop\Malls\Components\ContactDetails' => 'contactdetails',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
		Event::listen('formBuilder.beforeSendMessage', function ($form, $data, $files) {

			if(isset($data['enquiry'])){
				$mall = Malls::where('id', 1)->first();

				if(env('APP_DEBUG') == 'dev') {
					$form['to_email'] = 'bryce@virtualdesigns.co.za';
					$form['to_name'] = 'Bryce';
				} else {
					switch ($data['enquiry']) {
						case 'leasing':
							$form['to_email'] = $mall->emaillease;
							$form['to_name'] = 'Leasing';
							break;
						case 'complaint':
							$form['to_email'] = $mall->emailcomplaint;
							$form['to_name'] = 'Complaints';
							break;
						case 'general':
							$form['to_email'] = $mall->email;
							$form['to_name'] = 'General Enquiry';
							break;

						default:
							$form['to_email'] = $mall->email;
							$form['to_name'] = 'General Enquiry';
							break;
					}
				}
			}

		});

		RoleModel::extend(function ($model) {
			$model->belongsTo['mall'] = ['Hyprop\Malls\Models\Malls'];
		});

		Event::listen('backend.form.extendFields', function($widget) {

			// Only for the User controller
			if (!$widget->getController() instanceof RolesController) {
				return;
			}

			// Only for the User model
			if (!$widget->model instanceof RoleModel) {
				return;
			}

			// Add an extra description field
			$widget->addFields([
				'mall' => [
					'label'			=> 'Select Mall',
					'type'			=> 'relation',
					'placeholder'	=> 'Select Mall'
				]
			]);
		});
    }
}
