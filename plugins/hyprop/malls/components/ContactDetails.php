<?php namespace Hyprop\Malls\Components;

use Cms\Classes\ComponentBase;
use Hyprop\Malls\Models\Malls;

class ContactDetails extends ComponentBase
{
	public $locale = 'en';
	public $contactDetail;

	public function componentDetails(){
		return [
			'name' => 'Contact Details',
			'description' => 'Contact details of the mall'
		];
	}

	public function onRun() {
		$this->contactDetail = Malls::where('id', 1)->first();
	}
}
?>
