<?php namespace Hyprop\Malls\Components;

use Cms\Classes\ComponentBase;
use Hyprop\Malls\Models\Malls;

class SiteLogo extends ComponentBase
{
	public $locale = 'en';
	public $contactDetail;

	public function componentDetails(){
		return [
			'name' => 'Site Logo',
			'description' => 'Add HTML for header section with the mall logo'
		];
	}

	public function onRun() {
        
		$themeName = Theme::getActiveTheme()->getDirName();
        
        if( !empty($themeName) ) {
           $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first(); 
        }
		$this->contactDetail = Malls::where('id', 1)->first();
	}
}
?>
