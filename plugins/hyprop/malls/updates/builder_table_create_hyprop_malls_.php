<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateHypropMalls extends Migration
{
    public function up()
    {
        Schema::create('hyprop_malls_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('hyprop_malls_');
    }
}
