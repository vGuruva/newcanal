<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMalls10 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->text('google_tracking_code_header')->nullable();
            $table->text('google_tracking_code_body')->nullable();
            $table->string('store_count')->nullable();
            $table->string('theme_name')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->dropColumn('google_tracking_code_header');
            $table->dropColumn('google_tracking_code_body');
            $table->dropColumn('store_count');
            $table->dropColumn('theme_name');
        });
    }
}