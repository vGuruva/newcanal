<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMalls3 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->string('contactvisitor');
            $table->string('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('address')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->dropColumn('contactvisitor');
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('address')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
