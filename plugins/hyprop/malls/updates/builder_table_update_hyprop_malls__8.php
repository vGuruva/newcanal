<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMalls8 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->string('emailmarketing')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->string('emailmarketing', 255)->nullable(false)->change();
        });
    }
}
