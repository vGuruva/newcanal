<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMalls5 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('address')->nullable()->unsigned(false)->default(null)->change();
            $table->string('contactvisitor')->nullable()->change();
            $table->string('emaillease')->nullable()->change();
            $table->string('emailcomplaint')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('address')->nullable()->unsigned(false)->default(null)->change();
            $table->string('contactvisitor', 255)->nullable(false)->change();
            $table->string('emaillease', 255)->nullable(false)->change();
            $table->string('emailcomplaint', 255)->nullable(false)->change();
        });
    }
}
