<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBackendUserRoles extends Migration
{
    public function up()
    {
        Schema::table('backend_user_roles', function($table)
        {
            $table->integer('mall_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('backend_user_roles', function($table)
        {
            $table->dropColumn('mall_id');
        });
    }
}