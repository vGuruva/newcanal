<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMallsFloors2 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_floors', function($table)
        {
            $table->integer('sort_order')->nullable();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_floors', function($table)
        {
            $table->dropColumn('sort_order');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
