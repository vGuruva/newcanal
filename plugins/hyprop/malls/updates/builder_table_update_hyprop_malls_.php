<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMalls extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->string('contact')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->dropColumn('contact');
            $table->dropColumn('email');
            $table->dropColumn('address');
        });
    }
}
