<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateHypropMallsFloors extends Migration
{
    public function up()
    {
        Schema::create('hyprop_malls_floors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->string('floor_code')->nullable();
            $table->integer('mall_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('hyprop_malls_floors');
    }
}
