<?php namespace Hyprop\Malls\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMalls2 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->string('contactmanager')->nullable();
            $table->string('contactmarketing')->nullable();
            $table->string('contactsecurity')->nullable();
            $table->string('contactparking1')->nullable();
            $table->string('contactparking2')->nullable();
            $table->text('mallhours')->nullable();
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('address')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_malls_', function($table)
        {
            $table->dropColumn('contactmanager');
            $table->dropColumn('contactmarketing');
            $table->dropColumn('contactsecurity');
            $table->dropColumn('contactparking1');
            $table->dropColumn('contactparking2');
            $table->dropColumn('mallhours');
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('address')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
