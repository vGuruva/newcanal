<?php namespace Hyprop\Movies\Models;

use Model;
use DB;
use Hyprop\Movies\Models\Genre;
use Cms\Classes\Theme;
use BackendAuth;
use Illuminate\Support\Facades\Redirect;
use Cms\Classes\Page as CmsPage;

/**
 * Model
 */
class Movie extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'hyprop_movies_';


    /*relations */


    public $belongsToMany =[
      'genres' => [
        'Hyprop\Movies\Models\Genre',
        'table' => 'hyprop_movies_movies_genres',
        'order' => 'genre_title'

      ]

    ];
    
    public $attachOne = [
        'movie_poster' => 'System\Models\File',
        'movie_poster_land' => 'System\Models\File'
    ];   
    
    public function scopeListFrontEnd($query){
        
        $themeName = Theme::getActiveTheme()->getDirName();
        
        if( !empty($themeName) ) {
           $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first(); 
        }
        
        if( !empty($mall) ){
            return $query->where('hyprop_movies_.mall_id', '=', $mall->id );
        }

    }

    public static function getMenuTypeInfo($type)
    {
        $result = [];

        if ($type == 'all-movies') {
            $result = [
                'dynamicItems' => true
            ];
        }

        if ($result) {
            $theme = Theme::getActiveTheme();

            $pages = CmsPage::listInTheme($theme, true);
            $cmsPages = [];

            foreach ($pages as $page) {
                if (!$page->hasComponent('movies')) {
                    continue;
				}

                $cmsPages[] = $page;
            }

            $result['cmsPages'] = $cmsPages;
        }

        return $result;
    }


    public static function resolveMenuItem($item, $url, $theme)
    {
        $result = null;

        if ($item->type == 'all-movies') {
            $result = [
                'items' => []
            ];

            $posts = self::orderBy('title')
            ->get();

            foreach ($posts as $post) {
                $postItem = [
                    'title' => $post->title,
                    'url'   => self::getPostPageUrl($item->cmsPage, $post, $theme),
                    'mtime' => $post->updated_at,
                ];

                $postItem['isActive'] = $postItem['url'] == $url;

                $result['items'][] = $postItem;
            }
        }

        return $result;
    }
    
    /**
     * Returns URL of a post page.
     *
     * @param $pageCode
     * @param $category
     * @param $theme
     */
    protected static function getPostPageUrl($pageCode, $category, $theme)
    {
        $page = CmsPage::loadCached($theme, $pageCode);
        if (!$page) return;

       // dd($category->release);
       $timestamp = strtotime($category->release);

        $paramName = 'slug';
        $params = [
            $paramName => $category->slug,
            'year' => date("Y", $timestamp),
            'month' => date("m", $timestamp),
            'day' => date("d", $timestamp),
        ];
        $url = CmsPage::url($page->getBaseFileName(), $params);

        return $url;
    }


}
