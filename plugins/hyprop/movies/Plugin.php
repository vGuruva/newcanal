<?php namespace Hyprop\Movies;

use DB;
use System\Classes\PluginBase;
use Hyprop\Movies\Models\Movie;
use Hyprop\Movies\Models\Genre;
use Event;
use System\Models\File;
	 

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return[
        'Hyprop\Movies\Components\FilterMovies'=>'filtermovies',
        'Hyprop\Movies\Components\Movieslider'=>'movieslider',
        'Hyprop\Movies\Components\Movies'=>'movies',
      ];
    }

    public function registerSettings()
    {
    }


public function boot() {
    
    Event::listen('offline.sitesearch.extend', function () {

        return new MoviesSearchProvider();
    });
    
    /*
    * Register menu items for the RainLab.Pages plugin
    */
    Event::listen('pages.menuitem.listTypes', function() {
        return [
            'all-movies'		  => 'All Movies',
        ];
    });

    Event::listen('pages.menuitem.getTypeInfo', function($type) {
        if ($type == 'all-movies') {
            return Movie::getMenuTypeInfo($type);
        }
    });

    Event::listen('pages.menuitem.resolveItem', function($type, $item, $url, $theme) {
        if ($type == 'all-movies') {
            return Movie::resolveMenuItem($item, $url, $theme);
        }
    });
    
}



//https://octobercms.com/docs/plugin/scheduling

    public function registerSchedule($schedule){
    
	    $schedule->call(function () {
	
	
	
//start schedule
//$actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//if ($actual_link == 'https://hyprop-1250074418.us-east-2.elb.amazonaws.com/whats-on/movies') {



	
                        //NOW SHOWING
			
			//$file='cineplex.json';
                        //$filename = '/var/www/html/storage/app/uploads/'.$file;
			
			$json = file_get_contents('http://api.numetro.co.za/0.6/schedule?cineplex_id=6');

			//$json = file_get_contents('/var/www/html/storage/app/uploads/cineplex.json');
			
			//file_put_contents('/var/www/html/storage/app/uploads/'.$file,$json);

			$items = json_decode($json);

            $movies = array();
            $genres = array();

            DB::table('hyprop_movies_')->truncate();
            DB::table('hyprop_movies_genres')->truncate();
            DB::table('hyprop_movies_movies_genres')->truncate();
            DB::table('system_files')->where( 'field',  'movie_poster' )->delete();


			foreach($items as $item) {

                if(!in_array($item->movie_id,$movies)) {

                    array_push($movies,$item->movie_id);

                    $details = file_get_contents('http://api.numetro.co.za/0.6/movie?id='.$item->movie_id);

                    $detail = json_decode($details);

                    $youtube = '';
                    if (isset($detail->youtube_id)){
                        $youtube = $detail->youtube_id;
                    }

                    $movie             = new Movie();
                    $movie->title      = $detail->title;
                    $movie->synopsis   = $detail->synopsis_short;
                    $movie->release    = $detail->release_date;
                    $movie->slug       = $detail->url_short_code;
                    $movie->movietime  = $detail->running_time;
                    $movie->director   = $detail->director;
                    $movie->music      = $detail->music;
                    $movie->cast       = $detail->cast_headline.' '.$detail->cast_secondary;
                    $movie->youtube    = $youtube;
                    $movie->age        = $detail->age_restriction;
                    $movie->is_2d      = $item->is_2d;
                    $movie->is_3d      = $item->is_3d;
                    $movie->is_4dx     = $item->is_4dx;
                    $movie->is_xtreme  = $item->is_xtreme;
                    $movie->is_vip     = $item->is_vip;
                    $movie->movie_id   = $detail->id;
                    $movie->mall_name  = $item->cineplex_name;
                    $movie->running_time = $detail->running_time;
                    $movie->mall_id    = 1;

                    if ($detail->poster_file != null) {
                        $file                  = $detail->id.'.jpg';
                        $imageString           = file_get_contents($detail->poster_file);
                        $filename              = '/var/www/html/storage/app/uploads/'.$file;
                        file_put_contents('/var/www/html/storage/app/uploads/'.$file,$imageString);
                        $movie->movie_poster   = $filename;
                    }

                    if (isset($detail->landscape_poster_file)) {
                        $landscape_file                  = $detail->id.'_landscape.jpg';
                        $landscape_imageString           = file_get_contents($detail->landscape_poster_file);
                        $landscape_filename              = '/var/www/html/storage/app/uploads/'.$landscape_file;
                        file_put_contents('/var/www/html/storage/app/uploads/'.$landscape_file,$landscape_imageString);
                        $movie->movie_poster_land   = $landscape_filename;
                    }

                    $movie->save();

                    $genres = $detail->genres;

                    foreach($genres as $genre) {

                        $genre_id = DB::table('hyprop_movies_genres')->where('genre_title', $genre)->first();

                        if($genre_id == null) {

                            $string = strtolower($genre);
                            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

                            DB::table('hyprop_movies_genres')->insert(
                                ['genre_title' => $genre, 'slug' => $string]
                            );

                        }

                        $genre_movie_id = DB::table('hyprop_movies_genres')->where('genre_title', $genre)->first();

                        DB::table('hyprop_movies_movies_genres')->insert(
                            ['movie_id' => $movie->id, 'genre_id' => $genre_movie_id->id]
                        );

                    }

                }

            }

            //FORTH COMING           
            $json = file_get_contents('http://api.numetro.co.za/0.6/forthcoming');
            //file_put_contents('/var/www/html/storage/app/uploads/'.$file,$json);
            //$json = file_get_contents('/var/www/html/storage/app/uploads/forthcoming.json');
            $items = json_decode($json);
            $movies = array();

            foreach($items as $item) {

                if(!in_array($item->id,$movies)) {

                    array_push($movies,$item->id);

                    $details = file_get_contents('http://api.numetro.co.za/0.6/movie?id='.$item->id);
                    $detail = json_decode($details);

                    $youtube = '';
                    if (isset($detail->youtube_id)){
                        $youtube = $detail->youtube_id;
                    }

                    $movie             = new Movie();
                    $movie->title      = $detail->title;
                    $movie->synopsis   = $detail->synopsis_short;
                    $movie->release    = $detail->release_date;
                    $movie->slug       = $detail->url_short_code;
                    $movie->movietime  = $detail->running_time;
                    $movie->director   = $detail->director;
                    $movie->music      = $detail->music;
                    $movie->cast       = $detail->cast_headline.' '.$detail->cast_secondary;
                    $movie->youtube    = $youtube;
                    $movie->age        = $detail->age_restriction;
                    $movie->movie_id   = $detail->id;
                    $movie->mall_name  = $item->cineplex_name;
                    $movie->running_time = $detail->running_time;
                    $movie->mall_id    = 1;

                    if ($detail->poster_file != null) {
                        $file                  =   $detail->id.'.jpg';
                        $imageString           = file_get_contents($detail->poster_file);
                        $filename              = '/var/www/html/storage/app/uploads/'.$file;
                        file_put_contents('/var/www/html/storage/app/uploads/'.$file,$imageString);
                        $movie->movie_poster   = $filename;
                    }

                    if (isset($detail->landscape_poster_file)) {
                        $landscape_file                  = $detail->id.'_landscape.jpg';
                        $landscape_imageString           = file_get_contents($detail->landscape_poster_file);
                        $landscape_filename              = '/var/www/html/storage/app/uploads/'.$landscape_file;
                        file_put_contents('/var/www/html/storage/app/uploads/'.$landscape_file,$landscape_imageString);
                        $movie->movie_poster_land   = $landscape_filename;
                    }

                    $movie->save();

                }
            }
		   
		   
	//	   }
	

// end schedule

        })->everyMinute();
	  
    }

}

	 	