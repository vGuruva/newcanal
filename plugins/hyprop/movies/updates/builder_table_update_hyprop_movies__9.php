<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies9 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->date('release')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->integer('release')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
