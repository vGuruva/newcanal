<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies10 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->integer('movie_id');
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->dropColumn('movie_id');
        });
    }
}
