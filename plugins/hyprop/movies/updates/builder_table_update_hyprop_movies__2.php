<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies2 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->date('release')->nullable();
            $table->integer('time')->nullable();
            $table->text('director')->nullable();
            $table->text('music')->nullable();
            $table->text('cast')->nullable();
            $table->text('synopsis')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->dropColumn('release');
            $table->dropColumn('time');
            $table->dropColumn('director');
            $table->dropColumn('music');
            $table->dropColumn('cast');
            $table->dropColumn('synopsis');
        });
    }
}
