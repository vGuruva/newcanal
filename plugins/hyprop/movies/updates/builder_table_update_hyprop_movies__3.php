<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies3 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->integer('release')->nullable()->unsigned(false)->default(null)->change();
            $table->dropColumn('year');
            $table->dropColumn('synopsis');
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->date('release')->nullable()->unsigned(false)->default(null)->change();
            $table->integer('year')->nullable();
            $table->text('synopsis')->nullable();
        });
    }
}
