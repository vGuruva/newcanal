<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies12 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->integer('mall_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->dropColumn('mall_id');
        });
    }
}