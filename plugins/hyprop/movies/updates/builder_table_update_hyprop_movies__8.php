<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies8 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->integer('is_2d')->nullable();
            $table->integer('is_3d')->nullable();
            $table->integer('is_4dx')->nullable();
            $table->integer('is_xtreme')->nullable();
            $table->integer('is_vip')->nullable();
            $table->string('youtube')->nullable()->change();
            $table->integer('age')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->dropColumn('is_2d');
            $table->dropColumn('is_3d');
            $table->dropColumn('is_4dx');
            $table->dropColumn('is_xtreme');
            $table->dropColumn('is_vip');
            $table->string('youtube', 255)->nullable(false)->change();
            $table->integer('age')->nullable(false)->change();
        });
    }
}
