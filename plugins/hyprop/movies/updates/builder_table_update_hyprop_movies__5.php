<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies5 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->renameColumn('name', 'title');
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->renameColumn('title', 'name');
        });
    }
}
