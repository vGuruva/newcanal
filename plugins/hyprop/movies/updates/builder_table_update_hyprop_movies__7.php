<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateHypropMovies7 extends Migration
{
    public function up()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->string('youtube');
            $table->integer('age');
        });
    }
    
    public function down()
    {
        Schema::table('hyprop_movies_', function($table)
        {
            $table->dropColumn('youtube');
            $table->dropColumn('age');
        });
    }
}
