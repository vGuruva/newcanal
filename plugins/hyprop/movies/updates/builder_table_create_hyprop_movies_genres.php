<?php namespace Hyprop\Movies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateHypropMoviesGenres extends Migration
{
    public function up()
    {
        Schema::create('hyprop_movies_genres', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('genre_title');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('hyprop_movies_genres');
    }
}
