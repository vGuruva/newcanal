<?php

namespace Hyprop\Movies\Components;

use DB;
use ApplicationException;
use Cms\Classes\ComponentBase;
use Cache;
use Request;
use Hyprop\Movies\Models\Movie;
use Cms\Classes\Theme;



class Movieslider extends ComponentBase
{


    public $detail;

    public function componentDetails()
    {
        return [
            'name'        => 'Movie Slider',
            'description' => 'Movie Slider'
        ];
    }

    public function defineProperties()
    {
        return [
            'movieslider' => [
                'title'             => 'Movies',
                'type'              => 'dropdown',
                'default'           => 'now_showing',
			    'placeholder'       => 'Select',
                'options'           => ['now_showing'=>'now_showing', 'forthcoming'=>'Forth Coming']
            ]
        ];
    }



    public function onRun()
    {

          $this->page['movieslider'] = $this->movieSlider();

    }

    protected function movieSlider() {
        
        $themeName = Theme::getActiveTheme()->getDirName();
        
        $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first();        

          $date = date("Y-m-d");

          $query = DB::table('hyprop_movies_')
                ->where('hyprop_movies_.mall_id', '=', $mall->id )
                ->where('release', '<=', $date)
                ->orderBy('release', 'desc')
                ->get();

          return $query;

        }


    public $movieslider;


}
