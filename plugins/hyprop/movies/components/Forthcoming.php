<?php namespace Hyprop\Movies\Components;


use Cms\Classes\ComponentBase;

use Hyprop\Movies\Models\Movie;

class Forthcoming extends ComponentBase {

public function componentDetails() {

return [
  'name'=>'Forthcoming Movies',
  'description'=>'Movie List'
];

}


public function defineProperties() {

return [
  'sortOrder'=>[
    'title'=>'Sort Movies',
    'description'=>'Sort Movies Listing',
    'type'=>'dropdown',
    'default'=>'nowshowing',
  ]
];

}

public function getSortOrderOptions(){

  return [
    'nowshowing'=>'Now Showing',
    'forthcoming'=>'Forthcoming',
  ];

}

public function onRun(){
  $this->movies = $this->loadMovies();
}

protected function loadMovies(){

$query = Movie::all();

$date = date("Y-m-d");

if($this->property('sortOrder')=='nowshowing'){

  $query = $query->where('release', '<=', $date);

}

if($this->property('sortOrder')=='forthcoming'){

  $query = $query->where('release', '>', $date);

}

return $query;

}


public $movies;



}
