<?php namespace Hyprop\Movies\Components;

use Cms\Classes\ComponentBase;
use Input;
use Hyprop\Movies\Models\Movie;
use Hyprop\Movies\Models\Genre;

/**
 * Model
 */
class FilterMovies extends ComponentBase
{

    public function componentDetails(){
      return [
        'name'=>'Filter Movies',
        'description'=>'Filter Movies',
      ];
    }

    public function onRun(){
      $this->movies = $this->filterMovies();
      $this->genres = Genre::all();
    }

    protected function filterMovies() {
      $genre = Input::get('genre');
      $query = Movie::all();

      if($genre){
        $query = Movie::whereHas('genres',function($filter) use ($genre){

          $filter->where('slug','=',$genre);


        })->get();
      }



      return $query;
    }

    public $movies;
    public $genres;


}
