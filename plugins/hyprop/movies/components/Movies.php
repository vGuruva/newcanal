<?php namespace Hyprop\Movies\Components;

use DB;
use Input;
use Response;
use Redirect;
use Cms\Classes\ComponentBase;
use Hyprop\Movies\Models\Movie;
use Hyprop\Movies\Models\Genre;
use Cms\Classes\Theme;

class Movies extends ComponentBase {

  public function componentDetails() {
    return [
      'name'=>'Movies',
      'description'=>'Movie List'
    ];
  }


  public function defineProperties() {

    return [
      'sortOrder'=>[
        'title'=>'Sort Movies',
        'description'=>'Sort Movies Listing',
        'type'=>'dropdown',
        'default'=>'nowshowing',
      ]
    ];

  }

  public function getSortOrderOptions(){

    return [
    'nowshowing'=>'Now Showing',
    'forthcoming'=>'Forthcoming',
    ];

  }

  public function onRun(){

    //return Response::make($this->controller->run('404-movies'), 404);

    if ($this->loadMovies()->count()==0){

      //return Redirect::to('/whats-on/movies/');
      //return Response::make($this->controller->run('/movies/404-movies'), 404);

    }

    $this->movies = $this->loadMovies();
    $this->genres = Genre::all();
    $this->search = Input::get('search');

  }

  protected function loadMovies(){
    $themeName = Theme::getActiveTheme()->getDirName();

    $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first();     

    $search =  Input::get('search');

    if ($this->param('slug')){
      $genre = $this->param('slug');
    } else {
      $genre = Input::get('genre');
    }

    $query = Movie::listFrontEnd()->get();

    $today = date("Y-m-d");

    if($this->property('sortOrder') == 'nowshowing' && !$genre || $this->property('sortOrder')=='nowshowing' && !$search){

      $query = Movie::where('release', '<=', $today)
        ->where('hyprop_movies_.mall_id', $mall->id )
        ->orderBy('release', 'desc')
        ->get();

    }

    if($genre=='forthcoming'){

      $query = Movie::where('release', '>', $today)
      ->where('hyprop_movies_.mall_id', $mall->id );

    }

    if($this->property('sortOrder')=='nowshowing' && $genre ){

      $query = Movie::whereHas('genres',function($filter) use ($genre){
        $filter->where('slug','=',$genre);
      })->where('hyprop_movies_.mall_id', $mall->id )->get();

    }

    if($this->property('sortOrder')=='nowshowing' && $search){

      //$query = $query->where('title', 'like', 'T%'));
      $query = DB::table('hyprop_movies_')
              ->where('title', 'like', '%'.$search.'%')
              ->where('mall_id', '=', $mall->id )
              ->orWhere('synopsis', 'like', '%'.$search.'%')
              ->where('mall_id', '=', $mall->id )
              ->orderBy('release', 'desc')
              ->get();

    }

    return $query;

  }

  public $search;
  public $movies;
  public $today;
  public $genre;

}
