<?php namespace Hyprop\Movies;

use OFFLINE\SiteSearch\Classes\Providers\ResultsProvider;
use OFFLINE\SiteSearch\Classes\Result;
use Hyprop\Movies\Models\Movie as Movies;
use Cms\Classes\Theme;
use System\Models\File;


class MoviesSearchProvider extends ResultsProvider
{
    
     public function search()
    {
	
        $themeName = Theme::getActiveTheme()->getDirName();
        $mall = \Hyprop\Malls\Models\Malls::where('theme_name', $themeName)->first();          
         
        // Get your matching models
        $matching = Movies::where('title', 'like', "%{$this->query}%")
        ->where('mall_id', $mall->id )
        ->orWhere('synopsis', 'like', "%{$this->query}%")
        ->where( 'mall_id', $mall->id )
        ->get();

        // Create a new Result for every match
        foreach ($matching as $match) {
            
            $result            = $this->newResult();

            $result->relevance = 1;
            $result->title     = $match->title;
            $result->text      = $match->synopsis;
            $result->url       = '/whats-on/movie/' . $match->slug;
            //$result->thumb     = $match->poster;
            $result->model     = $match;


            // Add the results to the results collection
            $this->addResult( $result );
            
        }

        return $this;
    }

    public function displayName()
    {
        return 'Movies';
    }

    public function identifier()
    {
        return 'Hyprop.Movies';
    }   
    
    
}



